
#ifndef NET_LOG_RECORDER_H__
#define NET_LOG_RECORDER_H__

#include "common/utils/flowTrace.h"

class NetLog;

class NetLogRecorder : public FlowTrace::TraceCore
{
	struct RecorderImpl;

private:
	std::unique_ptr<RecorderImpl> m_logWrt;

public:
	NetLogRecorder();
	~NetLogRecorder() override;

private:
	void writeNetIssue(NetLog *issue);
	void Issue(std::unique_ptr<AbstractFlowIssue> issue) override;
	FlowTrace::FlowIssue *CreateNewIssue() override;

public:
	bool MakeFile(const std::string &url);
};

#endif

