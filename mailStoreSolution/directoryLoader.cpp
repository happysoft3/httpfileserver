
#include "directoryLoader.h"
#include "fileloader.h"
#include "invalidLoader.h"
#include "mimeLoader.h"
#include "responseFactory.h"
#include "common/include/incFilesystem.h"
#include "htmlListModel.h"
#include "uriRule.h"
#include "common/utils/stringhelper.h"
#include <algorithm>
#include <iterator>

using namespace _StringHelper;

class DirectoryLoader::ParentDIrLoader : public DirectoryLoader
{
public:
	ParentDIrLoader()
		: DirectoryLoader()
	{ }

	~ParentDIrLoader() override
	{ }

private:
	std::string FileName() const override
	{
		return "Parent Directory";
	}
};

class DirectoryLoader::DirectoryListModel : public HtmlListModel
{
private:
	DirectoryLoader *m_loader;

public:
	DirectoryListModel(DirectoryLoader *loader)
	{
		m_loader = loader;
	}
	~DirectoryListModel() override
	{ }

private:
	void InitialDeclare() override
	{
		declareKeyHandler("Icon", [this]() { return this->Image(); });
		declareKeyHandler("Name", [this]() { return this->FileName(); });
		pushSubKey("Name", [this]() { return this->URL(); });
		declareKeyHandler("Size", [this]() { return this->Size(); });
	}

	std::string value(FileAccessInfo accessInfo) const
	{
		std::string sValue;

		m_loader->GetFront(accessInfo, sValue);
		return sValue;
	}

	std::string FileName() const
	{
		return value(FileAccessInfo::NAME);
	}

	std::string Size() const
	{
		return value(FileAccessInfo::SIZE);
	}

	std::string URL() const
	{
		return value(FileAccessInfo::URL);
	}

	std::string Image() const
	{
		return stringFormat("/icons/%s.gif", (value(FileAccessInfo::ISDIR) == "0") ? "link" : "folder");
	}

	bool Has() const override
	{
		return m_loader->Has();
	}

	void Next() override
	{
		m_loader->PopFront();
	}
};

DirectoryLoader::DirectoryLoader(const std::string &rootPath)
	: AbstractLoader(rootPath)
{
	m_loadSubDirectories = false;
}

DirectoryLoader::~DirectoryLoader()
{ }

std::unique_ptr<AbstractResponseFactory> DirectoryLoader::CreateResponseFactory() const
{
	return std::make_unique<DirectoryResponseFactory>(uriTarget()->QueryValue("fmt"));
}

void DirectoryLoader::insertParentFolder(const std::string &dirPath)
{
	if (dirPath.empty())
		return;
	if (dirPath == "/")
		return;

	std::unique_ptr<AbstractLoader> parentDir = std::make_unique<ParentDIrLoader>();
	auto uri = std::make_unique<UriRule>();

	uri->FetchFromOtherUri(*uriTarget());
	uri->DiscardBackPathOne();
	parentDir->SetUri(std::move(uri));
	m_files.push_back(std::move(parentDir));
}

std::unique_ptr<AbstractLoader> DirectoryLoader::createFileLoaderDetail(const std::string &rootPath, const std::string &name)
{
	size_t extensionPos = name.find_last_of('.');
	std::string extension = name.substr(++extensionPos);

	if (extension == "eml")
		return std::make_unique<MimeLoader>(rootPath);

	return std::make_unique<FileLoader>(rootPath);
}

std::unique_ptr<AbstractLoader> DirectoryLoader::createLoader(const std::string &rootPath, const std::string &name, bool useDetail)
{
	std::string fullName = stringFormat("%s\\%s", rootPath, name);
	std::unique_ptr<AbstractLoader> loader;
	
	if (NAMESPACE_FILESYSTEM::is_directory(fullName))
		loader = std::make_unique<DirectoryLoader>(rootPath);
	else
		loader = useDetail ? createFileLoaderDetail(rootPath, name) : std::make_unique<FileLoader>(rootPath);
	loader->FetchUri(*this, useDetail ? "" : name);
	loader->Load(name);
	return loader;
}

void DirectoryLoader::loadImpl(const std::string &loadPath)
{
	for (const auto &entry : NAMESPACE_FILESYSTEM::directory_iterator(loadPath))
	{
		auto entryPath = entry.path();

		m_files.push_back(createLoader(loadPath, entryPath.filename().string()));
	}
}

std::string DirectoryLoader::FileName() const
{
	return DirName() + '/';
}

std::string DirectoryLoader::DirName() const
{
	return m_dirName;
}

std::string DirectoryLoader::writeFullpath(const std::string &url)
{
	if (url.empty())
		return source();

	return stringFormat("%s\\%s", source(), url);
}

void DirectoryLoader::tryLoad(const std::string &rscPath)
{
	std::string userPath = writeFullpath(rscPath);

	std::transform(userPath.cbegin(), userPath.cend(), userPath.begin(), [](const char &c) { return (c == '/') ? '\\' : c; });

	do
	{
		if (NAMESPACE_FILESYSTEM::exists(userPath))
		{
			m_files.clear();
			if (NAMESPACE_FILESYSTEM::is_directory(userPath))
			{
				size_t pos = userPath.find_last_of('\\');

				m_dirName = (pos != std::string::npos) ? userPath.substr(++pos) : userPath;
				if (m_loadSubDirectories)
				{
					insertParentFolder(rscPath);
					loadImpl(userPath);
				}
				break;
			}
			else
			{
				m_singleChild = createLoader(source(), rscPath, true);
			}

			break;
		}

		//m_singleChild = std::make_unique<InvalidLoader>();
		throw std::logic_error(stringFormat("has no attribution %s file", rscPath));
	} while (false);
}

void DirectoryLoader::Load(const std::string &rscPath)
{
	try
	{
		tryLoad(rscPath);
		return;
	}
	catch (const std::invalid_argument &invArg)
	{
		printf("exception::%s%d %s", __FILE__, __LINE__, invArg.what());
	}
	catch (const std::logic_error &lerr)
	{
		printf("exception::%s%d%s", __FILE__, __LINE__, lerr.what());
	}
	m_singleChild = std::make_unique<InvalidLoader>();
}

bool DirectoryLoader::GetFront(DirectoryLoader::FileAccessInfo index, std::string &dest)
{
	if (m_files.empty())
		return false;

	static auto retFunc = [](std::string &dest, const std::string &value) { dest = value; return true; };
	auto &&first = m_files.front();

	switch (index)
	{
	case FileAccessInfo::ISDIR: return retFunc(dest, first->IsDirectory() ? "1" : "0");
	case FileAccessInfo::NAME: return retFunc(dest, first->EncFileName());
	case FileAccessInfo::SIZE: return retFunc(dest, first->Size());
	case FileAccessInfo::URL: return retFunc(dest, /*first->URL()*/ first->EncURL());
	case FileAccessInfo::EXTENSION: return first->IsDirectory() ? false : retFunc(dest,  first->Extension());
	default: return false;
	}
}

std::unique_ptr<AbstractLoader> DirectoryLoader::FetchSingleChild()
{
	if (!m_singleChild)
		return {};

	std::unique_ptr<AbstractLoader> pop;

	pop.swap(m_singleChild);
	return pop;
}

std::unique_ptr<AbstractLoader> DirectoryLoader::PopFront()
{
	if (m_files.size())
	{
		std::unique_ptr<AbstractLoader> pop = std::move(m_files.front());

		m_files.pop_front();
		return pop;
	}
	return {};
}

std::unique_ptr<HtmlListModel> DirectoryLoader::CreateModel()
{
	return std::make_unique<DirectoryListModel>(this);
}

