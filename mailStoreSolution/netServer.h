
#ifndef NET_SERVER_H__
#define NET_SERVER_H__

#include <memory>
#include <functional>
#include <future>
#include <string>

class NetSocket;
class WorkThreadMan;
class ClientMap;
struct WSAData;
class NetOverlapped;
class NetBufferData;

class FlowTrace;
class AbstractTraceCore;
class AbstractFlowIssue;

class NetServer
{
	struct Core;
private:
	std::unique_ptr<NetSocket> m_servSocket;
	std::unique_ptr<Core> m_core;
	std::unique_ptr<WorkThreadMan> m_workerThread;
	std::shared_ptr<bool> m_aliveAcceptFork;
	std::unique_ptr<ClientMap> m_clientMap;
	std::future<void> m_acceptThreadResult;
	
	/////서버 로그파일 기록////
	std::shared_ptr<FlowTrace> m_servLog;

private:
	std::unique_ptr<NetOverlapped> m_haltOverl;

private:
	int m_portId;
	std::string m_ipAddress;

public:
	NetServer();
	virtual ~NetServer();

protected:
	virtual void onStartup() {}
	virtual bool onShutdown(bool ret);
	virtual void onDisconnected(NetSocket *client);
	virtual void onReceive(NetSocket *client, std::unique_ptr<NetBufferData> buffer);
	virtual void onSend(NetSocket *client);
	virtual void onJoin(const NetSocket &/*client*/)
	{ }

	void createServerLog(std::unique_ptr<AbstractFlowIssue> issue);

private:
	std::unique_ptr<WSAData> initWsa();
	bool initServerSocket();

private:
	bool workProc();
	void shutdownWorkThread(int numberOfConcurrentThreads);
	bool startupWorkThread();
	void acceptProc(std::weak_ptr<bool> ctl);
	void createAcceptThread();

	virtual bool initServer()
	{
		return true;
	}
	virtual std::unique_ptr<AbstractTraceCore> makeLogRecorder();
	bool initServerLog();

public:
	bool Start();
};

#endif

