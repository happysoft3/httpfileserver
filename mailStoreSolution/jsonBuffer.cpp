
#include "jsonBuffer.h"

static constexpr size_t s_maxPull = 100;

JsonBuffer::JsonBuffer(std::string &&jsStream)
	: BinaryBuffer()
{
	m_jsStream = std::forward<std::string>(jsStream);
	m_rdPos = 0;
}

JsonBuffer::~JsonBuffer()
{ }

std::vector<uint8_t> JsonBuffer::pullStream()
{
	size_t required = (m_rdPos + s_maxPull) > m_jsStream.size() ? (m_jsStream.size() - m_rdPos) : s_maxPull;
	
	while (m_rdPos < m_jsStream.size())
	{
		if (!required)
			break;

		std::vector<uint8_t> reqBuffer(required, 0);

		std::copy_n(m_jsStream.cbegin() + m_rdPos, reqBuffer.size(), reqBuffer.begin());
		m_rdPos += required;
		return reqBuffer;
	}
	return {};
}

void JsonBuffer::putStream()
{
	std::vector<uint8_t> sendBuffer = pullStream();

	if (sendBuffer.empty())
	{
		Clear();
		return;
	}

	StreamSet(sendBuffer);
}

bool JsonBuffer::CreateComponent()
{
	putStream();
	return true;
}
