﻿// mailStoreSolution.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

//#define TEST_THIS_APP
#ifndef TEST_THIS_APP


////JSON OBJECT FORMAT//////////
/*******

{
	"filecount" : 2,
	"base_url" : "127.0.0.1:8080",
	"source" :
	[
		{
			"filename" : "setting.txt",
			"filesize" : 500,
			"mimety" : "text/plain",
			"path" : "C:\\Users"
		}
	],
	[
		{
			"filename", "today.txt",
			"filesize" : 3141,
			"mimety" : "text/plain",
			"path" : "C:\\Users"
		}
	]
}

*********/

#include "httpServer.h"
#include <iostream>

int main()
{
	{
		std::unique_ptr<NetServer> serv(new HttpServer);

		if (serv->Start())
		{
			std::cout << "server on" << std::endl;
		}
		std::cout << "any key" << std::endl;
		std::getchar();
	}
	std::cout << "main thread end" << std::endl;
	return 0;
}

#else

#include "json/json.h"
#include <iostream>

#pragma warning(disable:4996)
#pragma comment(lib, "jsoncpp.lib")

int main()
{

	/*root["dirName"] = "F:\\project\\x-mailstore\\files";
	root["count"] = 2;*/

	/*root.append(1);
	root.append(2);*/
	Json::Value base;

	base["today"] = "8 May 2022- 10:52";
	{
		Json::Value tree;

		tree["name"] = "files";
		tree["size"] = 500;
		base["fork"] = tree;
	}

	{
		Json::Value sub;

		sub["filename"] = "CMakeList.txt";
		sub["filesize"] = 500;
		sub["extension"] = ".txt";
		base["files"].append(sub);
	}
	{
		Json::Value sub;

		sub["filename"] = "pch.h";
		sub["filesize"] = 12491;
		sub["extension"] = ".h";
		base["files"].append(sub);
	}

	Json::StyledWriter jWrt;

	std::string s = jWrt.write(base);

	std::cout << s << std::endl;

	
	std::getchar();

	return 0;
}

#endif


// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
