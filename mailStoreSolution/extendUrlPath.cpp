
#include "extendUrlPath.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

ExtendUrlPath::ExtendUrlPath()
	: UrlString()
{
	predefine();
}

ExtendUrlPath::~ExtendUrlPath()
{ }

std::string ExtendUrlPath::mixAll()
{
	std::string buffer;

	for (;;)
	{
		auto stok = popTerm();

		if (stok->Eof())
		{
			pushTerm(std::move(stok));
			return buffer;
		}

		buffer += stringFormat("/%s", stok->Value());
	}
}

void ExtendUrlPath::predefine()
{
	putValueFunction("path", [this]() { return this->mixAll(); });
}


