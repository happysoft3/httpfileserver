
#ifndef EXTEND_URL_PATH_H__
#define EXTEND_URL_PATH_H__

#include "common/utils/urlString.h"

class ExtendUrlPath : public UrlString
{
public:
	ExtendUrlPath();
	~ExtendUrlPath() override;

private:
	std::string mixAll();
	void predefine();
};

#endif

