
#ifndef NET_FILE_BUFFER_H__
#define NET_FILE_BUFFER_H__

#include "common/utils/binaryBuffer.h"
#include <string>

class NetFileBuffer : public BinaryBuffer
{
	static constexpr size_t binary_filebuffer_max = 256;
	struct Core;
private:
	std::string m_fUrl;
	size_t m_fSize;
	std::unique_ptr<Core> m_fileCore;

public:
	NetFileBuffer();
	explicit NetFileBuffer(const std::string &fileName, const std::string &fileSize);
	~NetFileBuffer() override;

private:
	void setFilesize(const std::string &fSize);
	void fillZero(size_t buffsize, size_t &procSize);	//파일 작업 중, 예외상황일 때에만
	void printFile(size_t rdsize, size_t &procSize);
	size_t requestSize() const;
	void printFilestream();
	bool makePrintImpl();

public:
	bool MakePrinter();

private:
	bool CreateComponent() override;
};

#endif

