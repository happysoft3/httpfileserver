
#include "netlog.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

NetLog::NetLog(const std::string &issueMsg)
	: FlowTrace::FlowIssue(issueMsg)
{ }

NetLog::~NetLog()
{ }

std::string NetLog::What() const
{
	return stringFormat("[%s] %s\n", issueTime(), issueName());
}

