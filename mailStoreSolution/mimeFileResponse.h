
#ifndef MIME_FILE_RESPONSE_H__
#define MIME_FILE_RESPONSE_H__

#include "httpFileResponse.h"

class MimeLoader;
class MimeContentFile;

class MimeFileResponse : public HttpFileResponse
{
private:
	MimeLoader *m_mimeLoader;
	std::string m_fileId;

public:
	MimeFileResponse(const std::string &fileId);
	~MimeFileResponse() override;

private:
	void setLoader(AbstractLoader *loader) override;
	bool writeFileMeta(MimeContentFile &content);
	void netPostTask(std::unique_ptr<MimeContentFile> content);
	size_t makeBody() override;
};

#endif

