
#ifndef WORK_THREAD_MAN_H__
#define WORK_THREAD_MAN_H__

#include <memory>
#include <vector>
#include <functional>
#include <future>

class WorkThreadMan
{
	struct WorkerIdentifier;

private:
	using worker_task_ty = std::function<bool()>;
	worker_task_ty m_task;
	using terminate_task_ty = std::function<void(int)>;
	terminate_task_ty m_terminateTask;
	std::weak_ptr<bool> m_checkRunning;
	std::vector<std::future<bool>> m_resultVec;

public:
	WorkThreadMan();
	~WorkThreadMan();

private:
	bool workThreadProc(std::unique_ptr<WorkerIdentifier> ident);

public:
	void SetTask(worker_task_ty &&task, terminate_task_ty &&shutdwTask = {});
	bool Place();
	void Terminate();
};

#endif

