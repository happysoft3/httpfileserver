
#include "httpJsonResponse.h"
#include "directoryLoader.h"
#include "jsonBuffer.h"
#include <json/json.h>

#pragma warning(disable:4996)
#pragma comment(lib, "jsoncpp.lib")

HttpJsonResponse::HttpJsonResponse()
	: HttpResponse()
{ }

HttpJsonResponse::~HttpJsonResponse()
{ }

void HttpJsonResponse::setLoader(AbstractLoader *loader)
{
	m_dirLoader = dynamic_cast<DirectoryLoader *>(loader);
}

std::unique_ptr<Json::Value> HttpJsonResponse::loadJsonSingle(std::unique_ptr<AbstractLoader> &&loader)
{
	std::unique_ptr<Json::Value> value(new Json::Value);

	(*value)["filename"] = loader->FileName();
	(*value)["filesize"] = loader->SizeInt();
	(*value)["extension"] = loader->IsDirectory() ? "[DIR]" : loader->Extension();

	return value;
}

std::unique_ptr<Json::Value> HttpJsonResponse::loadFiles()
{
	std::unique_ptr<AbstractLoader> loader;
	std::unique_ptr<Json::Value> tree(new Json::Value);
	std::list<std::unique_ptr<Json::Value>> fileJsList;

	(*tree)["dirName"] = m_dirLoader->DirName();
	(*tree)["count"] = m_dirLoader->Count();

	for (;;)
	{
		loader = m_dirLoader->PopFront();

		if (!loader)
			break;

		fileJsList.push_back(loadJsonSingle(std::move(loader)));
	}
	for (auto &js : fileJsList)
		(*tree)["files"].append(std::move(*js));

	return tree;
}

void HttpJsonResponse::makeJson(std::string &jsStream)
{
	std::unique_ptr<Json::Value> dirTree = std::move(loadFiles());
	Json::StyledWriter jsWrt;		//The class deprecated, is gone
	
	jsStream = jsWrt.write(*dirTree);
	//Json::StreamWriterBuilder jsWrt;

	//jsStream = jsWrt write(*dirTree);
}

size_t HttpJsonResponse::makeBody()
{
	if (m_dirLoader)
	{
		std::string jsStream;

		changeHeaderValue("Content-Type", "application/json; charset=utf-8");
		makeJson(jsStream);

		size_t length = jsStream.length();
		changeHeaderValue("Content-Length", std::to_string(length));

		putSendBuffer(std::make_unique<JsonBuffer>(std::move(jsStream)));
		return length;
	}
	return 0;
}


