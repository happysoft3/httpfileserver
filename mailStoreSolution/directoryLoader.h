
#ifndef DIRECTORY_LOADER_H__
#define DIRECTORY_LOADER_H__

#include "abstractLoader.h"

#include <string>
#include <list>
#include <memory>

class FileLoader;
class HtmlListModel;
class AbstractResponseFactory;

class DirectoryLoader : public AbstractLoader
{
	class ParentDIrLoader;
	class DirectoryListModel;

public:
	enum class FileAccessInfo
	{
		ISDIR,
		NAME,
		SIZE,
		URL,
		EXTENSION
	};

private:
	std::unique_ptr<AbstractLoader> m_singleChild;
	std::list<std::unique_ptr<AbstractLoader>> m_files;
	std::string m_dirName;
	bool m_loadSubDirectories;

public:
	DirectoryLoader(const std::string &rootPath = {});
	~DirectoryLoader() override;

private:
	std::unique_ptr<AbstractResponseFactory> CreateResponseFactory() const override;
	void insertParentFolder(const std::string &dirPath);
	std::unique_ptr<AbstractLoader> createFileLoaderDetail(const std::string &rootPath, const std::string &name);
	std::unique_ptr<AbstractLoader> createLoader(const std::string &rootPath, const std::string &name, bool useDetail = false);
	void loadImpl(const std::string &loadPath);
	bool IsDirectory() const override
	{
		return true;
	}

public:
	std::string FileName() const override;
	std::string DirName() const;
	void SetLoadingSubDirectories(bool enable)
	{
		m_loadSubDirectories = enable;
	}

private:
	std::string writeFullpath(const std::string &url);
	void tryLoad(const std::string &rscPath);

public:
	void Load(const std::string &rscPath) override;
	bool Has() const
	{
		return m_files.empty() ? false : true;
	}
	size_t Count() const
	{
		return m_files.size();
	}

public:
	bool GetFront(FileAccessInfo index, std::string &dest);
	std::unique_ptr<AbstractLoader> FetchSingleChild();
	std::unique_ptr<AbstractLoader> PopFront();
	std::unique_ptr<HtmlListModel> CreateModel();
};

#endif

