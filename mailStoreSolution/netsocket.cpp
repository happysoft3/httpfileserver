
#include "netsocket.h"
#include "netoverlapped.h"
#include "netstreambuffer.h"
#include "common/utils/iptobyte.h"
//#include "utils/flowTrace.h"
#include "netlog.h"
#include "common/utils/stringhelper.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>

using namespace _StringHelper;

class NetSocket::IntSocket
{
private:
	uint32_t m_fd;

public:
	IntSocket(uint32_t sock = INVALID_SOCKET)
		: m_fd(sock)
	{ }

	~IntSocket()
	{
		if (m_fd != INVALID_SOCKET)
			::closesocket(m_fd);
	}

	uint32_t operator*() const
	{
		return m_fd;
	}
};

struct NetSocket::Core
{
	std::unique_ptr<SOCKADDR_IN> m_servAddr;
	std::unique_ptr<SOCKADDR_IN> m_selfAddr;
	std::vector<uint8_t> m_acceptBuffer;
	size_t m_acceptBytes;
	std::list<std::unique_ptr<BinaryBuffer>> m_sendTaskQueue;
};

NetSocket::NetSocket(uint32_t sock)
	: m_core(new Core{})
{
	m_recvOverlapped = std::make_unique<NetOverlapped>();
	m_sendOverlapped = std::make_unique<NetOverlapped>();
	m_socket = std::make_unique<IntSocket>(sock);
	m_acceptOverl = std::make_unique<NetOverlapped>();
}

NetSocket::~NetSocket()
{ }

bool NetSocket::recvImpl()
{
	auto pOverlapped = m_recvOverlapped->Create();

	pOverlapped->m_ioOperation = stNetOverlapped::RECEIVE;
	int res = ::WSARecv(**m_socket, &pOverlapped->m_wsaBuffer, 1, &pOverlapped->m_realBytes, &pOverlapped->m_dwFlags, reinterpret_cast<LPWSAOVERLAPPED>(pOverlapped), nullptr);

	if (res == SOCKET_ERROR && (::WSAGetLastError() != ERROR_IO_PENDING))
	{
		///put message. -error- //연결해제 등의 예외처리가 요구될 것으로..
		return false;
	}
	return true;
}

bool NetSocket::sendImpl(const std::string &src, size_t &procBytes)
{
	auto pOverlapped = m_sendOverlapped->CreateWithBuffer(src);

	pOverlapped->m_ioOperation = stNetOverlapped::SEND;
	int res = ::WSASend(**m_socket, &pOverlapped->m_wsaBuffer, 1, &pOverlapped->m_realBytes, pOverlapped->m_dwFlags, reinterpret_cast<LPWSAOVERLAPPED>(pOverlapped), nullptr);

	if (res == SOCKET_ERROR && ::WSAGetLastError() != ERROR_IO_PENDING)
	{
		return false;
	}
	procBytes = pOverlapped->m_realBytes;
	return true;
}

bool NetSocket::sendBufferImpl(BinaryBuffer &buffer)
{
	std::string sendData;

	if (buffer.CreateComponent())
	{
		buffer.StreamMove(sendData);
		if (sendData.length())
		{
			std::string err;
			if (sendData.length() > NetOverlapped::net_buffer_max_size)
			{
				err = stringFormat("%s--%d Send buffer size over the maximum %d.\n", __FILE__, __LINE__, sendData.length());
				m_serverLog->PushTrace(std::make_unique<NetLog>(err));

				return false;
			}
			size_t sendBytes = 0, bufferBytes = sendData.length();
			bool result = sendImpl(sendData, sendBytes);

			if (bufferBytes != sendBytes)
			{
				//TODO. Erase it later
				err = stringFormat("%s--%d The size was Unmatched between %d to %d.\n", __FILE__, __LINE__, bufferBytes, sendBytes);
				m_serverLog->PushTrace(std::make_unique<NetLog>(err));
			}
			return result;
		}
	}
	return false;
}

bool NetSocket::IsValid() const
{
	if (m_socket)
		return **m_socket != INVALID_SOCKET;

	return false;
}

bool NetSocket::Bind()
{
	if (m_socket)
	{
		if (!m_core->m_servAddr)
			return false;

		if (bind(**m_socket, reinterpret_cast<sockaddr *>(m_core->m_servAddr.get()), sizeof(SOCKADDR_IN)) != SOCKET_ERROR)
			return true;

		m_socket.reset();
	}
	return false;
}

bool NetSocket::Listen(int backlog)
{
	if (m_socket)
	{
		if (listen(**m_socket, backlog) != SOCKET_ERROR)
			return true;

		m_socket.reset();
	}
	return false;
}

void NetSocket::SetInfo(const std::string &ipAddress, int port)
{
	IpToByte ipconv;

	ipconv.SetIpAddress(ipAddress);
	m_core->m_servAddr = std::make_unique<SOCKADDR_IN>();

	m_core->m_servAddr->sin_family = AF_INET;
	m_core->m_servAddr->sin_port = htons(static_cast<u_short>(port));
	//m_core->m_servAddr->sin_addr.S_un.S_addr = htonl(INADDR_ANY);

	std::vector<char> ipbyte;

	ipconv.ToByteStream(ipbyte);
	char *pSinAddr = reinterpret_cast<char *>(&m_core->m_servAddr->sin_addr);
	std::copy(ipbyte.begin(), ipbyte.end(), stdext::checked_array_iterator<char *>(pSinAddr, 4));
}

void NetSocket::PostReceiving(stNetOverlapped *pOverl, BinaryBuffer *pBuffer)
{
	if (pBuffer)
		pBuffer->StreamPush(pOverl->m_message, pOverl->m_realPostBytes);
	m_recvOverlapped->Delete(pOverl);
}

bool NetSocket::ReservReceive()
{
	if (!recvImpl())
		return false;

	////받기 요청을 했다 뿐이지, 실질적으로 즉시 받지는 못함..
	return true;
}

bool NetSocket::Send(const std::string &srcMsg)
{
	if (srcMsg.empty())
		return false;

	auto readBuffer = std::make_unique<NetStreamBuffer>();

	readBuffer->StreamSet(srcMsg);
	auto writeBuffer = std::make_unique<NetStreamBuffer>(std::move(readBuffer));

	AppendSendQueue(std::move(writeBuffer));
	return true;
}

NetSocket *NetSocket::Accept()
{
	if (m_socket)
	{
		int addrLen = sizeof(SOCKADDR_IN);
		std::unique_ptr<SOCKADDR_IN> clientInfo(new SOCKADDR_IN({}));
		auto client = ::WSAAccept(**m_socket, reinterpret_cast<sockaddr *>(clientInfo.get()), &addrLen, nullptr, 0);

		if (client != INVALID_SOCKET)
		{
			NetSocket *clientObject = new NetSocket(client);

			clientObject->m_core->m_selfAddr = std::move(clientInfo);
			return clientObject;
		}
	}
	return nullptr;
}

NetSocket *NetSocket::PostAccept()
{
	uint32_t sock = ::WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, nullptr, 0, WSA_FLAG_OVERLAPPED);

	if (sock == INVALID_SOCKET)
		return nullptr;

	std::unique_ptr<NetSocket> client(new NetSocket(sock));

	client->m_core->m_acceptBuffer.resize(static_cast<size_t>(64));
	auto pOverlapped = m_acceptOverl->Create();

	pOverlapped->m_ioOperation = stNetOverlapped::ACCEPT;
	static constexpr size_t commonAddrLength = sizeof(SOCKADDR_IN) + 16;
	if (!::AcceptEx(**m_socket, sock, client->m_core->m_acceptBuffer.data(), 0, commonAddrLength, commonAddrLength,
		reinterpret_cast<LPDWORD>(&client->m_core->m_acceptBytes), reinterpret_cast<LPOVERLAPPED>(pOverlapped)))
		return nullptr;
	return client.release();
}

uint32_t NetSocket::GetSocketNumber() const
{
	return m_socket ? **m_socket : INVALID_SOCKET;
}

void NetSocket::Disconnect()
{
	m_socket.reset();
}

bool NetSocket::Connect()
{
	if (m_socket)
	{
		return ::connect(**m_socket, reinterpret_cast<const sockaddr *>(m_core->m_servAddr.get()), sizeof(SOCKADDR_IN)) != SOCKET_ERROR;
	}
	return false;
}

void NetSocket::AppendSendQueue(std::unique_ptr<BinaryBuffer> &&task)
{
	BinaryBuffer *pBuffer = task.get();

	{
		std::lock_guard<std::mutex> lock(m_lock);
		m_core->m_sendTaskQueue.push_back(std::forward<std::remove_reference<decltype(task)>::type>(task));
		//sendBufferImpl(*pBuffer);
		sendBufferImpl(*m_core->m_sendTaskQueue.front());
	}
}

void NetSocket::PostSend(stNetOverlapped *pOverl)		//Send 할 데이터가 net_buffer_max_size(512바이트) 보다 크다면, 2개 이상의 스레드에서, 이 함수(PostSend)가 처리될 수 있습니다//
{
	std::lock_guard<std::mutex> lock(m_lock);

	m_sendOverlapped->Delete(pOverl);
	while (m_core->m_sendTaskQueue.size())
	{
		auto &&task = m_core->m_sendTaskQueue.front();

		if (sendBufferImpl(*task))
			break;

		m_core->m_sendTaskQueue.pop_front();
	}
}
