
#include "mimeFileResponse.h"
#include "mimeLoader.h"
#include "mimeExtractBuffer.h"
#include "mimeFormatUtil/mimeContentFile.h"

MimeFileResponse::MimeFileResponse(const std::string &fileId)
	: HttpFileResponse()
{
	m_fileId = fileId;
}

MimeFileResponse::~MimeFileResponse()
{ }

void MimeFileResponse::setLoader(AbstractLoader *loader)
{
	m_mimeLoader = dynamic_cast<MimeLoader *>(loader);
}

bool MimeFileResponse::writeFileMeta(MimeContentFile &content)
{
	size_t contentLength = content.DestSize();

	if (!contentLength)
		return false;

	std::string ty = content.ContentType();

	if (ty.empty())
		return false;

	changeHeaderValue("Content-Length", std::to_string(contentLength));
	changeHeaderValue("Content-Type", ty);
	return true;
}

void MimeFileResponse::netPostTask(std::unique_ptr<MimeContentFile> content)
{
	std::unique_ptr<MimeExtractBuffer> mimeExtractTask(new MimeExtractBuffer);
	MimeExtractBuffer::PrinterFactory makePrinter(*mimeExtractTask);

	mimeExtractTask->SetContentFile(std::move(content));
	content->CreateContentPrinter(makePrinter);
	putSendBuffer(std::move(mimeExtractTask));
}

size_t MimeFileResponse::makeBody()
{
	std::string stateMessage;

	if (!m_mimeLoader)
		return 0;

	if (!m_mimeLoader->GetResult(stateMessage))
	{
		SetErrorStatus(HttpStatus::NOT_FOUND, stateMessage);
		return 0;
	}

	std::unique_ptr<MimeContentFile> contentFile = m_mimeLoader->GetContentFile(m_fileId);

	if (writeFileMeta(*contentFile))
	{
		size_t bodyLength = contentFile->DestSize();

		netPostTask(std::move(contentFile));
		return bodyLength;
	}
	/////serve404NotFound();
	SetErrorStatus(HttpStatus::NOT_FOUND, "SERVER 404 NOT FOUND");
	return 0;
}

