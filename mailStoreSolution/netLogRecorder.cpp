
#include "netLogRecorder.h"
#include "netLog.h"
#include "common/utils/stringhelper.h"
#include <fstream>

using namespace _StringHelper;

struct NetLogRecorder::RecorderImpl
{
	std::ofstream m_file;
};

NetLogRecorder::NetLogRecorder()
	: FlowTrace::TraceCore()
{ }

NetLogRecorder::~NetLogRecorder()
{ }

void NetLogRecorder::writeNetIssue(NetLog *issue)
{
	m_logWrt->m_file << issue->What();
}

void NetLogRecorder::Issue(std::unique_ptr<AbstractFlowIssue> issue)
{
	if (dynamic_cast<NetLog *>(issue.get()))
		writeNetIssue(static_cast<NetLog *>(issue.get()));

	///////////////////


}

FlowTrace::FlowIssue *NetLogRecorder::CreateNewIssue()
{
	return new NetLog;
}

bool NetLogRecorder::MakeFile(const std::string &url)
{
	std::ofstream file(url, std::ios::app | std::ios::binary);

	file << std::noskipws;

	if (file)
	{
		m_logWrt = std::make_unique<RecorderImpl>();
		m_logWrt->m_file = std::move(file);

		return true;
	}
	return false;
}

