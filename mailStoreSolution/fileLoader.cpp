
#include "fileLoader.h"
#include "common/include/incFilesystem.h"
#include "uriRule.h"
#include "responseFactory.h"
#include "httpFileResponse.h"
#include "common/utils/stringhelper.h"
#include <algorithm>
#include <iterator>

using namespace _StringHelper;

struct FileLoader::FileCore
{
	std::string m_filename;
	size_t m_size;
	std::string m_extension;
};

FileLoader::FileLoader(const std::string &rootPath)
	: AbstractLoader(rootPath)
{ }

FileLoader::~FileLoader()
{ }

std::string FileLoader::getExtension()
{
	if (!m_core)
		return {};

	std::string &fname = m_core->m_filename;
	size_t pos = fname.find_last_of('.');

	if (pos != std::string::npos)
	{
		std::string extension;

		extension.reserve(fname.size());
		std::transform(fname.cbegin() + (++pos), fname.cend(), std::insert_iterator<std::string>(extension, extension.begin()), [](const auto &c) { return static_cast<char>(tolower(c)); });
		return extension;
	}
	return {};
}

void FileLoader::loadImpl(const std::string &fullPath)
{ }

void FileLoader::Load(const std::string &url)
{
	m_core.reset();
	std::string fullUrl = url.empty() ? source() : stringFormat("%s\\%s", source(), url);

	if (NAMESPACE_FILESYSTEM::exists(fullUrl))
	{
		if (NAMESPACE_FILESYSTEM::is_directory(fullUrl))
			return;

		m_core = std::make_unique<FileCore>();
		m_core->m_filename = url;
		m_core->m_size = static_cast<size_t>(NAMESPACE_FILESYSTEM::file_size(fullUrl));
		m_core->m_extension = getExtension();
		loadImpl(fullUrl);
	}
}

std::unique_ptr<AbstractResponseFactory> FileLoader::CreateResponseFactory() const
{
	return std::make_unique<FileResponseFactory>();
}

std::string FileLoader::FileName() const
{
	if (m_core)
		return m_core->m_filename;

	return {};
}

std::string FileLoader::Size() const
{
	if (m_core)
		return std::to_string(m_core->m_size);

	return "0";
}

std::string FileLoader::Extension() const
{
	return (m_core) ? m_core->m_extension : "";
}

std::string FileLoader::FullName() const
{
	if (m_core)
	{
		std::string &src = m_core->m_filename;
		std::string fixPath(src.size(), 0);

		std::transform(src.cbegin(), src.cend(), fixPath.begin(), [](const auto &c) { return (c == '/') ? '\\' : c; });

		return stringFormat("%s\\%s", source(), fixPath);
	}
	return {};
}

bool FileLoader::GetProperty(FileLoader::FileAccessInfo accessKey, std::string &dest)
{
	auto retFunc = [&dest](const std::string &src) { dest = src; return true; };

	switch (accessKey)
	{
	case FileAccessInfo::EXTENSION: return retFunc(Extension());
	case FileAccessInfo::NAME: return retFunc(FileName());
	case FileAccessInfo::SIZE: return retFunc(Size());
	case FileAccessInfo::FULLNAME: return retFunc(FullName());
	default: return false;
	}
}

