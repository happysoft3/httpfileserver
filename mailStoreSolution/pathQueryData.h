
#ifndef PATH_QUERY_DATA_H__
#define PATH_QUERY_DATA_H__

#include "common/utils/tokenizer.h"
#include <map>
#include <list>

class PathQueryData : public Tokenizer
{
private:
	std::map<std::string, std::string> m_queryMap;

	///////once
	std::list<std::unique_ptr<Token>> m_identTokList;
	std::list<std::unique_ptr<Token>> m_symbTokList;

public:
	PathQueryData();
	~PathQueryData() override;

private:
	std::unique_ptr<Token> popSymb();
	std::unique_ptr<Token> popIdent();
	void build(std::unique_ptr<Token> tok);
	bool makeQuery();
	bool makeQueryMap();
	bool buildAll();
	
public:
	void Query();
	int Count() const
	{
		return m_queryMap.size();
	}
	std::string GetQueryValue(const std::string &key) const;
};

#endif

