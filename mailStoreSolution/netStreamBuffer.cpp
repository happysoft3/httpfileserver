
#include "netStreamBuffer.h"

static constexpr size_t s_max_buffer = 256;

NetStreamBuffer::NetStreamBuffer(std::unique_ptr<NetStreamBuffer> buffer)
	: BinaryBuffer()
{
	m_streamBuffer = buffer ? std::move(buffer) : nullptr;
}

NetStreamBuffer::~NetStreamBuffer()
{ }

bool NetStreamBuffer::CreateComponent()
{
	if (!m_streamBuffer)
		return false;
	
	std::vector<uint8_t> part;
	if (!m_streamBuffer->pop(s_max_buffer, part))
		Clear();
	else
		StreamSet(part);

	return true;
}

bool NetStreamBuffer::pop(const size_t req, std::vector<uint8_t> &dest)
{
	if (m_bufferRd >= Size())
	{
		dest.clear();
		return false;
	}

	size_t availableSize = (m_bufferRd + req > Size()) ? Size() - m_bufferRd : req;

	dest.resize(availableSize);
	std::copy_n(Begin() + m_bufferRd, availableSize, dest.begin());
	m_bufferRd += availableSize;

	return true;
}

