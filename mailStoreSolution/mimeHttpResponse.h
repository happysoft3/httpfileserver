
#ifndef MIME_HTTP_RESPONSE_H__
#define MIME_HTTP_RESPONSE_H__

#include "httpHtmlResponse.h"

class MimeLoader;

class MimeHttpResponse : public HttpHtmlResponse
{
private:
	MimeLoader *m_mimeLoader;

public:
	MimeHttpResponse();
	~MimeHttpResponse() override;

private:
	void dispositionColumns() override;
	void setLoader(AbstractLoader *loader) override;
	size_t makeBody() override;
};

#endif

