
#include "httpServer.h"
#include "netbuffer.h"
#include "netsocket.h"
#include "systemFileParser/iniFileMan.h"
#include "abstractResponseFactory.h"
#include "directoryLoader.h"
#include "invalidLoader.h"
#include "httpResponse.h"
#include "common/utils/flowTrace.h"
#include "netlog.h"
#include "netLogRecorder.h"
#include "uriRule.h"
#include "common/utils/stringhelper.h"

#pragma comment(lib, "systemFileParser.lib")

static constexpr char system_file_name[] = "setting.ini";
static constexpr char system_file_file_section[] = "PATH";

using namespace _StringHelper;

HttpServer::HttpServer()
	: NetServer()
{ }

HttpServer::~HttpServer()
{ }

bool HttpServer::loadRootPath()
{
	static constexpr char root_path_key[] = "RootPath";
	std::unique_ptr<IniFileMan> iniFile(new IniFileMan);

	if (!iniFile->ReadIni(system_file_name))
		return false;

	std::string rootPath;

	if (!iniFile->GetItemValue(system_file_file_section, root_path_key, rootPath))
		return false;

	m_rootPath = rootPath;
	m_iniFile = std::move(iniFile);
	return true;
}

bool HttpServer::initServer()
{
	//if (logInit())
		return loadRootPath();

	//return false;
}

void HttpServer::onStartup()
{
	createServerLog(std::make_unique<NetLog>("void HttpServer::onStartup()"));
}

void HttpServer::onDisconnected(NetSocket *client)
{
	auto issue = std::make_unique<NetLog>();
	
	issue->SetIssueName(stringFormat("%d client is disconnect", client->GetSocketNumber()));
	createServerLog(std::move(issue));
}

void HttpServer::onJoin(const NetSocket &client)
{
	auto note = std::make_unique<NetLog>();

	note->SetIssueName(stringFormat("add %d client", client.GetSocketNumber()));
	createServerLog(std::move(note));
}

std::unique_ptr<AbstractTraceCore> HttpServer::makeLogRecorder()
{
	auto record = std::make_unique<NetLogRecorder>();

	if (record->MakeFile("testlog.txt"))
		return record;

	return {};
}

std::unique_ptr<AbstractLoader> HttpServer::createLoader(std::unique_ptr<UriRule> uri)
{
	std::string isError;

	if (uri->GetError(isError))
		return std::make_unique<InvalidLoader>();

	//if (uri->QueryValue("fmt") == "json")
	//	m_jsonResp = std::make_unique<HttpJsonResponse>();

	std::string resourcePath = uri->ResourcePath();
	std::unique_ptr<DirectoryLoader> loader(new DirectoryLoader(m_rootPath));

	loader->SetUri(std::move(uri));
	loader->SetLoadingSubDirectories(true);
	loader->Load(resourcePath);
	auto child = loader->FetchSingleChild();

	if (child)
	{
		//child->Load(resourcePath);
		return child;
	}

	return loader;
}

std::unique_ptr<AbstractLoader> HttpServer::readReceiveData(NetSocket *client, std::unique_ptr<NetBufferData> buffer)
{
	static constexpr char error_path[] = "ERROR";
	std::string method, uri;

	//여기에서, 수신기록을 저장합니다//
	auto recvIssue = std::make_unique<NetLog>();

	recvIssue->SetIssueName(stringFormat("%s (from %d)", buffer->GetFrontMessage(), client->GetSocketNumber()));
	createServerLog(std::move(recvIssue));
	////여기까지, 수신기록을 저장합니다//

	buffer->GetRequestMethod(method);		//버퍼에서 METHOD 가져오기
	buffer->GetRequestURI(uri);			///버퍼에서 URI 가져오기

	////////변경 루틴//////////////

	auto rule = std::make_unique<UriRule>();

	rule->StreamSet(uri);
	return createLoader(std::move(rule));
}

void HttpServer::onReceive(NetSocket *client, std::unique_ptr<NetBufferData> buffer)
{
	std::unique_ptr<AbstractLoader> loader = readReceiveData(client, std::move(buffer));		//오류날 시, InvalidLoader 을 반환합니다
	auto respFactory = loader->CreateResponseFactory();
	
	respFactory->Create();

	std::unique_ptr<HttpResponse> resp = respFactory->Release();  //여기까지
	std::string stream;

	resp->BindLoader(std::move(loader));
	resp->Build();
	resp->GetStream(stream);

	std::unique_ptr<BinaryBuffer> task = resp->ReleaseBuffer();

	client->Send(stream);
	if (task)
		client->AppendSendQueue(std::move(task));
}

