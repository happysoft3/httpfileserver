
#include "invalidLoader.h"
#include "responseFactory.h"

InvalidLoader::InvalidLoader()
	: AbstractLoader({})
{ }

InvalidLoader::~InvalidLoader()
{ }

std::unique_ptr<AbstractResponseFactory> InvalidLoader::CreateResponseFactory() const
{
	return std::make_unique<FileResponseFactory>();
}
