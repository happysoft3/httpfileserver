
#include "workThreadMan.h"
#include <thread>

static constexpr int s_work_thread_count = 3;

struct WorkThreadMan::WorkerIdentifier
{
	std::shared_ptr<bool> m_state;
	int m_testId;

};

WorkThreadMan::WorkThreadMan()
{ }

WorkThreadMan::~WorkThreadMan()
{
	Terminate();
}

bool WorkThreadMan::workThreadProc(std::unique_ptr<WorkerIdentifier> ident)
{
	while (*ident->m_state)
	{
		if (!m_task())
			return false;
	}
	return true;
}

void WorkThreadMan::SetTask(WorkThreadMan::worker_task_ty &&task, WorkThreadMan::terminate_task_ty &&shutdwTask)
{
	m_task = std::forward<worker_task_ty>(task);
	m_terminateTask = std::forward<terminate_task_ty>(shutdwTask);
}

bool WorkThreadMan::Place()
{
	if (!m_checkRunning.expired())
		return false;	//아직, 남아있는 스레드가 있음..

	std::shared_ptr<bool> checker(new bool(true));
	m_checkRunning = checker;

	for (int rep = s_work_thread_count; rep > 0; --rep)
	{
		WorkerIdentifier *ident(new WorkerIdentifier({}));

		ident->m_state = checker;
		ident->m_testId = rep;
		std::packaged_task<bool()> task([this, ident = ident]() { auto p = std::unique_ptr<WorkerIdentifier>(ident); return this->workThreadProc(std::move(p)); });
		m_resultVec.push_back(task.get_future());
		std::thread th(std::move(task));

		th.detach();
	}
	return true;
}

void WorkThreadMan::Terminate()
{
	auto checker = m_checkRunning.expired() ? nullptr : m_checkRunning.lock();

	if (checker)
		*checker = false;
	else
		return;

	if (m_terminateTask)
		m_terminateTask(m_resultVec.size());
	for (auto && res : m_resultVec)
		res.get();
}


