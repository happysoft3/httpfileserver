
#ifndef HTTP_SERVER_H__
#define HTTP_SERVER_H__

#include "netServer.h"
#include <map>

class IniFileMan;
class NetBufferData;
class NetSocket;
class HttpResponse;
class AbstractLoader;
class UriRule;

class HttpServer : public NetServer
{
private:
	std::unique_ptr<IniFileMan> m_iniFile;
	std::string m_rootPath;
	std::unique_ptr<HttpResponse> m_jsonResp;
	std::map<std::string, std::function<std::unique_ptr<HttpResponse>()>> m_extensionInstanceMap;

public:
	HttpServer();
	~HttpServer() override;

private:
	bool loadRootPath();
	bool initServer() override;
	void onStartup() override;
	void onDisconnected(NetSocket *client) override;
	void onSend(NetSocket *client) override
	{ }
	void onJoin(const NetSocket &client) override;
	std::unique_ptr<AbstractTraceCore> makeLogRecorder() override;

private:
	std::unique_ptr<AbstractLoader> createLoader(std::unique_ptr<UriRule> uri);
	std::unique_ptr<AbstractLoader> readReceiveData(NetSocket *client, std::unique_ptr<NetBufferData> buffer);
	void onReceive(NetSocket *client, std::unique_ptr<NetBufferData> buffer) override;
};

#endif

