
#include "httpFileResponse.h"
#include "fileLoader.h"
#include "netfilebuffer.h"
#include "common/include/incFilesystem.h"
#include <sstream>

static std::map<std::string, std::string> s_mimeTypeMap =
{
#define MIMETYPE(key, value) {key,value},
#include "common/identifier/mimeTypes.inc"
#undef MIMETYPE
};

HttpFileResponse::HttpFileResponse()
	: HttpResponse()
{
	m_fileLoader = nullptr;
}

HttpFileResponse::~HttpFileResponse()
{ }

void HttpFileResponse::setLoader(AbstractLoader *loader)
{
	m_fileLoader = dynamic_cast<FileLoader *>(loader);
}

bool HttpFileResponse::writeFilesize()
{
	std::string fSize;

	if (!m_fileLoader->GetProperty(FileLoader::FileAccessInfo::SIZE, fSize))
		return false;

	changeHeaderValue("Content-Length", fSize);
	return true;
}

bool HttpFileResponse::writeContentType()
{
	std::string extension;

	if (!m_fileLoader->GetProperty(FileLoader::FileAccessInfo::EXTENSION, extension))
		return false;

	if (extension.empty())
		return false;

	std::string mimeTy = getMimeType(extension);

	if (mimeTy.empty())
		return false;

	/////!FIXME!//// 여기서 타입에 따라, "charset=utf-8\r\n";" 을 붙여야 될지도 모름..////////
	changeHeaderValue("Content-Type", mimeTy);

	return true;
}

size_t HttpFileResponse::putFileContentLength()
{
	std::string fsize = headerValue("Content-Length");
	std::stringstream ss(fsize);
	size_t ret = 0;

	ss >> ret;
	return ret;
}

bool HttpFileResponse::writeContent()
{
	std::string fileUrl;

	if (!m_fileLoader->GetProperty(FileLoader::FileAccessInfo::FULLNAME, fileUrl))
		return false;

	if (fileUrl.empty())
		return false;

	if (!NAMESPACE_FILESYSTEM::exists(fileUrl))
		return false;
	std::string length = headerValue("Content-Length");
	////여기에서 파일버퍼를 생성합니다, 이것은 서버에 의해 예약됩니다////
	std::unique_ptr<NetFileBuffer> fileBuffer(new NetFileBuffer(fileUrl, length));

	if (fileBuffer->MakePrinter())
	{
		putSendBuffer(std::move(fileBuffer));
		return true;
	}
	return false;
}

std::string HttpFileResponse::getMimeType(const std::string &extension)
{
	auto typeIterator = s_mimeTypeMap.find(extension);

	return (typeIterator != s_mimeTypeMap.cend()) ? typeIterator->second : "";
}

bool HttpFileResponse::makeFileFormat()
{
	if (!writeFilesize())
		return false;

	if (!writeContentType())
		return false;

	//여기에서 정지// return true 하고 나가라는 의미!!//
	//헤더 먼저 Send 보내고, 나머지는 SendPost에 의해 처리하겠음//
	return writeContent();
}

size_t HttpFileResponse::makeBody()
{
	if (m_fileLoader)
	{
		if (makeFileFormat())
			return putFileContentLength();
		/////serve404NotFound();
		SetErrorStatus(HttpStatus::NOT_FOUND, "SERVER 404 NOT FOUND");
	}
	return 0;
}

