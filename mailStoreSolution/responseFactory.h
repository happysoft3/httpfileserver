
#ifndef RESPONSE_FACTORY_H__
#define RESPONSE_FACTORY_H__

#include "abstractResponseFactory.h"
#include <string>

class FileResponseFactory : public AbstractResponseFactory
{
public:
	FileResponseFactory();
	~FileResponseFactory() override;

private:
	std::unique_ptr<HttpResponse> createImpl() override;
};

class DirectoryResponseFactory : public AbstractResponseFactory
{
private:
	std::string m_option;

public:
	DirectoryResponseFactory(const std::string &option);
	~DirectoryResponseFactory() override;

private:
	std::unique_ptr<HttpResponse> createImpl() override;
};

class MimeResponseFactory : public AbstractResponseFactory
{
private:
	std::string m_attachKey;

public:
	MimeResponseFactory(const std::string &attachKey);
	~MimeResponseFactory() override;

private:
	std::unique_ptr<HttpResponse> createImpl() override;
};

#endif

