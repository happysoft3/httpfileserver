
#ifndef MIME_EXTRACT_BUFFER_H__
#define MIME_EXTRACT_BUFFER_H__

#include "common/utils/binaryBuffer.h"
#include "mimeFormatUtil/mimeContentStreamPrinter.h"

class SourceFile;
class MimeContentFile;

class MimeExtractBuffer : public BinaryBuffer
{
private:
	class Core;

public:
	class Printer;
	class PrinterFactory;

private:
	SourceFile *m_file;
	std::unique_ptr<MimeContentFile> m_contentFile;
	std::shared_ptr<Core> m_core;

public:
	MimeExtractBuffer();
	~MimeExtractBuffer() override;

private:
	void sendMimeFile();
	bool CreateComponent() override;

public:
	void SetContentFile(std::unique_ptr<MimeContentFile> contentFile);
};

class MimeExtractBuffer::Printer : public MimeContentStreamPrinter
{
private:
	std::weak_ptr<Core> m_parent;
	std::vector<char> m_writeBuffer;
	size_t m_accumulate;

public:
	Printer(std::shared_ptr<Core> parent);
	~Printer() override;

private:
	bool Create(const std::string &url) override;
	size_t Seekpointer() const override;
	size_t Write(const char *src, size_t writeBytes) override;
};

class MimeExtractBuffer::PrinterFactory : public MimePrinterFactory
{
private:
	std::weak_ptr<Core> m_parent;

public:
	PrinterFactory(MimeExtractBuffer &buffer);
	~PrinterFactory();

private:
	std::unique_ptr<MimeContentStreamPrinter> Create() const override;
};

#endif

