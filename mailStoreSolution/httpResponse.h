


#ifndef HTTP_RESPONSE_H__
#define HTTP_RESPONSE_H__

#include <map>
#include <list>
#include <string>
#include <memory>

class AbstractLoader;
class BinaryBuffer;

enum class HttpVersion
{
	HTTP0_9, HTTP1_0, HTTP1_1, HTTP2, HTTP3
};

struct HttpStatus
{
	enum State
	{
#define STATUS(code, id, str) id,
#include "common/identifier/httpStatusCode.inc"
#undef STATUS
	};
};

class HttpResponse
{
public: class ResponseToken;

private:
	std::map<std::string, std::string> m_httpHeaderMap;
	std::list<std::string> m_httpHeaderKeyList;

	using resp_tok_ty = std::unique_ptr<ResponseToken>;
	std::list<resp_tok_ty> m_writeLine;
	std::list<resp_tok_ty> m_bodyWriteLine;
	HttpStatus::State m_responseState;
	std::string m_errorMessage;
	std::unique_ptr<AbstractLoader> m_loader;
	std::unique_ptr<BinaryBuffer> m_sendBuffer;

public:
	HttpResponse();
	virtual ~HttpResponse();

private:
	void insert(const std::string &key, const std::string &value);
	void loadBuiltinsHeader();

protected:
	void changeHeaderValue(const std::string &key, const std::string &value, bool forced=false);
	void pushLineReturn(bool consistCarriageReturn = true);
	void pushToken(const std::string &value);

public:
	void BindLoader(std::unique_ptr<AbstractLoader> &&loader);

private:
	virtual void setLoader(AbstractLoader */*loader*/) {}

private:
	////
	void writeVersion();
	void writeStatusCode();
	void writeStatusMessage();
	void writeStatusLine();

public:
	std::unique_ptr<BinaryBuffer> ReleaseBuffer();

protected:
	void putSendBuffer(std::unique_ptr<BinaryBuffer> &&buffer);
	std::string headerValue(const std::string &key);

private:
	void beforeWriteHeader();

protected:
	size_t calcBodySize();

private:
	size_t makeError();
	virtual size_t makeBody();
	void writeHeader();
	void writeBody();

	void releaseStream(std::string &dest, size_t reserv = 0);

public:
	static int StatusErrorCode(HttpStatus::State state);
	static std::string StatusErrorMessage(HttpStatus::State state);
	void Build();
	void GetStream(std::string &dest);
	void SetErrorStatus(HttpStatus::State state, const std::string &errorMessage);
};

#endif

