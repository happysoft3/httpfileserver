
#include "clientMap.h"
#include "netBuffer.h"
#include "netsocket.h"
#include <ws2tcpip.h>

using retrieve_method_arg_ty = ClientMap::retrieve_method_ty::argument_type;
static auto s_retrieve_method_nothing = [](retrieve_method_arg_ty) {};

struct ClientMap::ClientCore
{
	std::unique_ptr<NetSocket> m_sock;
	std::unique_ptr<NetBuffer> m_buffer;
};

ClientMap::ClientMap()
{ }

ClientMap::~ClientMap()
{ }

std::shared_ptr<ClientMap::server_cb_proto_ty> ClientMap::SetServerCallback(ClientMap::server_cb_proto_ty &&servCb)
{
	std::shared_ptr<server_cb_proto_ty> pServCb(new server_cb_proto_ty(std::forward<server_cb_proto_ty>(servCb)));

	m_servCb = pServCb;
	return pServCb;
}

bool ClientMap::getSocketId(const NetSocket &client, uint32_t &idDest)
{
	if (!client.IsValid())
		return false;

	idDest = client.GetSocketNumber();
	return true;
}

ClientMap::ClientCore *ClientMap::makeClientCore(std::unique_ptr<NetSocket> client)
{
	ClientCore *core = new ClientCore;
	auto buffer = std::make_unique<NetBuffer>();
	uint32_t sockid = client->GetSocketNumber();

	buffer->SetAlert([sockid = sockid, this](std::unique_ptr<NetBufferData> netData) { this->bufferAnnounce(sockid, std::move(netData)); });
	core->m_buffer = std::move(buffer);
	core->m_sock.swap(client);
	core->m_sock->ReservReceive();
	return core;
}

ClientMap::ClientCore *ClientMap::retrieveImpl(const NetSocket &client, std::function<void(ClientMap::client_map_ty::iterator&)> &&method = s_retrieve_method_nothing)
{
	uint32_t sockId = INVALID_SOCKET;

	if (!getSocketId(client, sockId))
		return nullptr;

	{
		std::lock_guard<decltype(m_lock)> lock(m_lock);
		auto clientIterator = m_clients.find(sockId);
		if (clientIterator != m_clients.cend())
		{
			std::unique_ptr<ClientCore> &cc = clientIterator->second;

			method(clientIterator);
			return cc.get();
		}
	}
	return nullptr;
}

ClientMap::ClientCore *ClientMap::retrieve(const NetSocket &client)
{
	return retrieveImpl(client);
}

ClientMap::ClientCore *ClientMap::retrieveById(uint32_t sockId) const
{
	auto keyIterator = m_clients.find(sockId);

	return (keyIterator == m_clients.cend()) ? nullptr : keyIterator->second.get();
}

bool ClientMap::searching(const NetSocket &client)
{
	return retrieve(client) ? true : false;
}

void ClientMap::bufferAnnounce(uint32_t sockId, std::unique_ptr<NetBufferData> netData)
{
	auto cc = retrieveById(sockId);

	if (!cc) return;

	auto servCb = m_servCb.expired() ? nullptr : m_servCb.lock();

	if (servCb)
		(*servCb)(cc->m_sock.get(), std::move(netData));
}

void ClientMap::Add(std::unique_ptr<NetSocket> client)
{
	uint32_t sockId = INVALID_SOCKET;

	if (!getSocketId(*client, sockId))
		return;

	if (!searching(*client))
	{
		{
			std::lock_guard<decltype(m_lock)> lock(m_lock);

			m_clients.emplace(sockId, std::unique_ptr<ClientCore>(makeClientCore(std::move(client))));
		}
	}
}

void ClientMap::Remove(NetSocket &client)
{
	retrieveImpl(client, [this](retrieve_method_arg_ty pr) { this->m_clients.erase(pr); });
}

std::unique_ptr<NetSocket> ClientMap::Fetch(const NetSocket &client)
{
	uint32_t sockId = 0;

	if (!getSocketId(client, sockId))
		return {};

	{
		std::lock_guard<std::recursive_mutex> lock(m_lock);
		auto clientIterator = m_clients.find(sockId);

		if (clientIterator != m_clients.cend())
		{
			std::unique_ptr<NetSocket> discardSocket = std::move(clientIterator->second->m_sock);

			m_clients.erase(clientIterator);
			return discardSocket;
		}
	}
	return {};
}

NetBuffer *ClientMap::GetBuffer(NetSocket &client)
{
	std::lock_guard<std::recursive_mutex> lock(m_lock);
	auto cc = retrieve(client);

	return cc ? cc->m_buffer.get() : nullptr;
}
