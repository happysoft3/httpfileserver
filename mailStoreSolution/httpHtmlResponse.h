
#ifndef HTTP_HTML_RESPONSE_H__
#define HTTP_HTML_RESPONSE_H__

#include "httpResponse.h"
#include <stack>

class AbstractLoader;
class DirectoryLoader;
class HtmlListModel;

class HttpHtmlResponse : public HttpResponse
{
	class ScopedHtml;
	class ListColumn;

private:
	std::stack<std::string> m_htmlStack;
	DirectoryLoader *m_dirLoader;
	std::unique_ptr<HtmlListModel> m_model;

	////Columns
	std::list<std::unique_ptr<ListColumn>> m_columns;

public:
	HttpHtmlResponse();
	~HttpHtmlResponse() override;

protected:
	void setListmodel(std::unique_ptr<HtmlListModel> &&model);

private:
	void setLoader(AbstractLoader *loader) override;
	void putHtmlMarkup();
	void pushUnkeyword(const std::string &keyword);
	std::unique_ptr<ScopedHtml> makeScope(bool scope);
	std::unique_ptr<ScopedHtml> putHtmlKeyword(const std::string &keyword, bool scope = false);
	std::unique_ptr<ScopedHtml> putHtmlKeywordProperty(const std::string &keyword, const std::string &key, const std::string &value, bool scope = false);
	std::unique_ptr<ScopedHtml> putHtmlKeywordWithProperty(const std::string &keyword, std::list<std::pair<std::string, std::string>> &&properties, bool scope = false);
	bool popHtmlKeyword();
	void putTitle(const std::string &titleMessage);
	void putCotentTitle(const std::string &cotentTitleMessage);
	void putExplanation(const std::string &misc);
	void putTableColumnSingle(const std::string &columnName);
	void tableColSpan();
	void putTableImage(const std::string &imgUrl = {});

	//Column features
	void columnPutImage(const std::string &key);
	void columnPutTextWithUrl(const std::string &key);
	void columnPutText(const std::string &key);
	virtual void dispositionColumns();

protected:
	struct ColumnType
	{
		enum Type
		{
			TEXTURL,
			TEXT,
			IMAGE
		};
	};
	void declareColumn(const std::string &key, ColumnType::Type ty);
	///

	void putTableElementSingle();
	void putTableElement();
	void putTableColumn();
	void putTable();
	void putBody();
	void makeHtml();

protected:
	size_t makeHtmlCommon();

private:
	size_t makeBody() override;
};

#endif

