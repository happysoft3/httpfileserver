
#ifndef ABSTRACT_LOADER_H__
#define ABSTRACT_LOADER_H__

#include <string>

class UriRule;
class AbstractResponseFactory;

class AbstractLoader
{
private:
	const std::string m_source;
	std::unique_ptr<UriRule> m_uriTarget;

public:
	explicit AbstractLoader(const std::string &source);
	virtual ~AbstractLoader();

protected:
	std::string source() const
	{
		return m_source;
	}

public:
	virtual void Load(const std::string &) = 0;
	virtual bool IsDirectory() const
	{
		return false;
	}
	virtual std::string FileName() const = 0;
	virtual std::string Size() const
	{
		return {};
	}
	size_t SizeInt() const;
	virtual std::string URL() const;
	void SetUri(std::unique_ptr<UriRule> uri);

protected:
	UriRule *uriTarget() const
	{
		return m_uriTarget.get();
	}

private:
	std::unique_ptr<UriRule> createUri(AbstractLoader &loader);

public:
	void FetchUri(AbstractLoader &loader, const std::string &addPath = {});
	virtual std::unique_ptr<AbstractResponseFactory> CreateResponseFactory() const = 0;
	virtual std::string Extension() const
	{
		return {};
	}

	virtual bool Valid() const
	{
		return true;
	}

private:
	std::string encoding(const std::string &s) const;

public:
	std::string EncFileName() const;
	std::string EncURL();
};

#endif

