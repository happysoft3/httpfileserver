
#include "abstractLoader.h"
#include "common/utils/urlString.h"
#include "common/include/incFilesystem.h"
#include "uriRule.h"
#include "common/utils/stringhelper.h"
#include <sstream>

using namespace _StringHelper;

AbstractLoader::AbstractLoader(const std::string &source)
	: m_source(source)
{ }

AbstractLoader::~AbstractLoader()
{ }

size_t AbstractLoader::SizeInt() const
{
	std::string &&src = Size();

	if (src.empty())
		return 0;

	std::stringstream ss(src);
	size_t sz = 0;

	ss >> sz;
	return sz;
}

std::string AbstractLoader::URL() const
{
	if (!m_uriTarget)
		return {};

	return m_uriTarget->FullUri();
}

void AbstractLoader::SetUri(std::unique_ptr<UriRule> uri)
{
	if (uri)
		m_uriTarget = std::move(uri);
}

std::unique_ptr<UriRule> AbstractLoader::createUri(AbstractLoader &loader)
{
	if (!loader.m_uriTarget)
		return {};

	auto uri = std::make_unique<UriRule>();

	uri->FetchFromOtherUri(*loader.m_uriTarget);
	return uri;
}

void AbstractLoader::FetchUri(AbstractLoader &loader, const std::string &addPath)
{
	std::unique_ptr<UriRule> uri = createUri(loader);

	if (!uri)
		return;

	m_uriTarget.swap(uri);

	if (addPath.empty())
		return;

	m_uriTarget->AddPath(addPath);
}

std::string AbstractLoader::encoding(const std::string &s) const
{
	NAMESPACE_FILESYSTEM::path url(s);

	return url.u8string();
}

std::string AbstractLoader::EncFileName() const
{
	return encoding(FileName());
}

std::string AbstractLoader::EncURL()
{
	UrlString url;

	url.StreamSet(URL());

	std::string put;
	url.Encode(put);
	return put;


	//return URL();		//이걸로 하면, 제대로 되긴함
}
