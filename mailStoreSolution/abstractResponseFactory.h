
#ifndef ABSTRACT_RESPONSE_FACTORY_H__
#define ABSTRACT_RESPONSE_FACTORY_H__

#include <memory>

class HttpResponse;

class AbstractResponseFactory
{
private:
	std::unique_ptr<HttpResponse> m_response;

public:
	AbstractResponseFactory();
	virtual ~AbstractResponseFactory();

private:
	virtual std::unique_ptr<HttpResponse> createImpl() = 0;

public:
	void Create();
	std::unique_ptr<HttpResponse> Release();
};

#endif

