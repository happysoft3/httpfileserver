
#include "mimeHttpResponse.h"
#include "mimeLoader.h"
#include "htmlListModel.h"

MimeHttpResponse::MimeHttpResponse()
	: HttpHtmlResponse()
{
	m_mimeLoader = nullptr;
}

MimeHttpResponse::~MimeHttpResponse()
{ }

void MimeHttpResponse::dispositionColumns()
{
	declareColumn("Icon", ColumnType::IMAGE);
	declareColumn("ContentType", ColumnType::TEXT);
	declareColumn("Extension", ColumnType::TEXT);
	declareColumn("Encoding", ColumnType::TEXT);
	declareColumn("Disposition", ColumnType::TEXTURL);
	declareColumn("NodeIndex", ColumnType::TEXT);
	declareColumn("ContentId", ColumnType::TEXT);
	declareColumn("ContentTypeSubKey", ColumnType::TEXT);
	declareColumn("ContentLine", ColumnType::TEXT);
	declareColumn("ContentEndLine", ColumnType::TEXT);
	//declareColumn("ContentTypeSubValue", ColumnType::TEXT);
}

void MimeHttpResponse::setLoader(AbstractLoader *loader)
{
	m_mimeLoader = dynamic_cast<MimeLoader *>(loader);
}

size_t MimeHttpResponse::makeBody()
{
	if (!m_mimeLoader)
		return 0;

	std::string stateMessage;

	if (!m_mimeLoader->GetResult(stateMessage))
	{
		SetErrorStatus(HttpStatus::NOT_FOUND, stateMessage);
		return 0;
	}

	setListmodel(m_mimeLoader->CreateModel());
	return makeHtmlCommon();
}

