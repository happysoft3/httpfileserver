
#ifndef NET_STREAM_BUFFER_H__
#define NET_STREAM_BUFFER_H__

#include "common/utils/binaryBuffer.h"

class NetStreamBuffer : public BinaryBuffer
{
private:
	std::unique_ptr<NetStreamBuffer> m_streamBuffer;
	size_t m_bufferRd;

public:
	NetStreamBuffer(std::unique_ptr<NetStreamBuffer> buffer = nullptr);
	~NetStreamBuffer() override;

private:
	bool CreateComponent() override;
	//요청한 크기와, 받을 벡터입니다
	bool pop(const size_t reqSize, std::vector<uint8_t> &dest);	//읽기버퍼 전용
};

#endif

