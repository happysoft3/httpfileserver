
#include "mimeLoader.h"
#include "responseFactory.h"
#include "uriRule.h"
#include "mimeFormatUtil/mimeReader.h"
#include "mimeFormatUtil/mimePackage.h"
//#include "mimeFormatUtil/mimeFileExtractor.h"
//#include "mimeFormatUtil/mimeAttachPrinter.h"
#include "mimeFormatUtil/mimeAttach.h"

#include "htmlListModel.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

#pragma comment(lib, "mimeFormatUtil.lib")

static constexpr char s_attach_key[] = "attach";

struct MimeLoader::ParseResult
{
	std::unique_ptr<MimePackage> m_package;
	std::unique_ptr<MimeReader> m_reader;
	std::string m_lastError;
};

class MimeLoader::MimeListModel : public HtmlListModel
{
	using PackageItemList = MimePackage::PackageItemList;

private:
	std::string m_baseUri;
	std::unique_ptr<PackageItemList> m_itemList;

public:
	MimeListModel(std::unique_ptr<PackageItemList> itemList)
		: m_itemList(std::move(itemList))
	{ }
	~MimeListModel() override
	{ }

private:
	void InitialDeclare() override
	{
		declareKeyHandler("Icon", [this]() { return this->Image(); });
		declareKeyHandler("ContentType", [this]() { return this->getContentData(MimePackage::PackageItem::ItemType::CONTENT_TYPE); });
		declareKeyHandler("Extension", [this]() { return this->getContentData(MimePackage::PackageItem::ItemType::CONTENT_EXTENSION); });
		declareKeyHandler("Encoding", [this]() { return this->getContentData(MimePackage::PackageItem::ItemType::ENCODE_TYPE); });
		declareKeyHandler("Disposition", [this]() { return this->getContentData(MimePackage::PackageItem::ItemType::DISPOSITION); });
		pushSubKey("Disposition", [this]() { return this->URL(); });
		declareKeyHandler("NodeIndex", [this]() { return this->getContentData(MimePackage::PackageItem::ItemType::ITEM_ID); });
		declareKeyHandler("ContentId", [this]() { return this->getContentData(MimePackage::PackageItem::ItemType::CONTENT_ID); });
		declareKeyHandler("ContentTypeSubKey", [this]() { return this->getContentData(MimePackage::PackageItem::ItemType::CONTENT_TYPE_SUBKEY); });
		declareKeyHandler("ContentTypeSubValue", [this]() { return this->getContentData(MimePackage::PackageItem::ItemType::CONTENT_TYPE_SUBVALUE); });
		declareKeyHandler("ContentLine", [this]() { return this->getContentData(MimePackage::PackageItem::ItemType::FIELD_START_LINE); });
		declareKeyHandler("ContentEndLine", [this]() { return this->getContentData(MimePackage::PackageItem::ItemType::FIELD_END_LINE); });
	}

	std::string getContentData(MimePackage::PackageItem::ItemType::Ty ty) const
	{
		switch (ty)
		{
		case MimePackage::PackageItem::ItemType::CONTENT_TYPE:
		case MimePackage::PackageItem::ItemType::CONTENT_EXTENSION:
		case MimePackage::PackageItem::ItemType::ENCODE_TYPE:
		case MimePackage::PackageItem::ItemType::DISPOSITION:
		case MimePackage::PackageItem::ItemType::ITEM_ID:
		case MimePackage::PackageItem::ItemType::CONTENT_ID:
		case MimePackage::PackageItem::ItemType::CONTENT_TYPE_SUBKEY:
		case MimePackage::PackageItem::ItemType::CONTENT_TYPE_SUBVALUE:
		case MimePackage::PackageItem::ItemType::FIELD_START_LINE:
		case MimePackage::PackageItem::ItemType::FIELD_END_LINE:
		case MimePackage::PackageItem::ItemType::ITEM_URL:
			return m_itemList->Get(&MimePackage::PackageItem::GetMetaData, ty);

		default:
			return {};
		}
	}

	std::string URL() const
	{
		//std::string uri = getContentData(MimePackage::PackageItem::ItemType::ITEM_ID);  //우선은, content-id 으로 접근//		//인코딩 해야합니다//
		std::string uri = getContentData(MimePackage::PackageItem::ItemType::ITEM_URL);

		return stringFormat("%s?%s=%s", m_baseUri, s_attach_key, uri);
	}

	std::string Image() const
	{
		return "/icons/link.gif";
	}

	bool Has() const override
	{
		return m_itemList->IsEnd();
	}

	void Next() override
	{
		m_itemList->Next();
	}

public:
	void SetBaseUri(const std::string &uri)
	{
		m_baseUri = uri;
	}
};

using parse_result_ty = MimeLoader::parse_result_ty;

MimeLoader::MimeLoader(const std::string &rootPath)
	: FileLoader(rootPath)
{ }

MimeLoader::~MimeLoader()
{ }

parse_result_ty MimeLoader::mimeParse(std::unique_ptr<MimeReader> reader)
{
	std::unique_ptr<MimePackage> package = reader->Parse();
	parse_result_ty result(new ParseResult);

	if (reader->LastError(result->m_lastError))
	{
		result->m_package = std::move(package);
		result->m_reader = std::move(reader);
	}

	return result;
}

bool MimeLoader::GetResult(std::string &state)
{
	if (!m_parseResult)
		return false;

	parse_result_ty result = m_parseResult->get();		//여기서, 본선합류//
	m_parseResult.reset();

	bool ok = result->m_package ? true : false;

	state = ok ? "OK" : result->m_lastError;
	if (ok)
	{
		m_mimePackage = std::move(result->m_package);
		m_mimePackage->MakeFileKeymap();	//added on Thursday, 4 August 2022
	}
	return ok;
}

std::unique_ptr<AbstractResponseFactory> MimeLoader::CreateResponseFactory() const
{
	return std::make_unique<MimeResponseFactory>(uriTarget()->QueryValue(s_attach_key));
}

void MimeLoader::loadImpl(const std::string &fullPath)
{
	auto reader = std::make_unique<MimeReader>();

	reader->AddFile(fullPath);

	//이 작업은 지연가능합니다, 그러므로 기본 시동방침을, 적용합니다
	auto result = new std::future<parse_result_ty>(std::async(std::launch::async | std::launch::deferred, 
		[](std::unique_ptr<MimeReader> mimeReader) { return MimeLoader::mimeParse(std::move(mimeReader)); }, std::move(reader)));

	m_parseResult = std::unique_ptr<std::future<parse_result_ty>>(result);	//파싱을 시작합니다

}

std::unique_ptr<HtmlListModel> MimeLoader::CreateModel()
{
	auto itemList = m_mimePackage->MakeAsList();
	MimeListModel *listModel = new MimeListModel(std::move(itemList));

	listModel->SetBaseUri(URL());
	return std::unique_ptr<HtmlListModel>(listModel);
}

std::unique_ptr<MimeAttach> MimeLoader::FetchAttach(const std::string &key)
{
	if (!m_mimePackage)
		return {};

	std::string innerKey = m_mimePackage->RetrieveKeyWithFilename(key);

	if (innerKey.empty())	//파일명으로 찾았지만 없으면, 노드 id 값으로 판단//
		innerKey = key;

	return m_mimePackage->CreateAttach(innerKey);
}
