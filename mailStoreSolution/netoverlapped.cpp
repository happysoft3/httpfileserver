
#include "netoverlapped.h"

NetOverlapped::NetOverlapped()
{ }

NetOverlapped::~NetOverlapped()
{ }

void NetOverlapped::bufferClean(stNetOverlapped *pOverlapped)
{
	pOverlapped->m_message.fill(0);
}

void NetOverlapped::bufferCopy(stNetOverlapped *pOverlapped, const std::string &src)
{
	std::copy(src.cbegin(), src.cend(), pOverlapped->m_message.begin());
	pOverlapped->m_wsaBuffer.len = src.length();
}

stNetOverlapped *NetOverlapped::createCommon(std::function<void(stNetOverlapped*)> &&init)
{
	stNetOverlapped *pOverlapped = new stNetOverlapped({});

	pOverlapped->m_wsaBuffer.buf = pOverlapped->m_message.data();
	pOverlapped->m_wsaBuffer.len = pOverlapped->m_message.max_size();

	auto listIterator = m_overlappedList.emplace(m_overlappedList.end(), pOverlapped);	//m_overlappedList.push_back(overlapped_elem_ty(pOverlapped));
	m_overlListIterMap.emplace(pOverlapped, listIterator);
	init(pOverlapped);

	return pOverlapped;
}

stNetOverlapped *NetOverlapped::Create()
{
	return createCommon([](stNetOverlapped *pOverl) { NetOverlapped::bufferClean(pOverl); });
}

stNetOverlapped *NetOverlapped::CreateWithBuffer(const std::string &buffer)
{
	return createCommon([&buffer](stNetOverlapped *pOverl) { NetOverlapped::bufferCopy(pOverl, buffer); });
}

NetOverlapped::overlapped_list_iter_ty NetOverlapped::getIteratorFromMap(stNetOverlapped *pOverl)
{
	auto keyIterator = m_overlListIterMap.find(pOverl);

	if (keyIterator != m_overlListIterMap.cend())
	{
		auto listIterator = keyIterator->second;

		m_overlListIterMap.erase(keyIterator);
		return listIterator;
	}

	return m_overlappedList.end();
}

bool NetOverlapped::Delete(stNetOverlapped *pOverl)
{
	if (m_overlappedList.empty())
		return false;

	auto listIterator = getIteratorFromMap(pOverl);

	if (listIterator == m_overlappedList.cend())
		return false;

	m_overlappedList.erase(listIterator);
	return true;
}

