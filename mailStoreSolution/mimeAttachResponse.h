
#ifndef MIME_ATTACH_RESPONSE_H__
#define MIME_ATTACH_RESPONSE_H__

#include "httpFileResponse.h"

class MimeAttachBuffer;
class MimeLoader;
class MimeAttach;

class MimeAttachResponse : public HttpFileResponse
{
private:
	std::string m_key;
	std::unique_ptr<MimeAttachBuffer> m_attachBuffer;
	std::unique_ptr<MimeAttach> m_attach;
	MimeLoader *m_mimeLoader;

public:
	MimeAttachResponse(const std::string &key);
	~MimeAttachResponse() override;

private:
	void setLoader(AbstractLoader *loader) override;
	bool writeContent() override;
	size_t makeBody() override;
};

#endif

