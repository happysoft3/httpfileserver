
#ifndef NET_BUFFER_H__
#define NET_BUFFER_H__

#include "common/utils/binaryBuffer.h"
#include <functional>
#include <list>
#include <string>
#include <map>

class NetBufferData;

///HTTP ����
class NetBuffer : public BinaryBuffer
{
private:
	using net_buffer_alert_ty = std::function<void(std::unique_ptr<NetBufferData>)>;
	net_buffer_alert_ty m_alert;
	size_t m_byteRd;
	std::unique_ptr<NetBufferData> m_netData;

public:
	NetBuffer();
	~NetBuffer() override;

private:
	int readchar();
	void unreadchar();

private:
	int readLine(std::list<std::string> &msgList);
	bool verySimpleRequestParse(const std::string &msg);
	bool disposition();
	void onBufferHas() override;
	void createData();

public:
	void SetAlert(net_buffer_alert_ty &&alert);
	std::unique_ptr<NetBufferData> Reset();
};

class NetBufferData
{
	friend NetBuffer;

private:
	std::list<std::string> m_msgList;
	std::map<std::string, std::string> m_requestMsgMap;

public:
	NetBufferData();
	~NetBufferData();

private:
	std::string getRequestParam(size_t index);
	std::string decodeString();

public:
	std::string GetFrontMessage() const;
	bool GetRequestMethod(std::string &method);
	bool GetRequestURI(std::string &path);
	bool GetRequestVersion(std::string &version);
	std::string GetRequestMessage(const std::string &key) const;
};


#endif

