
#include "responseFactory.h"
#include "httpFileResponse.h"
#include "httpHtmlResponse.h"
#include "mimeAttachResponse.h"
#include "mimeHttpResponse.h"
#include "httpJsonResponse.h"

FileResponseFactory::FileResponseFactory()
	: AbstractResponseFactory()
{ }

FileResponseFactory::~FileResponseFactory()
{ }

std::unique_ptr<HttpResponse> FileResponseFactory::createImpl()
{
	return std::make_unique<HttpFileResponse>();
}

DirectoryResponseFactory::DirectoryResponseFactory(const std::string &option)
	: AbstractResponseFactory()
{
	m_option = option;
}

DirectoryResponseFactory::~DirectoryResponseFactory()
{ }

std::unique_ptr<HttpResponse> DirectoryResponseFactory::createImpl()
{
	if (m_option == "json")
		return std::make_unique<HttpJsonResponse>();

	return std::make_unique<HttpHtmlResponse>();
}

MimeResponseFactory::MimeResponseFactory(const std::string &attachKey)
	: AbstractResponseFactory()
{
	m_attachKey = attachKey;
}

MimeResponseFactory::~MimeResponseFactory()
{ }

std::unique_ptr<HttpResponse> MimeResponseFactory::createImpl()
{
	if (m_attachKey.length())
		return std::make_unique<MimeAttachResponse>(m_attachKey);

	return std::make_unique<MimeHttpResponse>();
}

