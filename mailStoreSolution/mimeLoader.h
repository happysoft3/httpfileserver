
#ifndef MIME_LOADER_H__
#define MIME_LOADER_H__

#include "fileLoader.h"
#include <future>

class MimeReader;
class MimePackage;
class HtmlListModel;
//class MimeFileExtractor;
//class AbstractMimeAttachPrinter;
class MimeAttach;

class MimeLoader : public FileLoader
{
	struct ParseResult;
	class MimeListModel;

public:
	using parse_result_ty = std::unique_ptr<ParseResult>;

private:
	std::unique_ptr<std::future<parse_result_ty>> m_parseResult;
	std::unique_ptr<MimePackage> m_mimePackage;
	//std::unique_ptr<MimeFileExtractor> m_extractor;

public:
	explicit MimeLoader(const std::string &rootPath);
	~MimeLoader() override;

private:
	static parse_result_ty mimeParse(std::unique_ptr<MimeReader> reader);

public:
	bool GetResult(std::string &state);

private:
	std::unique_ptr<AbstractResponseFactory> CreateResponseFactory() const override;
	void loadImpl(const std::string &fullPath) override;

public:
	std::unique_ptr<HtmlListModel> CreateModel();
	//std::unique_ptr<MimeFileExtractor> FetchExtractor(const std::string &key, std::unique_ptr<AbstractMimeAttachPrinter> printer = {});
	std::unique_ptr<MimeAttach> FetchAttach(const std::string &key);
};

#endif

