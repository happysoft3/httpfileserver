
#ifndef HTTP_FILE_RESPONSE_H__
#define HTTP_FILE_RESPONSE_H__

#include "httpResponse.h"

class AbstractLoader;
class FileLoader;

class HttpFileResponse : public HttpResponse
{
private:
	FileLoader *m_fileLoader;

public:
	HttpFileResponse();
	~HttpFileResponse() override;

private:
	void setLoader(AbstractLoader *loader) override;
	virtual bool writeFilesize();
	virtual bool writeContentType();

protected:
	size_t putFileContentLength();

private:
	virtual bool writeContent();
	std::string getMimeType(const std::string &extension);

private:
	bool makeFileFormat();
	size_t makeBody() override;
};

#endif

