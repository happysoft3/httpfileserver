
#include "mimeAttachResponse.h"
#include "mimeLoader.h"
#include "mimeAttachBuffer.h"
#include "mimeFormatUtil/mimeAttach.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

MimeAttachResponse::MimeAttachResponse(const std::string &key)
	: HttpFileResponse()
{
	m_key = key;
	m_mimeLoader = nullptr;
}

MimeAttachResponse::~MimeAttachResponse()
{ }

void MimeAttachResponse::setLoader(AbstractLoader *loader)
{
	MimeLoader *mimeLoader = dynamic_cast<MimeLoader *>(loader);

	if (mimeLoader)
	{
		m_mimeLoader = mimeLoader;
		m_attachBuffer = std::make_unique<MimeAttachBuffer>();
	}
}

bool MimeAttachResponse::writeContent()
{
	if (!m_attachBuffer)
		return false;

	m_attachBuffer->BindAttach(std::move(m_attach));
	putSendBuffer(std::move(m_attachBuffer));
	return true;
}

size_t MimeAttachResponse::makeBody()
{
	std::string state;

	if (!m_mimeLoader->GetResult(state))
		return 0;

	m_attach = m_mimeLoader->FetchAttach(m_key);

	if (m_attach)
	{
		changeHeaderValue("Content-Length", std::to_string(m_attach->GetContentLength()));
		changeHeaderValue("Content-Type", m_attach->GetContentType());
		changeHeaderValue("Content-Disposition", stringFormat("%s; filename=\"%s\"", m_attach->GetDispositionType(), m_attach->GetDispositionSubValue()), true);
		
		writeContent();

		return putFileContentLength();
	}
	return 0;
}


