
#include "pathQueryData.h"

PathQueryData::PathQueryData()
	: Tokenizer()
{ }

PathQueryData::~PathQueryData()
{ }

std::unique_ptr<Tokenizer::Token> PathQueryData::popSymb()
{
	if (m_symbTokList.empty())
		return {};

	auto symb = std::move(m_symbTokList.front());

	m_symbTokList.pop_front();
	return symb;
}

std::unique_ptr<Tokenizer::Token> PathQueryData::popIdent()
{
	if (m_identTokList.empty())
		return {};

	auto ident = std::move(m_identTokList.front());

	m_identTokList.pop_front();
	return ident;
}

void PathQueryData::build(std::unique_ptr<Tokenizer::Token> tok)
{
	switch (tok->Type())
	{
	case Token::TokenType::TIDENT:
		m_identTokList.push_back(std::move(tok));
		break;

	case Token::TokenType::TSYMB:
		m_symbTokList.push_back(std::move(tok));
		break;
	}
}

bool PathQueryData::makeQuery()
{
	auto key = popIdent();

	if (!key) return false;
	if (key->Type() != Token::TokenType::TIDENT) return false;

	auto value = popIdent();

	if (!value) return false;
	if (value->Type() != Token::TokenType::TIDENT) return false;

	m_queryMap.emplace(key->Value(), value->Value());
	return true;
}

bool PathQueryData::makeQueryMap()
{
	for (;;)
	{
		auto symb = popSymb();

		if (!symb)
			break;

		if (symb->Value() == "=")
		{
			if (!makeQuery())
				return false;
		}
	}
	return true;
}

bool PathQueryData::buildAll()
{
	for (;;)
	{
		auto tok = readToken();

		if (!tok) return false;
		if (tok->Type() == Token::TokenType::TEOF)
			break;

		build(std::move(tok));
	}
	return makeQuery();
}

void PathQueryData::Query()
{
	buildAll();
}

std::string PathQueryData::GetQueryValue(const std::string &key) const
{
	auto keyIterator = m_queryMap.find(key);

	if (keyIterator != m_queryMap.cend())
		return keyIterator->second;

	return {};
}

