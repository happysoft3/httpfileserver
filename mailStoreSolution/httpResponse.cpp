
#include "httpResponse.h"
#include "abstractLoader.h"
#include "common/utils/binaryBuffer.h"
#include "common/utils/stringhelper.h"

#include <ctime>

using namespace _StringHelper;

static constexpr char s_http_symb[] = "HTTP";
static constexpr char s_testResponseBodyData[] = "<!DOCTYPE html><html><head><title>Your response - V2</title></head><body>Here's the body of the web page. Version 2</body></html>";

template <HttpVersion V> struct HttpVersionInfo
{ };

template <> struct HttpVersionInfo<HttpVersion::HTTP0_9>
	{ static constexpr char value[] = "0.9"; };

template <> struct HttpVersionInfo<HttpVersion::HTTP1_0>
	{ static constexpr char value[] = "1.0"; };

template <> struct HttpVersionInfo<HttpVersion::HTTP1_1>
	{ static constexpr char value[] = "1.1"; };

template <> struct HttpVersionInfo<HttpVersion::HTTP2>
	{ static constexpr char value[] = "2.0"; };

template <> struct HttpVersionInfo<HttpVersion::HTTP3>
	{ static constexpr char value[] = "3.0"; };

class HttpResponse::ResponseToken
{
private:
	bool m_endSpace;
	std::string m_value;

public:
	explicit ResponseToken(const std::string &value)
	{
		m_endSpace = true;
		m_value = value;
	}

protected:
	std::string getValue() const { return m_value; }

public:
	virtual std::string Svalue() const
	{
		return stringFormat("%s%c", m_value, m_endSpace ? ' ' : 0);
	}
	void DisableEndSpace()
	{
		m_endSpace = false;
	}
};

class ResponseSeperator : public HttpResponse::ResponseToken
{
private:
	bool m_consistCarriageReturn;

public:
	ResponseSeperator()
		: ResponseToken("\r\n")
	{
		m_consistCarriageReturn = false;
	}

private:
	std::string Svalue() const override
	{
		return m_consistCarriageReturn ? "\r\n" : "\n";
	}

public:
	void SetCarriageReturn()
	{
		m_consistCarriageReturn = true;
	}
};

class ResponseHeader : public HttpResponse::ResponseToken
{
private:
	std::string m_key;

public:
	explicit ResponseHeader(const std::string &key, const std::string &val)
		: ResponseToken(val)
	{
		m_key = key;
	}
	std::string Svalue() const override
	{
		return stringFormat("%s: %s", m_key, getValue());
	}
};

HttpResponse::HttpResponse()
{
	m_responseState = HttpStatus::OK;

	loadBuiltinsHeader();
}

HttpResponse::~HttpResponse()
{ }

void HttpResponse::insert(const std::string &key, const std::string &value)
{
	m_httpHeaderMap.emplace(key, value);
	m_httpHeaderKeyList.push_back(key);
}

void HttpResponse::loadBuiltinsHeader()
{
	//general
	insert("Date", "Thu, 20 May 2004 21:12:58 GMT");
	insert("Connection", "keep-alive");

	//response
	insert("Server", "Apache/1.3.27");
	insert("Accept-Ranges", "bytes");

	//entity
	insert("Content-Type", "text/html");
	insert("Content-Length", "0");
	insert("Last-Modified", "Tue, 18 May 2004 10:14:49 GMT");
}

void HttpResponse::changeHeaderValue(const std::string &key, const std::string &value, bool forced)
{
	auto keyIterator = m_httpHeaderMap.find(key);

	if (keyIterator != m_httpHeaderMap.cend())
		keyIterator->second = value;
	else if (forced)
	{
		if (key.empty())
			return;
		insert(key, value);
	}
}

void HttpResponse::pushLineReturn(bool consistCarriageReturn)
{
	ResponseSeperator *line = new ResponseSeperator;

	if (consistCarriageReturn)
		line->SetCarriageReturn();
	m_bodyWriteLine.push_back(resp_tok_ty(line));
}

void HttpResponse::pushToken(const std::string &value)
{
	ResponseToken *tok = new ResponseToken(value);

	tok->DisableEndSpace();
	m_bodyWriteLine.push_back(resp_tok_ty(tok));
}

void HttpResponse::BindLoader(std::unique_ptr<AbstractLoader> &&loader)
{
	if (!loader->Valid())
		SetErrorStatus(HttpStatus::NOT_FOUND, "404 File not found.");

	m_loader = std::forward<std::remove_reference<decltype(loader)>::type>(loader);
	setLoader(m_loader.get());
}

void HttpResponse::writeVersion()
{
	m_writeLine.push_back(std::make_unique<ResponseToken>(stringFormat("%s/%s", s_http_symb, HttpVersionInfo<HttpVersion::HTTP1_1>::value)));
}

void HttpResponse::writeStatusCode()
{
	int errcode = StatusErrorCode(m_responseState);

	m_writeLine.push_back(std::make_unique<ResponseToken>(std::to_string(errcode)));
}

void HttpResponse::writeStatusMessage()
{
	std::string stateMsg = StatusErrorMessage(m_responseState);

	m_writeLine.push_back(std::make_unique<ResponseToken>(stateMsg));
}

void HttpResponse::writeStatusLine()
{
	writeVersion();
	writeStatusCode();
	writeStatusMessage();
	m_writeLine.push_back(std::make_unique<ResponseSeperator>());
}

std::unique_ptr<BinaryBuffer> HttpResponse::ReleaseBuffer()
{
	if (!m_sendBuffer)
		return {};

	std::unique_ptr<BinaryBuffer> retBuffer = std::move(m_sendBuffer);
	return retBuffer;
}

void HttpResponse::putSendBuffer(std::unique_ptr<BinaryBuffer> &&buffer)
{
	m_sendBuffer = std::forward<std::remove_reference<decltype(buffer)>::type>(buffer);
}

std::string HttpResponse::headerValue(const std::string &key)
{
	auto keyIterator = m_httpHeaderMap.find(key);

	return (keyIterator != m_httpHeaderMap.cend()) ? keyIterator->second : "";
}

void HttpResponse::beforeWriteHeader()
{
	size_t bodyLength = 0;

	for (;;)
	{
		if (m_responseState == HttpStatus::OK)
		{
			bodyLength = makeBody();
			if (m_responseState == HttpStatus::OK)
				break;
		}
		bodyLength = makeError();
		break;
	}
	changeHeaderValue("Content-Length", std::to_string(bodyLength));

	std::time_t now = std::time(0);
	std::tm datetime;
	localtime_s(&datetime, &now);
	
	changeHeaderValue("Date", 
		stringFormat("%02d-%02d-%04d %02d:%02d:%02d", datetime.tm_mday, datetime.tm_mon + 1, datetime.tm_year + 1900, datetime.tm_hour, datetime.tm_min, datetime.tm_sec));
}

size_t HttpResponse::calcBodySize()
{
	size_t ttSize = 0;

	for (auto &elem : m_bodyWriteLine)
		ttSize += elem->Svalue().length();

	return ttSize;
}

size_t HttpResponse::makeError()
{
	m_bodyWriteLine.clear();
	pushToken("<h1>");
	pushToken(m_errorMessage.empty() ? StatusErrorMessage(m_responseState) : m_errorMessage);
	pushToken("</h1>");
	pushLineReturn(false);

	return calcBodySize();
}

size_t HttpResponse::makeBody()
{
	pushToken(s_testResponseBodyData);

	return calcBodySize();
}

void HttpResponse::writeHeader()
{
	std::string val;

	for (const std::string &key : m_httpHeaderKeyList)
	{
		val = headerValue(key);

		m_writeLine.push_back(std::make_unique<ResponseHeader>(key, (val.empty() ? "empty" : val)));
		m_writeLine.push_back(std::make_unique<ResponseSeperator>());
	}
	m_writeLine.push_back(std::make_unique<ResponseSeperator>());
}

void HttpResponse::writeBody()
{
	while (m_bodyWriteLine.size())
	{
		auto tok = std::move(m_bodyWriteLine.front());

		m_bodyWriteLine.pop_front();
		m_writeLine.push_back(std::move(tok));
	}
}

void HttpResponse::releaseStream(std::string &dest, size_t reserv)
{
	if (m_writeLine.empty())
	{
		if (reserv > 0)
		{
			if (reserv != dest.size())
				dest.resize(reserv);
		}
		return;
	}

	auto top = std::move(m_writeLine.front());
	std::string sv = top->Svalue();

	m_writeLine.pop_front();
	releaseStream(dest, reserv + sv.length());
	std::copy(sv.cbegin(), sv.cend(), dest.begin() + reserv);
}

int HttpResponse::StatusErrorCode(HttpStatus::State state)
{
	static const int s_httpStatusCode[] =
	{
	#define STATUS(code, id, str) code,
	#include "common/identifier/httpStatusCode.inc"
	#undef STATUS
	};
	return s_httpStatusCode[state];
}

std::string HttpResponse::StatusErrorMessage(HttpStatus::State state)
{
	static const char *s_httpStatusMessage[] =
	{
	#define STATUS(code, id, str) str,
	#include "common/identifier/httpStatusCode.inc"
	#undef STATUS
	};
	return s_httpStatusMessage[state];
}

void HttpResponse::Build()
{
	beforeWriteHeader();
	writeStatusLine();
	writeHeader();
	writeBody();
}

void HttpResponse::GetStream(std::string &dest)
{
	if (m_writeLine.empty())
		return;

	releaseStream(dest);
}

void HttpResponse::SetErrorStatus(HttpStatus::State state, const std::string &errorMessage)
{
	m_responseState = state;
	m_errorMessage = errorMessage;
}
