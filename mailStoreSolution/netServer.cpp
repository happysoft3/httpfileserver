
#include "netServer.h"
#include "netsocket.h"
#include "clientmap.h"
#include "workThreadMan.h"
#include "netoverlapped.h"
#include "netbuffer.h"
#include "httpresponse.h"
#include "common/utils/flowTrace.h"
#include "netLog.h"
#include "common/utils/stringhelper.h"
#include <thread>
#include <ws2tcpip.h>
#include <iostream>

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "mswsock.lib")
#pragma comment(lib, "common.lib")

using wsa_ptr_ty = std::unique_ptr<WSADATA, std::function<void(WSADATA*)>>;
static auto s_wsa_destructor = [](WSADATA *wsa) { ::WSACleanup(); delete wsa; };

using namespace _StringHelper;
using serv_cb_ty = std::function<void(NetSocket *, std::unique_ptr<NetBufferData>)>;

struct NetServer::Core
{
	wsa_ptr_ty m_wsaData;
	HANDLE m_cpHandle;
	std::shared_ptr<serv_cb_ty> m_servCb;
};

NetServer::NetServer()
	: m_core(new Core{})
{
	m_clientMap = std::make_unique<ClientMap>();
	m_core->m_servCb = m_clientMap->SetServerCallback([this](NetSocket *sock, std::unique_ptr<NetBufferData> buffer) { return this->onReceive(sock, std::move(buffer)); });
	m_ipAddress = "127.0.0.1";
	m_portId = 80; //18590;
}

NetServer::~NetServer()
{
	m_workerThread.reset();
	m_aliveAcceptFork.reset();
	if (m_acceptThreadResult.valid())
		m_acceptThreadResult.get();

	if (m_core->m_cpHandle)
		::CloseHandle(m_core->m_cpHandle);

	m_clientMap.reset();
	m_core.reset();
}

bool NetServer::onShutdown(bool ret)
{
	std::cout << "server has been halted" << std::endl;
	return ret;
}

void NetServer::onDisconnected(NetSocket *client)
{
	std::cout << stringFormat("onDisconnected::%d", client->GetSocketNumber()) << std::endl;
}

void NetServer::onReceive(NetSocket *client, std::unique_ptr<NetBufferData> cliBuffer)
{
	std::string method, path;

	cliBuffer->GetRequestMethod(method);
	cliBuffer->GetRequestURI(path);

	std::cout << stringFormat("onReceive::%d, first line: %s, %s", client->GetSocketNumber(), method, path) << std::endl;
	HttpResponse httpResp;
	std::string stream;

	httpResp.Build();
	httpResp.GetStream(stream);
	client->Send(stream);
}

void NetServer::onSend(NetSocket *client)
{
	std::cout << stringFormat("onSend::%d", client->GetSocketNumber()) << std::endl;
}

void NetServer::createServerLog(std::unique_ptr<AbstractFlowIssue> issue)
{
	m_servLog->PushTrace(std::move(issue));
}

std::unique_ptr<WSAData> NetServer::initWsa()
{
	std::unique_ptr<WSADATA> wsadata(new WSADATA);

	if (::WSAStartup(MAKEWORD(2, 2), wsadata.get()) != 0)
	{
		//wsa 초기화오류
		return nullptr;
	}
	return wsadata;
}

bool NetServer::initServerSocket()
{
	std::unique_ptr<NetSocket> servSocket(new NetSocket(::WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, nullptr, 0, WSA_FLAG_OVERLAPPED)));

	if (!servSocket->IsValid())
	{
		return false;
	}

	servSocket->SetInfo(m_ipAddress, m_portId);
	m_servSocket = std::move(servSocket);
	return true;
}

bool NetServer::workProc()
{
	DWORD bytes = 0;
	NetSocket *clientSocket = nullptr;
	stNetOverlapped *pOverlapped = nullptr;

	bool res = ::GetQueuedCompletionStatus(m_core->m_cpHandle, &bytes, reinterpret_cast<LPDWORD>(&clientSocket), reinterpret_cast<LPOVERLAPPED *>(&pOverlapped), INFINITE) & true;

	if (pOverlapped == nullptr)
		return true;

	pOverlapped->m_realPostBytes = bytes;

	switch (pOverlapped->m_ioOperation)
	{
	case stNetOverlapped::SHUTDOWN: return onShutdown(false);

	case stNetOverlapped::ACCEPT:
		//여기서 처리 안함//
		return true;

	case stNetOverlapped::RECEIVE:
		if ((!res) || (!bytes))
		{
			onDisconnected(clientSocket);
			m_clientMap->Remove(*clientSocket);
			return true;
		}
		clientSocket->PostReceiving(pOverlapped, m_clientMap->GetBuffer(*clientSocket));
		clientSocket->ReservReceive();
		return true;

	case stNetOverlapped::SEND:	//만약 여기에서,pOverlapped 객체를 참조했다면, 순서가 꼬일 수도 있을 듯...
		clientSocket->PostSend(pOverlapped);
		onSend(clientSocket);
		return true;

	default: return onShutdown(false);
	}
}

void NetServer::shutdownWorkThread(int numberOfConcurrentThreads)
{
	m_haltOverl = std::make_unique<NetOverlapped>();

	while ((--numberOfConcurrentThreads) >= 0)
	{
		auto pOverlapped = m_haltOverl->Create();

		pOverlapped->m_ioOperation = stNetOverlapped::SHUTDOWN;

		::PostQueuedCompletionStatus(m_core->m_cpHandle, 0, 0, reinterpret_cast<LPOVERLAPPED>(pOverlapped));
	}

	std::string shutdownMsg("void NetServer::shutdownWorkThread(int numberOfConcurrentThreads) properly called\n");

	createServerLog(std::make_unique<NetLog>(shutdownMsg));
}

bool NetServer::startupWorkThread()
{
	m_workerThread = std::make_unique<WorkThreadMan>();

	m_workerThread->SetTask([this]() { return this->workProc(); }, [this](int n) { this->shutdownWorkThread(n); });

	return m_workerThread->Place();
}

void NetServer::acceptProc(std::weak_ptr<bool> ctl)
{
	timeval tm = { 0, 100 };

	do
	{
		auto alive = ctl.expired() ? nullptr : ctl.lock();

		if (!alive)
			break;

		fd_set ss = {};

		FD_SET(m_servSocket->GetSocketNumber(), &ss);
		int res = select(0, &ss, nullptr, nullptr, &tm);

		if (res == SOCKET_ERROR)
			break;
		if (!res)
			continue;

		auto client = std::unique_ptr<NetSocket>(m_servSocket->Accept());

		if (client)
		{
			onJoin(*client);
			m_core->m_cpHandle = ::CreateIoCompletionPort(reinterpret_cast<HANDLE>(client->GetSocketNumber()), m_core->m_cpHandle, reinterpret_cast<DWORD>(client.get()), 0);

			m_clientMap->Add(std::move(client));
		}
	} while (true);
}

void NetServer::createAcceptThread()
{
	m_aliveAcceptFork = std::make_shared<bool>(true);
	std::packaged_task<void()> task([this, alive = std::weak_ptr<bool>(m_aliveAcceptFork)]() { this->acceptProc(alive); });
	m_acceptThreadResult = task.get_future();
	std::thread acceptThread(std::move(task));

	acceptThread.detach();
}

std::unique_ptr<AbstractTraceCore> NetServer::makeLogRecorder()
{
	return std::make_unique<FlowTrace::TraceCore>();
}

bool NetServer::initServerLog()
{
	auto rec = makeLogRecorder();

	if (!rec)
		return false;

	m_servLog = std::make_shared<FlowTrace>();
	return m_servLog->Start(std::move(rec));
}

bool NetServer::Start()
{
	if (!initServer())
		return false;

	if (!initServerLog())
		return false;

	auto pWsa = initWsa();

	if (!pWsa)
		return false;

	std::unique_ptr<WSAData, std::function<void(WSAData*)>> wsa(pWsa.release(), s_wsa_destructor);

	if (!initServerSocket())
		return false;
	if (!m_servSocket->Bind())
		return false;
	if (!m_servSocket->Listen())
		return false;

	m_core->m_wsaData = std::move(wsa);
	m_core->m_cpHandle = ::CreateIoCompletionPort(INVALID_HANDLE_VALUE, nullptr, 0, 2);
	startupWorkThread();
	createAcceptThread();
	onStartup();

	return true;
}
