
#ifndef FILE_LOADER_H__
#define FILE_LOADER_H__

#include "abstractLoader.h"

class FileLoader : public AbstractLoader
{
	struct FileCore;

public:
	enum class FileAccessInfo
	{
		FULLNAME,
		NAME,
		SIZE,
		URL,
		EXTENSION
	};

private:
	std::unique_ptr<FileCore> m_core;

public:
	FileLoader(const std::string &rootPath);
	~FileLoader() override;

private:
	std::string getExtension();
	virtual void loadImpl(const std::string &fullPath);

public:
	void Load(const std::string &url) override;
	std::unique_ptr<AbstractResponseFactory> CreateResponseFactory() const override;

private:
	std::string FileName() const override;
	std::string Size() const override;
	std::string Extension() const override;
	std::string FullName() const;

public:
	bool GetProperty(FileAccessInfo accessKey, std::string &dest);
};

#endif

