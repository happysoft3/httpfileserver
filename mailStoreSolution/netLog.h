
#ifndef NET_LOG___
#define NET_LOG___

#include "common/utils/flowTrace.h"

class NetLog : public FlowTrace::FlowIssue
{
public:
	NetLog(const std::string &issueMsg = {});
	~NetLog() override;

	std::string What() const;
};

#endif

