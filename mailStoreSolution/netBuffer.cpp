
#include "netBuffer.h"
#include "common/utils/stringhelper.h"
#include "common/utils/urlString.h"

using namespace _StringHelper;

NetBuffer::NetBuffer()
	: BinaryBuffer()
{
	m_byteRd = 0;
	createData();
}

NetBuffer::~NetBuffer()
{ }

int NetBuffer::readchar()
{
	char c = 0;

	if (GetC(c, m_byteRd))
	{
		++m_byteRd;
		return static_cast<int>(c);
	}
	return EOF;
}

void NetBuffer::unreadchar()
{
	if (m_byteRd > 0)
		--m_byteRd;
}

int NetBuffer::readLine(std::list<std::string> &msgList)
{
	std::string msg;

	for (;;)
	{
		int c = readchar();

		switch (c)
		{
		case EOF:
			StreamSet(msg);
			return c;

		case '\r':
			break;
		case '\n':
			msgList.push_back(msg);
			return c;

		default:
			msg.push_back(static_cast<char>(c));
		}
	}
}

bool NetBuffer::verySimpleRequestParse(const std::string &msg)
{
	size_t seperatePos = msg.find_first_of(':');

	if (seperatePos == std::string::npos)
		return false;

	std::string key = msg.substr(0, seperatePos);
	std::string value = msg.substr(++seperatePos);

	stringTrim(key);
	stringTrim(value);

	if (key.size() && value.size())
	{
		m_netData->m_requestMsgMap.emplace(key, value);
		return true;
	}
	return false;
}

bool NetBuffer::disposition()
{
	m_byteRd = 0;

	std::list<std::string> msgList;

	while (readLine(msgList) != EOF);

	while (msgList.size())
	{
		auto s = std::move(msgList.front());

		msgList.pop_front();
		verySimpleRequestParse(s);
		m_netData->m_msgList.push_back(s);
	}

	return Empty();
}

void NetBuffer::onBufferHas()
{
	if (disposition())
	{
		if (m_alert)
			m_alert(std::move(m_netData));

		createData();
	}
}

void NetBuffer::createData()
{
	m_netData = std::make_unique<NetBufferData>();
}

void NetBuffer::SetAlert(NetBuffer::net_buffer_alert_ty &&alert)
{
	m_alert = std::forward<net_buffer_alert_ty>(alert);
}

std::unique_ptr<NetBufferData> NetBuffer::Reset()
{
	Clear();

	return std::move(m_netData);
}

NetBufferData::NetBufferData()
{ }

NetBufferData::~NetBufferData()
{ }

std::string NetBufferData::getRequestParam(size_t index)
{
	std::string top = GetFrontMessage();

	if (top.empty())
		return {};

	std::vector<std::string> keywordVec;
	std::string keyword;

	top.push_back(' ');
	keyword.reserve(top.size());
	for (const char &c : top)
	{
		switch (c)
		{
		case ' ': case '\t':
			if (keyword.length())
			{
				keywordVec.push_back(keyword);
				keyword.clear();
			}
			break;

		default:
			keyword.push_back(c);
		}
	}

	if (index >= keywordVec.size())
		return {};

	return keywordVec[index];
}

std::string NetBufferData::decodeString()
{
	std::string encodedStr = getRequestParam(1);
	UrlString decoder;
	std::string res;

	decoder.StreamSet(encodedStr);
	decoder.Decode(res);

	return res;
}

std::string NetBufferData::GetFrontMessage() const
{
	return m_msgList.empty() ? "" : m_msgList.front();
}

bool NetBufferData::GetRequestMethod(std::string &method)
{
	std::string param = getRequestParam(0);

	if (param.empty())
		return false;

	method = param;
	return true;
}

bool NetBufferData::GetRequestURI(std::string &path)
{
	std::string param = decodeString();

	if (param.empty())
		return false;

	if (param.front() == '/')
	{
		if (param.length() == 1)
			return false;

		path = param.substr(1);
	}
	else
		path = param;
	return true;
}

bool NetBufferData::GetRequestVersion(std::string &version)
{
	std::string param = getRequestParam(2);

	if (param.empty())
		return false;

	version = param;
	return true;
}

std::string NetBufferData::GetRequestMessage(const std::string &key) const
{
	auto keyIterator = m_requestMsgMap.find(key);

	if (keyIterator != m_requestMsgMap.cend())
		return keyIterator->second;

	return {};
}

