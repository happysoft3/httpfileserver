
#ifndef MIME_ATTACH_BUFFER_H__
#define MIME_ATTACH_BUFFER_H__

#include "common/utils/binaryBuffer.h"

class MimeAttach;

class MimeAttachBuffer : public BinaryBuffer
{
private:
	std::unique_ptr<MimeAttach> m_attach;

public:
	MimeAttachBuffer();
	~MimeAttachBuffer() override;

private:
	void sendStream(const std::vector<char> &stream);

public:
	void BindAttach(std::unique_ptr<MimeAttach> attach);

private:
	bool CreateComponent() override;
};

#endif

