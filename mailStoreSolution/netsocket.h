
#ifndef NET_SOCKET_H__
#define NET_SOCKET_H__

#include <memory>
#include <list>
#include <string>
#include <mutex>

class NetOverlapped;
struct stNetOverlapped;
class BinaryBuffer;

class FlowTrace;

class NetSocket
{
	class IntSocket;
	struct Core;

private:
	std::unique_ptr<Core> m_core;
	std::unique_ptr<IntSocket> m_socket;
	std::unique_ptr<NetOverlapped> m_recvOverlapped;
	std::unique_ptr<NetOverlapped> m_sendOverlapped;
	std::unique_ptr<NetOverlapped> m_acceptOverl;

	///////서버 로그객체 입니다////
	std::shared_ptr<FlowTrace> m_serverLog;

public:
	explicit NetSocket(uint32_t sock);
	~NetSocket();

private:
	bool recvImpl();
	bool sendImpl(const std::string &src, size_t &procBytes);
	bool sendBufferImpl(BinaryBuffer &buffer);

public:
	bool IsValid() const;
	bool Bind();
	bool Listen(int backlog = 5);
	void SetInfo(const std::string &ipAddress, int port);
	void PostReceiving(stNetOverlapped *pOverl, BinaryBuffer *pBuffer = nullptr);
	bool ReservReceive();
	bool Send(const std::string &srcMsg);
	NetSocket *Accept();
	NetSocket *PostAccept();

	uint32_t GetSocketNumber() const;
	void Disconnect();
	bool Connect();

public:
	void AppendSendQueue(std::unique_ptr<BinaryBuffer> &&task);
	void PostSend(stNetOverlapped *pOverl);

private:
	std::mutex m_lock;
};

#endif

