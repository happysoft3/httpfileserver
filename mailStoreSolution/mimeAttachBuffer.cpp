
#include "mimeAttachBuffer.h"
#include "mimeFormatUtil/mimeAttach.h"

MimeAttachBuffer::MimeAttachBuffer()
	: BinaryBuffer()
{ }

MimeAttachBuffer::~MimeAttachBuffer()
{ }

void MimeAttachBuffer::sendStream(const std::vector<char> &stream)
{
	StreamSet(stream);
}

void MimeAttachBuffer::BindAttach(std::unique_ptr<MimeAttach> attach)
{
	if (attach)
	{
		attach->RegistOutstreamMethod([this](const std::vector<char> &src) { sendStream(src); });
		m_attach = std::move(attach);
	}
}

bool MimeAttachBuffer::CreateComponent()
{
	int state = m_attach->DoExtract();

	if (state == EOF)
	{
		Clear();
	}
	else if (!state)
	{
		Clear(); //TODO. 오류발생인 경우, 입니다
		return false;
	}

	return true;
}

