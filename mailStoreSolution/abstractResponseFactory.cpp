
#include "abstractResponseFactory.h"
#include "httpResponse.h"

AbstractResponseFactory::AbstractResponseFactory()
{ }

AbstractResponseFactory::~AbstractResponseFactory()
{ }

void AbstractResponseFactory::Create()
{
	m_response = createImpl();
}

std::unique_ptr<HttpResponse> AbstractResponseFactory::Release()
{
	if (!m_response)
		return {};

	std::unique_ptr<HttpResponse> resp;

	resp.swap(m_response);
	return resp;
}

