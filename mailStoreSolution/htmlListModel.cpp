
#include "htmlListModel.h"

HtmlListModel::HtmlListModel()
{ }

HtmlListModel::~HtmlListModel()
{ }

void HtmlListModel::declareKeyHandler(const std::string &key, HtmlListModel::key_handler_fn_ty &&handler)
{
	if (m_keyHandlerMap.find(key) == m_keyHandlerMap.cend())
		m_keyHandlerMap.emplace(key, std::forward<key_handler_fn_ty>(handler));
}

void HtmlListModel::pushSubKey(const std::string &subKey, HtmlListModel::key_handler_fn_ty &&subHandler)
{
	if (m_subKeyMap.find(subKey) == m_subKeyMap.cend())
		m_subKeyMap.emplace(subKey, std::forward<key_handler_fn_ty>(subHandler));
}

std::string HtmlListModel::FetchValue(const std::string &key) const
{
	auto keyIterator = m_keyHandlerMap.find(key);

	if (keyIterator == m_keyHandlerMap.cend())
		return {};

	return keyIterator->second();
}

std::string HtmlListModel::FetchSubValue(const std::string &subKey) const
{
	auto subKeyIterator = m_subKeyMap.find(subKey);

	if (subKeyIterator == m_subKeyMap.cend())
		return {};

	return subKeyIterator->second();
}

