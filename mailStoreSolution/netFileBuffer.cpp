
#include "netFileBuffer.h"
#include "common/include/incFilesystem.h"
#include <sstream>
#include <functional>
#include <fstream>

using binary_char_ty = uint8_t;
using binary_file_ty = std::basic_fstream<binary_char_ty, std::char_traits<binary_char_ty>>;

struct NetFileBuffer::Core
{
	std::unique_ptr<binary_file_ty> m_reader;

	using printer_function_ty = std::function<void(size_t, size_t &)>;
	std::unique_ptr<printer_function_ty> m_printTask;

	std::vector<binary_char_ty> m_readBuffer;
	size_t m_wrPos;
};

NetFileBuffer::NetFileBuffer()
	: BinaryBuffer()
{
	m_fSize = 0;
}

NetFileBuffer::NetFileBuffer(const std::string &fileName, const std::string &fileSize)
	: BinaryBuffer()
{
	m_fUrl = fileName;
	setFilesize(fileSize);
}

NetFileBuffer::~NetFileBuffer()
{ }

void NetFileBuffer::setFilesize(const std::string &fSize)
{
	std::stringstream ss(fSize);

	ss >> m_fSize;
}

void NetFileBuffer::fillZero(size_t buffsize, size_t &procSize)
{
	std::vector<binary_char_ty> zeros(buffsize, 0);

	StreamSet(zeros);
	procSize = buffsize;
}

void NetFileBuffer::printFile(size_t rdsize, size_t &procSize)
{
	size_t readCount = 0;

	m_fileCore->m_readBuffer.resize(rdsize);
	m_fileCore->m_reader->read(m_fileCore->m_readBuffer.data(), rdsize);
	readCount = static_cast<size_t>(m_fileCore->m_reader->gcount());

	if (readCount != 0)
		StreamSet(m_fileCore->m_readBuffer);
	procSize = readCount;
}

size_t NetFileBuffer::requestSize() const
{
	return ((m_fSize - m_fileCore->m_wrPos) >= binary_filebuffer_max) ? binary_filebuffer_max : (m_fSize - m_fileCore->m_wrPos);
}

void NetFileBuffer::printFilestream()
{
	size_t procSize = 0;

	for (;;)
	{
		if (m_fileCore->m_wrPos < m_fSize)
		{
			(*m_fileCore->m_printTask)(requestSize(), procSize);

			if (!procSize)
			{
				m_fileCore->m_printTask = std::make_unique<Core::printer_function_ty>([this](size_t req, size_t &procSize) { this->fillZero(req, procSize); });
				continue;
			}

			m_fileCore->m_wrPos += procSize;
		}
		else
			Clear();
		break;
	}
}

bool NetFileBuffer::makePrintImpl()
{
	std::unique_ptr<binary_file_ty> fileRd(new binary_file_ty(m_fUrl, std::ios::in | std::ios::binary));

	*fileRd << std::noskipws;
	if (fileRd)
	{
		m_fileCore = std::make_unique<NetFileBuffer::Core>();
		m_fileCore->m_reader = std::move(fileRd);
		m_fileCore->m_readBuffer.reserve(binary_filebuffer_max);
		m_fileCore->m_wrPos = 0;
		m_fileCore->m_printTask = std::make_unique<Core::printer_function_ty>([this](size_t reqSize, size_t &procSize) { this->printFile(reqSize, procSize); });
		return true;
	}
	return false;
}

bool NetFileBuffer::MakePrinter()
{
	if (!NAMESPACE_FILESYSTEM::exists(m_fUrl))
		return false;

	return makePrintImpl();
}

bool NetFileBuffer::CreateComponent()
{
	if (m_fileCore)
	{
	//여기에서 파일을 읽고, 스트림으로 씁니다.
		printFilestream();
		return true;
	}
	return false;
}
