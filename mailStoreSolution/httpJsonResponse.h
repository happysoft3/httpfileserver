
#ifndef HTTP_JSON_RESPONSE_H__
#define HTTP_JSON_RESPONSE_H__

#include "httpResponse.h"

namespace Json
{
	class Value;
}

class DirectoryLoader;

class HttpJsonResponse : public HttpResponse
{
private:
	DirectoryLoader *m_dirLoader;

public:
	HttpJsonResponse();
	~HttpJsonResponse() override;

private:
	void setLoader(AbstractLoader *loader) override;

	std::unique_ptr<Json::Value> loadJsonSingle(std::unique_ptr<AbstractLoader> &&loader);
	std::unique_ptr<Json::Value> loadFiles();
	void makeJson(std::string &jsStream);
	size_t makeBody() override;
};

#endif

