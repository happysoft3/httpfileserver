
#include "uriRule.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

class UriRule::QueryError
{
private:
	int m_pos;
	std::string m_name;

public:
	QueryError(const std::string &name, int pos = 0)
	{
		m_name = name;
		m_pos = pos;
	}
	~QueryError()
	{ }

	std::string Get() const
	{
		return stringFormat("%s-- pos: %d", m_name, m_pos);
	}
};

UriRule::UriRule()
	: BinaryHelper()
{ }

UriRule::~UriRule()
{ }

void UriRule::putQueryMap(const std::string &key, const std::string &value)
{
	m_queryMap.emplace(key, value);
}

int UriRule::hexaChar(int c) const
{
	switch (c)
	{
	case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		return c - 'A' + 10;
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
		return c - '0';

	default:
		return -1;
	}
}

int UriRule::readHexaExpr(int /*c*/)
{
	int high = hexaChar(pop());
	int low = hexaChar(pop());

	if (high == -1 || low == -1)
		throw QueryError("Wrong hexa string", getPos());

	return low + (high * 16);
}

bool UriRule::isIdentChar(int c) const
{
	//if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))	//alphabet
	//	return true;

	//switch (c)
	//{
	//case '0': case '1': case '2': case '3': case '4':		//number
	//case '5': case '6': case '7': case '8': case '9':
	//case '-':		//allow hyphen
	//case '.':		//allowed for extension of file
	//	return true;

	//default:
	//	return false;
	//}

	switch (c)
	{
	case '/':
	case '=':
	case '&':
	case '?':
	case EOF:
		return false;

	default:
		return true;
	}
}

std::string UriRule::readIdent(int c)
{
	std::string buffer(sizeof(char), c);

	for (;;)
	{
		c = pop();

		if (c == '%')
			c = readHexaExpr(c);
		else if (!isIdentChar(c))
		{
			push(c);
			return buffer;
		}		

		//default:
		//	if (!isIdentChar(c))
		//	{
		//		push(c);
		//		return buffer;
		//	}
		buffer.push_back(static_cast<char>(c));
	}
}

void UriRule::readQuery()
{
	int c;
	std::string key;

	for (;;)
	{
		c = pop();
		if (!isIdentChar(c))
			throw QueryError(stringFormat("expect 'key' but got '%c'", c), getPos());
		key = readIdent(c);
		if (!next('='))
			throw QueryError(stringFormat("expect '=' but got '%c'", c), getPos());
		c = pop();
		if (!isIdentChar(c))
			throw QueryError(stringFormat("expect 'value' but got '%c'", c), getPos());
		putQueryMap(key, readIdent(c));

		if (next(EOF))
			break;

		if (next('&'))
			continue;

		throw QueryError(stringFormat("expect '&' but got '%c'", c), getPos());
	}
}

void UriRule::readUri()
{
	for (;;)
	{
		int c = pop();

		switch (c)
		{
		case EOF:
			return;

		case '?':
			readQuery();
			break;

		case '/':
			break;

		default:
			m_identList.push_back(readIdent(c));
		}
		//if (c == EOF)
		//	break;

		//if (c == '?')
		//{
		//	readQuery();
		//	continue;
		//}
		//if (isIdentChar(c))
		//{
		//	m_identList.push_back(readIdent(c));
		//}
	}
}

void UriRule::versionImpl()
{
	if (m_identList.empty())
		throw QueryError("the uri has no version");

	m_version = m_identList.front();
	m_identList.pop_front();
}

void UriRule::onBufferPlaced()
{
	m_error.reset();
	try
	{
		readUri();
		versionImpl();
	}
	catch (const QueryError &err)
	{
		m_error = std::make_unique<QueryError>(err);
	}
}

bool UriRule::GetError(std::string &err) const
{
	if (m_error)
	{
		err = m_error->Get();
		return true;
	}
	return false;
}

std::string UriRule::QueryValue(const std::string &key) const
{
	auto keyIterator = m_queryMap.find(key);

	if (keyIterator == m_queryMap.cend())
		return {};
	
	return keyIterator->second;
}

std::string UriRule::Version() const
{
	return m_version;
}

std::string UriRule::ResourcePath(char tok) const
{
	std::string resource;

	for (const std::string &s : m_identList)
		resource.append(stringFormat("%c%s", tok, s));
	
	return resource;
}

void UriRule::FetchFromOtherUri(UriRule &uri)
{
	m_identList = uri.m_identList;
	m_version = uri.m_version;
	m_queryMap = uri.m_queryMap;
}

void UriRule::AddPath(const std::string &path)
{
	m_identList.push_back(path);
}

void UriRule::DiscardBackPathOne()
{
	if (m_identList.size())
		m_identList.pop_back();
}

std::string UriRule::FullUri() const
{
	std::string full = '/' + m_version;

	for (const std::string &s : m_identList)
		full.append(stringFormat("/%s", s));

	return full;
}
