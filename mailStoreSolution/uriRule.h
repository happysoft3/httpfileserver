
#ifndef URI_RULE_H__
#define URI_RULE_H__

#include "common/utils/binaryHelper.h"
#include <memory>
#include <list>
#include <string>
#include <map>

class UriRule : public BinaryHelper
{
	class QueryError;
private:
	std::list<std::string> m_identList;
	std::string m_version;
	std::map<std::string, std::string> m_queryMap;
	std::unique_ptr<QueryError> m_error;

public:
	UriRule();
	~UriRule();

private:
	void putQueryMap(const std::string &key, const std::string &value);
	int hexaChar(int c) const;
	int readHexaExpr(int c);
	bool isIdentChar(int c) const;
	std::string readIdent(int c);
	void readQuery();
	void readUri();
	void versionImpl();
	void onBufferPlaced() override;

public:
	bool GetError(std::string &error) const;
	std::string QueryValue(const std::string &key) const;
	std::string Version() const;
	std::string ResourcePath(char tok = '/') const;
	void FetchFromOtherUri(UriRule &uri);
	void AddPath(const std::string &path);
	void DiscardBackPathOne();
	std::string FullUri() const;
};

#endif

