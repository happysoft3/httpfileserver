
#ifndef CLIENT_MAP_H__
#define CLIENT_MAP_H__

#include <map>
#include <memory>
#include <functional>
#include <mutex>

class NetSocket;
class NetBuffer;
class NetBufferData;

class ClientMap
{
	struct ClientCore;
	using server_cb_proto_ty = std::function<void(NetSocket *, std::unique_ptr<NetBufferData>)>;

private:
	using client_map_ty = std::map<uint32_t, std::unique_ptr<ClientCore>>;
	client_map_ty m_clients;
	std::weak_ptr<server_cb_proto_ty> m_servCb;

public:
	ClientMap();
	~ClientMap();

public:
	std::shared_ptr<server_cb_proto_ty> SetServerCallback(server_cb_proto_ty &&servCb);

private:
	bool getSocketId(const NetSocket &client, uint32_t &idDest);
	ClientCore *makeClientCore(std::unique_ptr<NetSocket> client);

public:
	using retrieve_method_ty = std::function<void(client_map_ty::iterator&)>;

private:
	ClientCore *retrieveImpl(const NetSocket &client, retrieve_method_ty &&method);
	ClientCore *retrieve(const NetSocket &client);
	ClientCore *retrieveById(uint32_t sockId) const;
	bool searching(const NetSocket &client);
	void bufferAnnounce(uint32_t sockId, std::unique_ptr<NetBufferData> netData);

public:
	void Add(std::unique_ptr<NetSocket> client);
	void Remove(NetSocket &client);
	std::unique_ptr<NetSocket> Fetch(const NetSocket &client);
	NetBuffer *GetBuffer(NetSocket &client);

private:
	std::recursive_mutex m_lock;
};

#endif

