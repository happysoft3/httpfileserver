
#ifndef INVALID_LOADER_H__
#define INVALID_LOADER_H__

#include "abstractLoader.h"

class InvalidLoader : public AbstractLoader
{
public:
	InvalidLoader();
	~InvalidLoader() override;

private:
	bool Valid() const override
	{
		return false;
	}

	void Load(const std::string &) override
	{ }

	std::string FileName() const override
	{
		return "invalid";
	}

private:
	std::unique_ptr<AbstractResponseFactory> CreateResponseFactory() const override;
};

#endif

