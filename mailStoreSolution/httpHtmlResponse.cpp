
#include "httpHtmlResponse.h"
#include "directoryLoader.h"
#include "htmlListModel.h"
#include "common/utils/stringhelper.h"

#include <functional>

using namespace _StringHelper;

static constexpr char s_resource_serv_host[] = "http://archive.noxcommunity.com/";


class HttpHtmlResponse::ScopedHtml
{
private:
	HttpHtmlResponse *m_parent;

public:
	ScopedHtml(HttpHtmlResponse *parent)
	{
		m_parent = parent;
	}

	~ScopedHtml()
	{
		m_parent->popHtmlKeyword();
	}
};

class HttpHtmlResponse::ListColumn
{
private:
	std::string m_name;
	using column_write_fn_ty = std::function<void(const std::string &)>;
	column_write_fn_ty m_trig;

public:
	ListColumn(column_write_fn_ty &&trig, const std::string &name = {})
		: m_name(name)
	{
		m_trig = std::forward<std::decay_t<decltype(trig)>>(trig);
	}
	std::string Name() const
	{
		return m_name;
	}
	void Write()
	{
		if (m_trig)
			m_trig(m_name);
	}
};

HttpHtmlResponse::HttpHtmlResponse()
	: HttpResponse()
{
	m_dirLoader = nullptr;
}

HttpHtmlResponse::~HttpHtmlResponse()
{ }

void HttpHtmlResponse::setListmodel(std::unique_ptr<HtmlListModel> &&model)
{
	m_model = std::forward<std::decay_t<decltype(model)>>(model);
	m_model->InitialDeclare();
}

void HttpHtmlResponse::setLoader(AbstractLoader *loader)
{
	m_dirLoader = dynamic_cast<DirectoryLoader *>(loader);
	setListmodel(m_dirLoader->CreateModel());
}

void HttpHtmlResponse::putHtmlMarkup()
{
	static constexpr char html_symbol[] = "<!DOCTYPE html>";

	pushToken(html_symbol);
	putHtmlKeyword("html");
}

void HttpHtmlResponse::pushUnkeyword(const std::string &keyword)
{
	m_htmlStack.push(stringFormat("</%s>", keyword));
}

std::unique_ptr<HttpHtmlResponse::ScopedHtml> HttpHtmlResponse::makeScope(bool scope)
{
	if (scope)
		return std::make_unique<ScopedHtml>(this);

	return nullptr;
}

std::unique_ptr<HttpHtmlResponse::ScopedHtml> HttpHtmlResponse::putHtmlKeyword(const std::string &keyword, bool scope)
{
	pushToken(stringFormat("<%s>", keyword));
	pushUnkeyword(keyword);

	return makeScope(scope);
}

std::unique_ptr<HttpHtmlResponse::ScopedHtml> HttpHtmlResponse::putHtmlKeywordProperty(const std::string &keyword, const std::string &key, const std::string &value, bool scope)
{
	if (key.empty())
		return nullptr;

	pushToken(stringFormat("<%s %s=%s>", keyword, key, AdjectCharT(value, '"')));
	pushUnkeyword(keyword);

	return makeScope(scope);
}

std::unique_ptr<HttpHtmlResponse::ScopedHtml> HttpHtmlResponse::putHtmlKeywordWithProperty(const std::string &keyword, std::list<std::pair<std::string, std::string>> &&properties, bool scope)
{
	if (properties.empty())
		return nullptr;

	pushToken(stringFormat("<%s", keyword));
	pushUnkeyword(keyword);
	for (const auto &elem : properties)
		pushToken(stringFormat(" %s=%s", elem.first, AdjectCharT(elem.second, '"')));

	pushToken(">");

	return makeScope(scope);
}

bool HttpHtmlResponse::popHtmlKeyword()
{
	if (m_htmlStack.empty())
		return false;

	std::string pop = m_htmlStack.top();

	m_htmlStack.pop();
	pushToken(pop);
	return true;
}

void HttpHtmlResponse::putTitle(const std::string &titleMessage)
{
	auto headScope = putHtmlKeyword("head", true);

	if (titleMessage.length())
	{
		auto titleScope = putHtmlKeyword("title", true);

		pushToken(titleMessage);
	}
}

void HttpHtmlResponse::putCotentTitle(const std::string &contentTitleMessage)
{
	if (contentTitleMessage.length())
	{
		auto scope = putHtmlKeyword("h1", true);

		pushToken(contentTitleMessage);
	}
}

void HttpHtmlResponse::putExplanation(const std::string &misc)
{
	if (misc.length())
	{
		auto scope = putHtmlKeyword("p", true);

		pushToken(misc);
	}
}

void HttpHtmlResponse::putTableColumnSingle(const std::string &columnName)
{
	auto scope = putHtmlKeyword("th", true);

	pushToken(columnName);
}

void HttpHtmlResponse::tableColSpan()
{
	static constexpr int table_col_span = 5;

	auto otScope = putHtmlKeyword("tr", true);
	{
		auto inScope = putHtmlKeywordProperty("th", "colspan", std::to_string(table_col_span), true);
		{
			auto hrScope = putHtmlKeyword("hr", true);
		}
	}
}

void HttpHtmlResponse::putTableImage(const std::string &imgUrl)
{	
	auto thScope = putHtmlKeywordProperty("td", "valign", "top", true);
	{
		auto img = putHtmlKeywordWithProperty("img", { {"src", s_resource_serv_host + imgUrl}, {"alt", "[ICO]"} }, true);
	}
}

void HttpHtmlResponse::columnPutImage(const std::string &key)
{
	putTableImage(m_model->FetchValue(key));
}

void HttpHtmlResponse::columnPutTextWithUrl(const std::string &key)
{
	auto tdField = putHtmlKeyword("td", true);
	{
		auto aField = putHtmlKeywordProperty("a", "href", m_model->FetchSubValue(key), true);
		pushToken(m_model->FetchValue(key));
	}
}

void HttpHtmlResponse::columnPutText(const std::string &key)
{
	auto tdField = putHtmlKeyword("td", true);
	{
		pushToken(m_model->FetchValue(key));
	}
}

void HttpHtmlResponse::dispositionColumns()
{
	declareColumn("Icon", ColumnType::IMAGE);
	declareColumn("Name", ColumnType::TEXTURL);
	declareColumn("Size", ColumnType::TEXT);
}

void HttpHtmlResponse::declareColumn(const std::string &key, HttpHtmlResponse::ColumnType::Type ty)
{
	using column_fn_ty = std::function<void(const std::string &)>;
	auto mapPut = [this](const std::string &key, column_fn_ty &&f)
	{
		this->m_columns.push_back(std::make_unique<ListColumn>(std::forward<std::decay_t<decltype(f)>>(f), key));
	};

	switch (ty)
	{
	case ColumnType::IMAGE: mapPut(key, [this](const std::string &k) { this->columnPutImage(k); }); break;
	case ColumnType::TEXT: mapPut(key, [this](const std::string &k) { this->columnPutText(k); }); break;
	case ColumnType::TEXTURL: mapPut(key, [this](const std::string &k) { this->columnPutTextWithUrl(k); }); break;
	default: break;
	}
}

void HttpHtmlResponse::putTableElementSingle()
{
	{
		auto trField = putHtmlKeyword("tr", true);
		{
			for (auto &&column : m_columns)
				column->Write();
		}
	}
	pushLineReturn(false);
}

void HttpHtmlResponse::putTableElement()
{
	while (m_model->Has())
	{
		putTableElementSingle();
		m_model->Next();
	}
}

void HttpHtmlResponse::putTableColumn()
{
	auto trField = putHtmlKeyword("tr", true);

	for (auto &&column : m_columns)
		putTableColumnSingle(column->Name());
}

void HttpHtmlResponse::putTable()
{
	auto tableScope = putHtmlKeyword("table", true);
	{
		putTableColumn();
		tableColSpan();
		putTableElement();
		tableColSpan();
	}
}

void HttpHtmlResponse::putBody()
{
	auto bodyScope = putHtmlKeyword("body", true);

	putCotentTitle("FILES");
	putExplanation("Description");
	putTable();
}

void HttpHtmlResponse::makeHtml()
{
	dispositionColumns();
	putHtmlMarkup();
	putTitle("directories");
	putBody();
	while (popHtmlKeyword());
}

size_t HttpHtmlResponse::makeHtmlCommon()
{
	changeHeaderValue("Content-Type", "text/html; charset=utf-8");
	makeHtml();
	return calcBodySize();
}

size_t HttpHtmlResponse::makeBody()
{
	if (m_dirLoader)
	{
		return makeHtmlCommon();
	}
	return 0;
}


