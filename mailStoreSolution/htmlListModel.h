
#ifndef HTML_LIST_MODEL_H__
#define HTML_LIST_MODEL_H__

#include <string>
#include <map>
#include <functional>

class HtmlListModel
{
protected:
	using key_handler_fn_ty = std::function<std::string()>;

private:
	std::map<std::string, key_handler_fn_ty> m_keyHandlerMap;
	std::map<std::string, key_handler_fn_ty> m_subKeyMap;

public:
	HtmlListModel();
	virtual ~HtmlListModel();

protected:
	void declareKeyHandler(const std::string &key, key_handler_fn_ty &&handler);
	void pushSubKey(const std::string &subKey, key_handler_fn_ty &&subHandler);

public:
	virtual void InitialDeclare() = 0;
	
public:
	std::string FetchValue(const std::string &key) const;
	std::string FetchSubValue(const std::string &subKey) const;
	virtual bool Has() const = 0;
	virtual void Next() = 0;
};

#endif

