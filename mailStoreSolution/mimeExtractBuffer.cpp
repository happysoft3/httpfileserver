
#include "mimeExtractBuffer.h"
#include "mimeFormatUtil/mimeContentFile.h"

class MimeExtractBuffer::Core
{
private:
	MimeExtractBuffer *m_parent;

public:
	Core(MimeExtractBuffer *parent)
		: m_parent(parent)
	{ }

	~Core()
	{ }

	MimeExtractBuffer *operator->() const
	{
		return m_parent;
	}
};

MimeExtractBuffer::MimeExtractBuffer()
	: BinaryBuffer()
{
	m_core = std::make_shared<Core>(this);
}

MimeExtractBuffer::~MimeExtractBuffer()
{
	m_core.reset();
}

void MimeExtractBuffer::sendMimeFile()
{
	int c = m_file->Read();

	if (c == EOF)
	{
		Clear();
		return;
	}
}

bool MimeExtractBuffer::CreateComponent()
{
	sendMimeFile();
	return true;
}

void MimeExtractBuffer::SetContentFile(std::unique_ptr<MimeContentFile> contentFile)
{
	if (!contentFile)
		return;

	m_contentFile = std::move(contentFile);
	m_file = m_contentFile.get();
	m_file->Open({});
}


MimeExtractBuffer::Printer::Printer(std::shared_ptr<MimeExtractBuffer::Core> parent)
	: MimeContentStreamPrinter()
{
	m_accumulate = 0;
	m_parent = parent;
}

MimeExtractBuffer::Printer::~Printer()
{ }

bool MimeExtractBuffer::Printer::Create(const std::string &url)
{
	return true;
}

size_t MimeExtractBuffer::Printer::Seekpointer() const
{
	return m_accumulate;
}

size_t MimeExtractBuffer::Printer::Write(const char *src, size_t writeBytes)
{
	auto parent = m_parent.lock();

	m_writeBuffer = std::vector<char>(src, src + writeBytes);
	m_accumulate += writeBytes;

	if (parent)
		(*parent)->StreamSet(m_writeBuffer);

	return writeBytes;
}

MimeExtractBuffer::PrinterFactory::PrinterFactory(MimeExtractBuffer &buffer)
	: MimePrinterFactory()
{
	m_parent = buffer.m_core;
}

MimeExtractBuffer::PrinterFactory::~PrinterFactory()
{ }

std::unique_ptr<MimeContentStreamPrinter> MimeExtractBuffer::PrinterFactory::Create() const
{
	auto parent = m_parent.lock();

	return std::make_unique<Printer>(parent);
}

