
#ifndef NET_OVERLAPPED_H__
#define NET_OVERLAPPED_H__

#include <ws2tcpip.h>
#include <list>
#include <memory>
#include <array>
#include <string>
#include <functional>
#include <map>

struct stNetOverlapped : public WSAOVERLAPPED	//Implicitly rule
{
	//WSAOVERLAPPED m_wsaOverlapped;
	WSABUF m_wsaBuffer;

	enum
	{
		NONE,
		ACCEPT,
		SHUTDOWN,
		RECEIVE,
		SEND
	} m_ioOperation;
	unsigned long m_realBytes;
	unsigned long m_realPostBytes;
	unsigned long m_dwFlags;
	static constexpr size_t net_buffer_max_size = 512;
	std::array<char, net_buffer_max_size> m_message;
};

class NetOverlapped
{
	using overlapped_elem_ty = std::unique_ptr<stNetOverlapped>;
	using overlapped_list_iter_ty = std::list<overlapped_elem_ty>::iterator;

public:
	static constexpr size_t net_buffer_max_size = stNetOverlapped::net_buffer_max_size;

private:
	std::map<stNetOverlapped *, overlapped_list_iter_ty> m_overlListIterMap;
	std::list<overlapped_elem_ty> m_overlappedList;

public:
	NetOverlapped();
	~NetOverlapped();

private:
	static void bufferClean(stNetOverlapped *pOverlapped);
	static void bufferCopy(stNetOverlapped *pOverlapped, const std::string &src);
	stNetOverlapped *createCommon(std::function<void(stNetOverlapped*)> &&init);

public:
	stNetOverlapped *Create();
	stNetOverlapped *CreateWithBuffer(const std::string &buffer);

private:
	overlapped_list_iter_ty getIteratorFromMap(stNetOverlapped *pOverl);

public:
	bool Delete(stNetOverlapped *pOverl);
};

#endif

