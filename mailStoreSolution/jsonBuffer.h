
#ifndef JSON_BUFFER_H__
#define JSON_BUFFER_H__

#include "common/utils/binaryBuffer.h"

class JsonBuffer : public BinaryBuffer
{
private:
	std::string m_jsStream;
	size_t m_rdPos;

public:
	explicit JsonBuffer(std::string &&jsStream);
	~JsonBuffer() override;

private:
	std::vector<uint8_t> pullStream();
	void putStream();
	bool CreateComponent() override;
};

#endif

