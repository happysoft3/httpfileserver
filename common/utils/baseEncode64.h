
#ifndef BASE_ENCODE_64_H__
#define BASE_ENCODE_64_H__

#include "binaryHelper.h"
#include <memory>

class BaseEncode64 : public BinaryHelper
{
	class InvokeImpl;
private:
	using entry_vector_ty = std::vector<int>;
	using entry_vector_iter = entry_vector_ty::reverse_iterator;
	entry_vector_ty m_entryVec;
	entry_vector_iter m_entryVecIter;
	std::vector<uint8_t> m_resultVec;

	std::shared_ptr<InvokeImpl> m_currentInvoke;
	std::shared_ptr<InvokeImpl> m_decodeInvoke;
	std::shared_ptr<InvokeImpl> m_encodeInvoke;

	//decode only//
	size_t m_bitCount;
	int m_temporary;

public:
	BaseEncode64(size_t reserveSize = 1024);
	virtual ~BaseEncode64();

private:
	int pop() override;
	void push(int value) override;

private:
	int encodePop(int c);
	void encodePush();
	void calcEncode();
	void encodeImpl();

	int decodePop(int c);
	void decodePush();
	void writeDecodeBuffer(std::vector<int> &buffer, size_t &bitCount, int &temp);
	void decodeImpl();

	void writeDecodeBufferEx(std::vector<int> &buffer);
	void decodeImplEx();

private:
	void resetSeek() override;
	template <class Container, class ImplFunction>
	void resultCopyImpl(Container &dest, ImplFunction &&fn)
	{
		resetSeek();
		if (m_resultVec.size())
			m_resultVec.clear();

		(this->*std::forward<ImplFunction>(fn))();

		dest.resize(m_resultVec.size());
		std::copy(m_resultVec.cbegin(), m_resultVec.cend(), dest.begin());
	}

public:
	template <class Container>
	void Encode(Container &dest)
	{
		resultCopyImpl(dest, &BaseEncode64::encodeImpl);
	}

	template <class Container>
	void Decode(Container &dest)
	{
		resultCopyImpl(dest, &BaseEncode64::decodeImpl);
	}
};

#endif

