
#ifndef RUN_CLOCK_H__
#define RUN_CLOCK_H__

#include <string>

class RunClock
{
private:
	double m_initClock;
	double m_latestClock;
	std::string m_fmt;

public:
	RunClock();
	~RunClock();

	std::string Get(bool reset = false);
	bool SetFormat(const std::string &fmt);
	double Sync();
};

#endif

