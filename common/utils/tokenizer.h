
#ifndef TOKENIZER_H__
#define TOKENIZER_H__

#include "binaryBuffer.h"

class Tokenizer : public BinaryBuffer
{
public:
	class Token;

private:
	class EofToken;
	class SpaceToken;
	class SymbolToken;
private:
	size_t m_byteRd;

public:
	Tokenizer();
	~Tokenizer() override;

private:
	int peek();
	int pop();
	void push(int tok);
	std::string debugLine(const std::string &debugString) override;
	std::unique_ptr<Token> readNumber(int c);
	std::unique_ptr<Token> readIdent(int c);
	bool skipWhitespaceImpl();
	bool skipWhitespace();

protected:
	std::unique_ptr<Token> readToken();

public:
	bool GetToken(std::string &dest);
};

class Tokenizer::Token
{
public:
	struct TokenType
	{
		enum Type
		{
			TIDENT,
			TEOF,
			TSPACE,
			TSYMB
		};
	};

private:
	std::string m_tokenName;

public:
	Token(const std::string &name)
		: m_tokenName(name)
	{ }
	virtual ~Token()
	{ }
	std::string Value() const
	{
		return m_tokenName;
	}
	virtual TokenType::Type Type() const
	{
		return TokenType::TIDENT;
	}
};

#endif

