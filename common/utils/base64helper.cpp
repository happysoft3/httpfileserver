
#include "base64helper.h"
#include <list>

static std::vector<char> s_encodingTable =
{
#define BASE64CASE(base64char) base64char,
#include "../identifier/base64Symbols.inc"
#undef BASE64CASE
};

static std::vector<char> s_decodeTable(255);

class DecodeTableInit
{
public:
	DecodeTableInit()
	{
		char inc = 0;

		for (const char &c : s_encodingTable)
			s_decodeTable[c] = inc++;
	}
};

static DecodeTableInit s_makeDecodeTable;

Base64Helper::Base64Helper()
	: BinaryHelper()
{ }

Base64Helper::~Base64Helper()
{ }

bool Base64Helper::IsBase64Symbol(int c)
{
	switch (c)
	{
#define BASE64CASE(base64case) case base64case:
#include "../identifier/base64Symbols.inc"
#undef BASE64CASE
		return true;
	default:
		return false;
	}
}

void Base64Helper::onBufferClear()
{
	resetSeek();
}

int Base64Helper::decodePop()
{
	for (;;)
	{
		int c = pop();

		if (!IsBase64Symbol(c))
		{
			if (c == EOF || c == '=')
				return c;
			continue;
		}
		return c;
	}
}

bool Base64Helper::fillDecodeQueue(int &padCount)
{
	int c;

	while (m_decodeQueue.size() < 4)
	{
		c = decodePop();
		if (c == EOF)
			return false;
		if (c == '=')
		{
			c = 0;
			++padCount;
		}

		m_decodeQueue.push_back(static_cast<char>(c));
	}
	return true;
}

bool Base64Helper::decodeImpl()
{
	int padCount = 0;
	if (!fillDecodeQueue(padCount))
		return false;

	int bitCount = 0, temp = 0;
	int count = m_decodeQueue.size();
	std::list<char> bytes;

	while (--count >= 0)
	{
		temp |= s_decodeTable[m_decodeQueue[count]] << bitCount;
		bitCount += 6;
		if (bitCount >= 8)
		{
			bytes.push_front(static_cast<char>(temp));
			temp >>= 8;
			bitCount -= 8;
		}
	}
	for (; padCount; --padCount)
		bytes.pop_back();
	std::copy(bytes.cbegin(), bytes.cend(), std::insert_iterator<std::vector<char>>(m_resultStream, m_resultStream.end()));
	m_decodeQueue.clear();
	return true;
}

int Base64Helper::encodePop(int &padCount)
{
	padCount = 0;
	int c = pop();

	if (c == EOF)
		return c;

	union
	{
		int ret;
		char slot[3];
	} field = { 0, };

	field.slot[2] = static_cast<char>(c);
	c = pop();
	if (c == EOF)
		++padCount;
	field.slot[1] = static_cast<char>(c);
	c = pop();
	if (c == EOF)
		++padCount;
	field.slot[0] = static_cast<char>(c);

	return field.ret;
}

bool Base64Helper::encodeImpl()
{
	int padCount = 0;
	int slots = encodePop(padCount);

	if (slots == EOF)
		return false;
	int rShift = 18, count = 4;

	for (; count; --count) // while (--count >= 0)
	{
		m_resultStream.push_back((padCount >= count) ? '=' : s_encodingTable[(slots >> rShift) & 0x3f]);
		rShift -= 6;
	}
	return true;
}

