
#ifndef FILE_TOKENIZER_FORMAT_H__
#define FILE_TOKENIZER_FORMAT_H__

#include "fileReader.h"

class CToken;
class TokenPosition;

class FileTokenizer : public FileReader
{
private:
	std::unique_ptr<TokenPosition> m_markPos;

public:
	FileTokenizer();
	~FileTokenizer() override;

private:
	void marking();
	int peek();
	bool next(int expect);
	bool isWhitespace(int c) const;
	bool doSkipSpace();
	bool skipSpace();

	std::string readHexaNumber(int zero, int symb);
	std::string readBinaryNumber(int zero, int symb);
	std::unique_ptr<CToken> readNumber(char c);
	std::unique_ptr<CToken> readIdent(char c);
	int readEscapedChar();
	std::unique_ptr<CToken> readString();
	bool calcHexChar(int &res);
	bool calcHexWord(uint8_t &output);
	std::unique_ptr<CToken> readPercentString();
	//std::unique_ptr<CToken> readAnyLine(int c);
	std::unique_ptr<CToken> readUntilLine();

protected:
	std::unique_ptr<CToken> doReadToken();

	std::unique_ptr<CToken> doReadLine();

private:
	void createdToken(std::unique_ptr<CToken> &&tok);

	template <class Ty, class... Args>
	std::unique_ptr<CToken> makeToken(Args&&... args)
	{
		//auto tok = std::make_unique<Ty>(std::forward<Args>(args)...);		//이러한 경우에,
		std::unique_ptr<CToken> tok = std::make_unique<Ty>(std::forward<Args>(args)...);

		createdToken(std::move(tok));		//!이 함수를 통해, tok 을 잃게됩니다!//그럼에도 불구하고 만약에, 실제 타입이 이 함수 인자와 같다면, 그것은 여전히 유효합니다///
		return tok;
	}

public:
	bool Open(const std::string &url);

private:
	void Process();
};

#endif


