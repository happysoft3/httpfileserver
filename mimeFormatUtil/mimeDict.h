
#ifndef MIME_DICT_H__
#define MIME_DICT_H__

#include <string>
#include <map>
#include <memory>
#include <functional>
#include <vector>

class MimeNodeEx;

//Mime 형식 분석은, 여기에서 수행합니다//
class MimeDict		//Branch 그리고, factor 으로 나눌 수 있을 듯//
{
public:			//TODO. 클래스 분기, parent 그리고 child 로// 중간에 추상 노드를, 배치하거나 해야할 듯//
	enum class ValueType
	{
		MIME_VALUE,
		SUBKEY,
		SUBVALUE
	};

private:
	class Core;
	struct MimeDictElem;
	struct MimeBoundary;

private:
	std::map<std::string, std::unique_ptr<MimeDictElem>> m_headMap;
	std::string m_latestKey;
	std::shared_ptr<Core> m_core;
	int m_createdId;		//생성 id, 노드 생성마다 1증가

	//parent
	std::shared_ptr<MimeBoundary> m_boundary;
	std::weak_ptr<Core> m_parentCore;

	//child
	std::vector<std::unique_ptr<MimeDict>> m_childVec;

public:
	MimeDict(MimeDict *parent = nullptr);
	virtual ~MimeDict();

private:
	MimeDictElem *makeElem(const std::string &value);
	MimeBoundary *makeBoundary(const std::string &uniqKey);
	bool retrieveMap(const std::string &key, std::function<void(MimeDictElem &)> fn) const;

public:
	int Length() const
	{
		return m_headMap.size();
	}
	std::string BoundaryKey() const;
	bool HasKey(const std::string &key) const;
	bool MatchWithLatestKey(const std::string &key) const;
	void Put(std::unique_ptr<MimeNodeEx> node);
	std::string Value(const std::string &key, ValueType ty = ValueType::MIME_VALUE);

private:
	void readContentType(MimeDictElem &dictElem);

public:
	bool HasBoundaryKey() const;
	void MimePreorder();

private:
	std::shared_ptr<MimeBoundary> loadBoundary();
	MimeDict *getBoundaryOwner();
	MimeDict *getParent() const;

protected:
	size_t getChildCount() const;
	MimeDict *getChild(size_t index);

private:
	virtual void onAddedChild(const MimeDict &child);
	virtual void onExpiredBoundary();

public:
	bool MatchBoundaryKey(std::unique_ptr<MimeNodeEx> &&boundary, bool silence = false);
	void PushChild(std::unique_ptr<MimeDict> child);
	void ExitBoundary();

protected:
	using node_travel_fn_ty = std::function<void(MimeDict *)>;
	virtual void traversalPreorder(node_travel_fn_ty &&fn);		//모든 노드 경유
	void traversalCurrentChild(node_travel_fn_ty &&fn);	//현재 노드의, 자식 노드만 경유
	using traversal_ty = decltype(&MimeDict::traversalCurrentChild);
	void childTraversal(node_travel_fn_ty &&fn, traversal_ty traversal, MimeDict &from);
};

#endif

