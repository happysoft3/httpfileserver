
#include "abstractMimeContent.h"
#include "mimeHeader.h"
#include "mimeError.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

class AbstractMimeContent::Core
{
private:
	AbstractMimeContent *m_owner;

public:
	Core(AbstractMimeContent *owner)
		: m_owner(owner)
	{ }

	~Core()
	{ }

	AbstractMimeContent *Owner() const
	{
		return m_owner;
	}
	AbstractMimeContent *operator->() const
	{
		return m_owner;
	}
};

AbstractMimeContent::AbstractMimeContent(AbstractMimeContent *parent)
	: m_core(new Core(this)), m_nodeId(0)
{
	if (parent)
		m_parentCore = parent->m_core;
}

AbstractMimeContent::~AbstractMimeContent()
{
	m_core.reset();
}

void AbstractMimeContent::onNotifyAddChild(const AbstractMimeContent &child)
{
	AbstractMimeContent *parent = getParent();

	if (parent)
		parent->onNotifyAddChild(child);
}

void AbstractMimeContent::PutSequence(AbstractMimeContent::Numbering &seq)
{
	onSetSequence(seq);
}

void AbstractMimeContent::PutHeader(std::unique_ptr<MimeHeader> header)
{
	m_header.swap(header);
	if (m_header)
		onPutHeader(*m_header);
}

void AbstractMimeContent::PushChild(std::unique_ptr<AbstractMimeContent> &&child)
{
	onNotifyAddChild(*child);
	onAddedChild(std::forward<std::unique_ptr<AbstractMimeContent>>(child));
}

bool AbstractMimeContent::MatchBoundaryKey(const std::string &boundaryKey)
{
	AbstractMimeContent *owner = m_header->HasBoundaryKey() ? this : getParent();
	
	if (!owner)
		return false;
	
	if (!((*owner)->CheckBoundaryKey(boundaryKey)))
		return false;

	return true;
}

void AbstractMimeContent::traversalPreorderInvoke(AbstractMimeContent::mime_traversal_fn_ty &&fn, AbstractMimeContent *node)
{
	if (node)
		node->traversalPreorder(std::forward<mime_traversal_fn_ty>(fn));
}

void AbstractMimeContent::traversalPreorder(AbstractMimeContent::mime_traversal_fn_ty &&fn)
{
	std::forward<mime_traversal_fn_ty>(fn)(this);
}

AbstractMimeContent *AbstractMimeContent::getParent() const
{
	auto parentCore = m_parentCore.lock();

	return parentCore ? parentCore->Owner() : nullptr;
}

std::string AbstractMimeContent::GetNodeIdString() const
{
	return std::to_string(m_nodeId);
}

AbstractMimeContent *AbstractMimeContent::findContentInvoke(int nodeId, int itemId, AbstractMimeContent &node)
{
	return node.findContent(nodeId, itemId);
}

AbstractMimeContent *AbstractMimeContent::findContent(int /*nodeId*/, int itemId)
{
	if (m_nodeId == itemId)
		return this;

	return nullptr;
}

