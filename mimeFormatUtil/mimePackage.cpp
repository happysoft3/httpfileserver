
#include "mimePackage.h"
#include "mimeFileExtractor.h"
#include "mimeHeader.h"
#include "mimeAttach.h"
#include "common/include/incFilesystem.h"

MimePackage::MimePackage()
	: MimeMultipart()
{ }

MimePackage::~MimePackage()
{ }

void MimePackage::SetUrl(const std::string &url)
{
	m_url.reserve(url.size());
	char prev = 0;

	for (char c : url)
	{
		if (c == '/')
			c = '\\';

		if (c == '\\' && prev == '\\') {}
		else
			m_url.push_back(c);
		prev = c;
	}
}

void MimePackage::makeChildList(MimePackage::PackageItemList &itemList, AbstractMimeContent *child)
{
	if (child == this)
		return;

	std::unique_ptr<PackageItem> item(new PackageItem(*child));

	itemList.Push(std::move(item));
}

void MimePackage::makeAttachImpl(const std::string &key, MimeAttach **pAttach, AbstractMimeContent &content)
{
	if (key == content.GetNodeIdString())
	{
		MimeAttach *attach = new MimeAttach;

		attach->PutMimeContent(content);	//content 정보를 장착합니다//
		attach->SetInputUrl(m_url);		//입력 파일url을 공유합니다//
		*pAttach = attach;
	}
}

void MimePackage::makeFileKeymapImpl(AbstractMimeContent &content)
{
	if (content->Value(MimeHeader::mime_header_content_disposition) == "attachment")
	{
		if (content->Value(MimeHeader::mime_header_content_disposition, MimeHeader::ValueType::SUBKEY) != "filename")
			return;

		std::string key = content->Value(MimeHeader::mime_header_content_disposition, MimeHeader::ValueType::SUBVALUE);
		std::string valueKey = content.GetNodeIdString();

		m_fileKeyMap[key] = valueKey;
	}
}

int MimePackage::MakeFileKeymap()
{
	traversalPreorder([this](AbstractMimeContent *content) { makeFileKeymapImpl(*content); });

	return static_cast<int>(m_fileKeyMap.size());
}

std::unique_ptr<MimeFileExtractor> MimePackage::MakeExtractor(FileReader &reader)
{
	MimeFileExtractor *extractor = new MimeFileExtractor;

	extractor->PutSourceContext(reader);
	traversalPreorder([extractor](AbstractMimeContent *elem) { extractor->Include(*elem); });		//미리 캐시//
	return std::unique_ptr<MimeFileExtractor>(extractor);
}

std::unique_ptr<MimePackage::PackageItemList> MimePackage::MakeAsList()
{
	PackageItemList *itemList = new PackageItemList;

	traversalPreorder([this, items = itemList](AbstractMimeContent *elem) { this->makeChildList(*items, elem); });
	return std::unique_ptr<PackageItemList>(itemList);
}

std::unique_ptr<MimeAttach> MimePackage::CreateAttach(const std::string &key)
{
	MimeAttach *attach = nullptr;

	traversalPreorder([pAttach = &attach, this, key](AbstractMimeContent *elem) { makeAttachImpl(key, pAttach, *elem); });
	if (attach == nullptr)
		return {};

	return std::unique_ptr<MimeAttach>(attach);
}

std::string MimePackage::RetrieveKeyWithFilename(const std::string &fileName) const
{
	auto keyIterator = m_fileKeyMap.find(fileName);

	if (keyIterator == m_fileKeyMap.cend())
		return {};

	return keyIterator->second;
}

////PackgeItem////

MimePackage::PackageItem::PackageItem(AbstractMimeContent &mime)
{
	m_encode = mime->Value(MimeHeader::mime_header_transfer_encoding);
	m_itemId = mime.GetNodeIdString();
	m_contentId = mime->Value("Content-ID");
	setContentTypeSub(mime);
	m_contentStartLine = mime->StartLine();
	m_contentEndLine = mime->EndLine();
	setContentFileName(mime);
}

MimePackage::PackageItem::~PackageItem()
{ }

void MimePackage::PackageItem::setContentFileName(AbstractMimeContent &mime)
{
	if (mime->Value(MimeHeader::mime_header_content_disposition, MimeHeader::ValueType::SUBKEY) == "filename")
	{
		std::string url = mime->Value(MimeHeader::mime_header_content_disposition, MimeHeader::ValueType::SUBVALUE);

		if (url.empty())
			return;

		m_contentName = NAMESPACE_FILESYSTEM::path(url).u8string();
	}
}

void MimePackage::PackageItem::setTypeExtension(AbstractMimeContent &mime)
{
	m_extension = mime->GetContentSubType();
	m_type = mime->GetContentMainType();
}

void MimePackage::PackageItem::setContentTypeSub(AbstractMimeContent &mime)
{
	m_contentTySub = mime->Value(MimeHeader::mime_header_content_type, MimeHeader::ValueType::SUBKEY);
	m_contentTySubValue = mime->Value(MimeHeader::mime_header_content_type, MimeHeader::ValueType::SUBVALUE);
	setTypeExtension(mime);
}

std::string MimePackage::PackageItem::getContentTypeOption(MimePackage::PackageItem::ItemType::Ty contentTyKey) const
{
	switch (contentTyKey)
	{
	case ItemType::CONTENT_TYPE: return m_type;
	case ItemType::CONTENT_EXTENSION: return m_extension;
	case ItemType::CONTENT_TYPE_SUBKEY: return m_contentTySub;
	case ItemType::CONTENT_TYPE_SUBVALUE: return m_contentTySubValue;
		//case ItemType::CONTENT_TYPE_SUBKEY: return 
	default: return {};
	}
}

std::string MimePackage::PackageItem::GetMetaData(MimePackage::PackageItem::ItemType::Ty key) const
{
	switch (key)
	{
	case ItemType::CONTENT_TYPE_SUBKEY:
	case ItemType::CONTENT_TYPE_SUBVALUE:
	case ItemType::CONTENT_EXTENSION:
	case ItemType::CONTENT_TYPE: return getContentTypeOption(key);
	case ItemType::ENCODE_TYPE: return m_encode;
	case ItemType::ITEM_URL:
	case ItemType::DISPOSITION: return m_contentName;
	case ItemType::ITEM_ID: return m_itemId;
	case ItemType::CONTENT_ID: return m_contentId;
	case ItemType::FIELD_START_LINE: return std::to_string(m_contentStartLine);			//내부 테스트 목적//
	case ItemType::FIELD_END_LINE: return std::to_string(m_contentEndLine);				//내부 테스트 목적//
	default: return {};
	}
}

//////

/////PackageItemList/////

MimePackage::PackageItemList::PackageItemList()
{
	m_itemPicker = m_items.end();
}

MimePackage::PackageItemList::~PackageItemList()
{ }

bool MimePackage::PackageItemList::Has() const
{
	return m_items.size() > 0;
}

bool MimePackage::PackageItemList::IsEnd() const
{
	return m_itemPicker != m_items.end();
}

void MimePackage::PackageItemList::Push(std::unique_ptr<MimePackage::PackageItem> item)
{
	m_items.push_front(std::move(item));
	m_itemPicker = m_items.begin();
}

void MimePackage::PackageItemList::Next()
{
	if (m_itemPicker == m_items.end())
		m_itemPicker = m_items.begin();

	++m_itemPicker;
}

MimePackage::PackageItem *MimePackage::PackageItemList::currentItem()
{
	if (m_items.empty())
		return nullptr;

	return m_itemPicker->get();
}

//////
