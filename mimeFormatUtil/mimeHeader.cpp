
#include "mimeHeader.h"
#include "mimeError.h"
#include "mimeNodeEx.h"
#include "ctoken.h"
#include "common/utils/qPrintEncode.h"
#include "common/utils/stringhelper.h"
#include <list>

using namespace _StringHelper;

static char s_content_type_key[] = "Content-Type";
static char s_content_dispos_key[] = "Content-Disposition";

struct MimeHeader::HeaderElem
{
	std::string m_value;
	std::string m_subKey;
	std::string m_subValue;
	std::unique_ptr<MimeNodeEx> m_mimeNode;
};

struct MimeHeader::MimeBoundary
{
	std::string m_uniqueKey;
	std::list<int> m_lines;
};

MimeHeader::MimeHeader()
{
	pushSpecialKeyFunction(s_content_type_key, [this](const std::string &key) { this->readContentType(key); });
	pushSpecialKeyFunction(s_content_dispos_key, [this](const std::string &key) { this->readContentDisposition(key); });
}

MimeHeader::~MimeHeader()
{ }

void MimeHeader::pushSpecialKeyFunction(const std::string &key, std::function<void(const std::string&)> &&fn)
{
	if (m_specialKeyFnIterMap.find(key) != m_specialKeyFnIterMap.cend())
		return;

	m_specialKeyFnList.push_back(std::forward<std::decay_t<decltype(fn)>>(fn));
	m_specialKeyFnIterMap.emplace(key, std::prev(m_specialKeyFnList.end()));
}

void MimeHeader::popSpecialKeyFunctionAfterInvoke(const std::string &key)
{
	auto fnIterator = m_specialKeyFnIterMap.find(key);

	if (fnIterator == m_specialKeyFnIterMap.cend())
		return;

	(*fnIterator->second)(key);
	m_specialKeyFnList.erase(fnIterator->second);
	m_specialKeyFnIterMap.erase(fnIterator);
}

MimeHeader::HeaderElem *MimeHeader::makeHeaderCore(const std::string &value)
{
	HeaderElem *header = new HeaderElem;

	header->m_value = value;
	return header;
}

bool MimeHeader::retrieveMap(const std::string &key, std::function<void(MimeHeader::HeaderElem &)> fn) const
{
	auto keyIterator = m_headerMap.find(key);

	if (keyIterator != m_headerMap.cend())
	{
		if (fn)
			fn(*(*keyIterator->second));
		return true;
	}
	return false;
}

int MimeHeader::StartLine() const
{
	if (m_boundaryOffsetList.empty())
		return -1;

	auto &first = m_boundaryOffsetList.front();

	return first.line;
}

int MimeHeader::EndLine() const
{
	if (m_boundaryOffsetList.empty())
		return -1;

	auto &last = m_boundaryOffsetList.back();

	return last.line;
}

size_t MimeHeader::EntryOffset() const
{
	auto &first = m_boundaryOffsetList.front();

	return first.offset;
}

size_t MimeHeader::EndOffset() const
{
	auto &last = m_boundaryOffsetList.back();

	return last.offset;
}

void MimeHeader::PushBoundaryOffset(CToken &tok)
{
	m_boundaryOffsetList.push_back({ tok.Offset(), tok.Line() });
}

bool MimeHeader::MatchWithLatestKey(const std::string &key) const
{
	if (m_latestKey.empty())
		return false;

	return m_latestKey == key;
}

bool MimeHeader::HasKey(const std::string &key) const
{
	return retrieveMap(key, {});
}

void MimeHeader::Put(std::unique_ptr<MimeNodeEx> node)
{
	m_latestKey = {};

	if (node->Type() == MimeNodeType::Ident)
	{
		m_latestKey = node->Extra(&MimeCoreBase::MimeIdent::Key);
		if (m_latestKey.empty())
			return;

		std::unique_ptr<HeaderElem> elem(makeHeaderCore(node->Extra(&MimeCoreBase::MimeIdent::Value)));
		MimeNodeEx *subPair = nullptr;

		if (node->Extra(&MimeCoreBase::MimeIdent::GetSubPair, subPair))
		{
			elem->m_subKey = subPair->Extra(&MimeCoreBase::MimePair::Key);
			elem->m_subValue = subPair->Extra(&MimeCoreBase::MimePair::Value);
		}
		elem->m_mimeNode.swap(node);
		m_headerList.push_back(std::move(elem));
		m_headerMap[m_latestKey] = std::prev(m_headerList.end());

		popSpecialKeyFunctionAfterInvoke(m_latestKey);
	}
}

std::string MimeHeader::Value(const std::string &key, MimeHeader::ValueType ty) const
{
	std::shared_ptr<std::string> value = std::make_shared<std::string>();

	switch (ty)
	{
	case ValueType::MIME_VALUE:
		retrieveMap(key, [value](HeaderElem &elem) { *value = elem.m_value; });
		break;

	case ValueType::SUBKEY:
		retrieveMap(key, [value](HeaderElem &elem) { *value = elem.m_subKey; });
		break;

	case ValueType::SUBVALUE:
		retrieveMap(key, [value](HeaderElem &elem) { *value = elem.m_subValue; });
		break;

	default:
		return {};
	}
	return *value;
}

void MimeHeader::SetValue(const std::string &key, const std::string &value, MimeHeader::ValueType ty)
{
	switch (ty)
	{
	case ValueType::MIME_VALUE:
		retrieveMap(key, [value](HeaderElem &elem) { elem.m_value = value; });
		break;
		
	case ValueType::SUBVALUE:
		retrieveMap(key, [value](HeaderElem &elem) { elem.m_subValue = value; });
		//!Fallthrough!//
	default:
		break;
	}
}

void MimeHeader::ThrowNodeException(const std::string &key, const std::string &exceptName)
{
	std::unique_ptr<MimeNodeEx> exceptNode;

	retrieveMap(key, [&exceptNode](HeaderElem &elem) { exceptNode.swap(elem.m_mimeNode); });
	throw std::make_unique<MimeError>(std::move(exceptNode), exceptName);
}

MimeHeader::MimeBoundary *MimeHeader::createBoundary(const std::string &uniqKey)
{
	MimeBoundary *boundary = new MimeBoundary;

	boundary->m_uniqueKey = uniqKey;
	return boundary;
}

void MimeHeader::readContentType(const std::string &key)
{
	std::string mainTy = GetContentMainType();

	///멀티파트 이면, 바운더리, 아니면 오류///
	if (mainTy == "multipart")
	{
		std::string contentTySubkey = Value(key, MimeHeader::ValueType::SUBKEY);
		if (contentTySubkey != "boundary")
			ThrowNodeException(key, stringFormat("expect 'boundary' but got %s", contentTySubkey));

		std::string contentTySub = Value(key, MimeHeader::ValueType::SUBVALUE);

		if (contentTySub.empty())
			ThrowNodeException(key, "boundary key is empty");

		m_boundary = std::shared_ptr<MimeBoundary>(createBoundary(contentTySub));
	}
}

void MimeHeader::readContentDisposition(const std::string &key)
{
	std::string subTyKey = Value(key, ValueType::SUBKEY);

	if (subTyKey == "filename")
	{
		std::shared_ptr<QPrintEncode> qEncode(new QPrintEncode);

		retrieveMap(key, [encode = std::move(qEncode)](HeaderElem &elem) mutable
		{
			encode->StreamSet(elem.m_subValue);
			if (encode->Decode())
				elem.m_subValue = encode->Value();
		});
	}
}

std::string MimeHeader::GetContentType() const
{
	return Value(s_content_type_key);
}

std::string MimeHeader::GetContentMainType() const
{
	std::string ty = GetContentType();
	
	if (ty.empty())
		return ty;
	size_t seperator = ty.find_first_of('/');

	return ty.substr(0, seperator);
}

std::string MimeHeader::GetContentSubType() const
{
	std::string ty = GetContentType();

	if (ty.empty())
		return ty;

	size_t seperator = ty.find_first_of('/');

	return ty.substr(seperator + 1);
}

bool MimeHeader::HasBoundaryKey() const
{
	if (!m_boundary)
		return false;

	return true;
}

bool MimeHeader::CheckBoundaryKey(const std::string &boundaryKey)
{
	if (!m_boundary)
		return false;

	if (m_boundary->m_uniqueKey == boundaryKey)
	{
		//m_boundary->m_lines.push_back(boundary.Line());
		return true;
	}
	return false;
}

bool MimeHeader::EraseKey(const std::string &key)
{
	auto keyIterator = m_headerMap.find(key);

	if (keyIterator == m_headerMap.cend())
		return false;

	m_headerList.erase(keyIterator->second);
	m_headerMap.erase(keyIterator);
	return true;
}



