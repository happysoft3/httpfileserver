
#ifndef MIME_ERROR_H__
#define MIME_ERROR_H__

#include <string>
#include <memory>

class CToken;
class MimeNodeEx;
class AbstractMimeContent;

class MimeError
{
	class BaseError;
	class TokenError;
	class NodeError;
	class ContentError;
private:
	std::unique_ptr<BaseError> m_error;
	std::string m_errMessage;

public:
	MimeError(std::unique_ptr<CToken> tok, const std::string &errMessage);
	MimeError(std::unique_ptr<MimeNodeEx> node, const std::string &errMessage);
	MimeError(const AbstractMimeContent *content, const std::string &errMessage);
	~MimeError();

	std::string What() const;
};

#endif

