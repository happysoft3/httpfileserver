
#ifndef C_TOKEN_H__
#define C_TOKEN_H__

#include <string>
#include <memory>

class TokenPosition;

enum class CTokenType
{
	Ident,
	Number,
	Space,
	Newline,
	Eof,
	Invalid,
	String,
	Keyword,
	Anyline
};

class CToken
{
private:
	CTokenType m_ty;
	std::unique_ptr<TokenPosition> m_pos;
	int m_tokenChar;

public:
	CToken(CTokenType ty, int tokenChar = 0);
	CToken(const CToken &other);
	virtual ~CToken();

private:
	virtual std::string tokenPosition(int l, int c) const;
	virtual std::string strValue() const;
	virtual std::string errorValue() const;
	virtual bool errorTriggered() const { return false; }

protected:
	int tokenChar() const
	{
		return m_tokenChar;
	}
	std::string typeToStr() const;

public:
	void SetPos(const TokenPosition &pos);
	std::string Error() const;
	virtual std::string Print() const;
	int Line() const;
	size_t Offset() const;

public:
	CTokenType Type() const
	{
		return m_ty;
	}

	bool IsType(CTokenType ty) const
	{
		return m_ty == ty;
	}

	bool IsChar(int ch) const
	{
		if (!m_tokenChar)
			return false;

		return m_tokenChar == ch;
	}
	std::string Value() const
	{
		return strValue();
	}
	virtual CToken *Clone() const = 0;
};

class CTokenIdent : public CToken
{
protected:
	std::string m_svalue;

public:
	CTokenIdent(CTokenType ty, const std::string &ident);
	CTokenIdent(const std::string &ident);
	~CTokenIdent() override;

private:
	std::string strValue() const override;
	CTokenIdent *Clone() const override;
};

class CTokenString : public CTokenIdent
{
public:
	CTokenString(CTokenType ty, const std::string &s);
	CTokenString(const std::string &s);
	~CTokenString() override;

private:
	std::string strValue() const override;
};

class CTokenNumber : public CTokenString
{
public:
	CTokenNumber(const std::string &numberString);
	~CTokenNumber() override;
};

class CTokenKeyword : public CToken
{
public:
	struct ExtendType
	{
		enum
		{
			TY_DOUBLE_MINUS = 256,
		};
	};

public:
	CTokenKeyword(int ch, CTokenType ty = CTokenType::Keyword);
	CTokenKeyword(CTokenType ty);
	~CTokenKeyword() override;

private:
	std::string strValue() const override;
	CTokenKeyword *Clone() const override;
};

class CTokenSpace : public CTokenKeyword
{
public:
	CTokenSpace();
	~CTokenSpace() override;

private:
	std::string strValue() const override;
};

class CTokenNewline : public CTokenKeyword
{
public:
	CTokenNewline();
	~CTokenNewline() override;

private:
	std::string strValue() const override;
};

class CTokenEof : public CTokenKeyword
{
public:
	CTokenEof();
	~CTokenEof() override;

private:
	std::string strValue() const override;
};

class CTokenInvalid : public CTokenKeyword
{
private:
	std::string m_err;

public:
	CTokenInvalid(const std::string &err, int c);
	~CTokenInvalid() override;

private:
	std::string errorValue() const override
	{
		return m_err;
	}
	std::string strValue() const override;
	bool errorTriggered() const override { return true; }
	CTokenInvalid *Clone() const override;
};

class CTokenAnyLine : public CTokenIdent
{
public:
	CTokenAnyLine(const std::string &anyStr);
	~CTokenAnyLine() override;
};

#endif

