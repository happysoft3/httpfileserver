
#ifndef ABSTRACT_MIME_CONTENT_H__
#define ABSTRACT_MIME_CONTENT_H__

#include <memory>
#include <functional>
#include <string>

class MimeHeader;

class AbstractMimeContent
{
	class Core;

public:
	class Numbering;

private:
	std::unique_ptr<MimeHeader> m_header;
	std::shared_ptr<Core> m_core;
	std::weak_ptr<Core> m_parentCore;

	int m_nodeId;

public:
	AbstractMimeContent(AbstractMimeContent *parent = nullptr);
	virtual ~AbstractMimeContent();

private:
	virtual void onNotifyAddChild(const AbstractMimeContent &child);
	virtual void onAddedChild(std::unique_ptr<AbstractMimeContent> &&child)
	{ }
	virtual void onSetSequence(Numbering &seq)
	{ }
	virtual void onPutHeader(MimeHeader &header)
	{ }

public:
	void PutSequence(Numbering &seq);
	void PutHeader(std::unique_ptr<MimeHeader> header);
	void PushChild(std::unique_ptr<AbstractMimeContent> &&child);
	bool MatchBoundaryKey(const std::string &boundaryKey);

protected:
	using mime_traversal_fn_ty = std::function<void(AbstractMimeContent *)>;
	void traversalPreorderInvoke(mime_traversal_fn_ty &&fn, AbstractMimeContent *node);
	virtual void traversalPreorder(mime_traversal_fn_ty &&fn);
	AbstractMimeContent *getParent() const;

public:
	virtual std::string GetNodeIdString() const;

protected:
	AbstractMimeContent *findContentInvoke(int nodeId, int itemId, AbstractMimeContent &node);
	virtual AbstractMimeContent *findContent(int nodeId, int itemId);

public:
	void SetNodeId(int id)
	{
		m_nodeId = id;
	}
	int GetNodeId() const
	{
		return m_nodeId;
	}
	MimeHeader *operator->() const
	{
		return m_header.get();
	}
};

class AbstractMimeContent::Numbering
{
private:
	int m_sequence;

public:
	Numbering(int init = 1)
		: m_sequence(init)
	{ }
	~Numbering()
	{ }

	int Get()
	{
		return m_sequence++;
	}
};

#endif

