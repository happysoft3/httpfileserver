
#ifndef MIME_READER_H__
#define MIME_READER_H__

#include "fileTokenizer.h"
#include <stack>
#include <functional>

class MimeNodeEx;
class MimeError;

class AbstractMimeContent;
class MimeMultipart;
class MimeHeader;
class MimePackage;

//Mime 스트림 및 토큰 입출력은, 여기에서 수행합니다
class MimeReader : public FileTokenizer
{
	class ContentNumbering;

private:
	std::vector<std::unique_ptr<CToken>> m_tokens;
	std::shared_ptr<MimeNodeEx> m_error;
	std::stack<AbstractMimeContent *> m_root;

	using reader_fn_ty = std::function<std::unique_ptr<CToken>()>;
	std::stack<reader_fn_ty> m_readerFnStack;
	AbstractMimeContent *m_topLevels;			//작업이 끝날 때 까지, nullptr 일 수 없습니다//

	std::string m_lastError;

	std::unique_ptr<ContentNumbering> m_seq;

public:
	MimeReader();
	~MimeReader() override;

private:
	void readerSwitching(reader_fn_ty &&fn = {});
	void pushMimeScope(AbstractMimeContent &dict);
	AbstractMimeContent *popMimeScope();
	std::unique_ptr<CToken> getToken();
	void ungetToken(std::unique_ptr<CToken> tok);
	CToken *peekToken();

	bool expectKeyword(int expect);
	void readInscope(MimeNodeEx &node);
	void readOutscope(MimeNodeEx &node);
	std::unique_ptr<MimeNodeEx> readExpr();
	std::unique_ptr<MimeNodeEx> readIdent();
	std::unique_ptr<MimeNodeEx> readProperty();
	std::unique_ptr<MimeNodeEx> readLine();
	void ignoreHeaderValue(size_t ignoreByteCount);

	void readMailHeader(AbstractMimeContent &content);

	void skipSpaceNewline();

	//added fork
	bool readMimeBoundaryDirect(CToken &boundary);
	void tryReadMimeBoundary();

	///

	std::unique_ptr<MimeHeader> readMimePairField();
	void doReadMimeHeader();
	void assertIfUnterminatedLevel();
	void readMime();

public:
	bool LastError(std::string &err) const;
	std::unique_ptr<MimePackage> Parse();
};

#endif

