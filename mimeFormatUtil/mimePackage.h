
#ifndef MIME_PACKAGE_H__
#define MIME_PACKAGE_H__

#include "mimeMultipart.h"
#include <list>
#include <map>

class FileReader;
class MimeFileExtractor;

class MimeAttach;

/////멀티파트를 직렬화////
class MimePackage : public MimeMultipart
{
public:
	class PackageItem;
	class PackageItemList;

private:
	std::string m_url;
	std::map<std::string, std::string> m_fileKeyMap;

public:
	MimePackage();
	~MimePackage() override;

public:
	void SetUrl(const std::string &url);
	
private:
	void makeChildList(PackageItemList &itemList, AbstractMimeContent *elem);
	void makeAttachImpl(const std::string &key, MimeAttach **pAttach, AbstractMimeContent &content);
	void makeFileKeymapImpl(AbstractMimeContent &content);

public:
	int MakeFileKeymap();
	std::unique_ptr<MimeFileExtractor> MakeExtractor(FileReader &reader);
	std::unique_ptr<PackageItemList> MakeAsList();
	std::unique_ptr<MimeAttach> CreateAttach(const std::string &key);
	std::string RetrieveKeyWithFilename(const std::string &fileName) const;
};

class MimePackage::PackageItem
{
public:
	struct ItemType
	{
		enum Ty
		{
			CONTENT_TYPE,
			CONTENT_EXTENSION,
			ENCODE_TYPE,
			DISPOSITION,
			ITEM_ID,
			CONTENT_ID,
			CONTENT_TYPE_SUBKEY,
			CONTENT_TYPE_SUBVALUE,
			FIELD_START_LINE,	//test
			FIELD_END_LINE,		//test
			ITEM_URL,
		};
	};

private:
	std::string m_type;
	std::string m_extension;
	std::string m_encode;
	std::string m_contentName;
	std::string m_itemId;
	std::string m_contentId;
	std::string m_contentTySub;
	std::string m_contentTySubValue;
	int m_contentStartLine;
	int m_contentEndLine;

public:
	PackageItem(AbstractMimeContent &mime);
	~PackageItem();

private:
	void setContentFileName(AbstractMimeContent &mime);
	void setTypeExtension(AbstractMimeContent &mime);
	void setContentTypeSub(AbstractMimeContent &mime);
	std::string getContentTypeOption(ItemType::Ty contentTyKey) const;

public:
	std::string GetMetaData(ItemType::Ty key) const;
};

class MimePackage::PackageItemList
{
private:
	using package_item_list_ty = std::list<std::unique_ptr<PackageItem>>;
	package_item_list_ty m_items;
	package_item_list_ty::iterator m_itemPicker;

public:
	PackageItemList();
	~PackageItemList();

public:
	bool Has() const;
	bool IsEnd() const;
	void Push(std::unique_ptr<PackageItem> item);
	void Next();

private:
	PackageItem *currentItem();

	template <class Fn>
	struct FunctionHelper;

	template <class RetTy, class Ty>
	struct FunctionHelperBase
	{
		using ret_type = RetTy;
		using object_type = Ty;
	};

	template <class Ret, class Ty, class... Args>
	struct FunctionHelper<Ret(Ty::*)(Args...)> : public FunctionHelperBase<Ret, Ty>
	{ };

	template <class Ret, class Ty, class... Args>
	struct FunctionHelper<Ret(Ty::*)(Args...)const> : public FunctionHelperBase<Ret, Ty>
	{ };

public:		//Access Current Item
	template <class Method, class... Args>
	auto Get(Method &&fn, Args&&... args)
		-> typename FunctionHelper<std::decay_t<decltype(fn)>>::ret_type
	{
		PackageItem *item = currentItem();

		return (item->*std::forward<Method>(fn))(std::forward<Args>(args)...);
	}
};

#endif

