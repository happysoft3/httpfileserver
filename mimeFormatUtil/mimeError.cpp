
#include "mimeError.h"
#include "abstractMimeContent.h"
#include "mimeHeader.h"
#include "mimeNodeEx.h"
#include "ctoken.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

class MimeError::BaseError
{
public:
	BaseError()
	{ }

	virtual ~BaseError()
	{ }

	virtual std::string What() const = 0;
};

class MimeError::TokenError : public MimeError::BaseError
{
private:
	std::unique_ptr<CToken> m_tok;

public:
	TokenError(std::unique_ptr<CToken> tok)
		: BaseError()
	{
		m_tok.swap(tok);
	}
	~TokenError() override
	{ }

private:
	std::string What() const override
	{
		return m_tok->Error();
	}
};

class MimeError::NodeError : public MimeError::BaseError
{
private:
	std::unique_ptr<MimeNodeEx> m_node;

public:
	NodeError(std::unique_ptr<MimeNodeEx> node)
		: BaseError()
	{
		m_node.swap(node);
	}
	~NodeError() override
	{ }

private:
	std::string What() const override
	{
		return m_node->Error();
	}
};

class MimeError::ContentError : public MimeError::BaseError
{
private:
	const AbstractMimeContent *m_content;

public:
	ContentError(const AbstractMimeContent *content)
		: BaseError(), m_content(content)
	{
	}
	~ContentError() override
	{ }

private:
	std::string What() const override
	{
		if (!m_content)
			return "null content";

		return stringFormat("mmcontent-%d", (*m_content)->StartLine());
	}
};

MimeError::MimeError(std::unique_ptr<CToken> tok, const std::string &errMessage)
	: m_errMessage(errMessage)
{
	m_error = std::make_unique<TokenError>(std::move(tok));
}

MimeError::MimeError(std::unique_ptr<MimeNodeEx> node, const std::string &errMessage)
	: m_errMessage(errMessage)
{
	m_error = std::make_unique<NodeError>(std::move(node));
}

MimeError::MimeError(const AbstractMimeContent *content, const std::string &errMessage)
	: m_errMessage(errMessage)
{
	m_error = std::make_unique<ContentError>(content);
}

MimeError::~MimeError()
{ }

std::string MimeError::What() const
{
	return stringFormat("%s--%s", m_errMessage, m_error->What());
}

