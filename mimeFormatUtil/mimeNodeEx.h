
#ifndef MIME_NODE_EX_H__
#define MIME_NODE_EX_H__

#include <memory>
#include <list>
#include <functional>

class CToken;
class MimeNodeEx;

enum class MimeNodeType
{
	Base,
	Pair,
	Ident
};

class MimeCoreBase
{
public:
	class MimePair;
	class MimeIdent;

	using token_ty = std::unique_ptr<CToken>;
	using token_list_ty = std::list<token_ty>;
	using token_list_const_iter = token_list_ty::const_iterator;

private:
	token_list_ty m_tokens;

public:
	explicit MimeCoreBase(std::unique_ptr<CToken> tok = {});
	virtual ~MimeCoreBase();

private:
	void releaseString(std::string &dest, token_list_const_iter iter, size_t initSize = 0);

public:
	std::string ToStr();

public:
	void Append(std::unique_ptr<CToken> tok);
	void Append(MimeCoreBase &other);
	virtual MimeNodeType Type() const
	{
		return MimeNodeType::Base;
	}
	virtual int GetLineFromFirstToken() const;

private:
	int stringMatch(const std::list<std::string> &src, std::function<bool(const std::string &, const std::string &)> &&cond);

public:
	std::string GetError() const;
	int MatchList(const std::list<std::string> &srcList);
};

class MimeCoreBase::MimePair : public MimeCoreBase
{
private:
	std::unique_ptr<MimeNodeEx> m_key;
	std::unique_ptr<MimeNodeEx> m_value;

public:
	MimePair();
	explicit MimePair(std::unique_ptr<MimeNodeEx> key, std::unique_ptr<MimeNodeEx> value);
	~MimePair() override;

public:
	std::string Key();
	std::string Value();

private:
	int GetLineFromFirstToken() const override;

public:
	MimeNodeType Type() const override
	{
		return MimeNodeType::Pair;
	}
};

class MimeCoreBase::MimeIdent : public MimeCoreBase::MimePair
{
private:
	std::unique_ptr<MimeNodeEx> m_subexpr;

public:
	MimeIdent();
	explicit MimeIdent(std::unique_ptr<MimeNodeEx> key, std::unique_ptr<MimeNodeEx> value, std::unique_ptr<MimeNodeEx> subexpr);
	~MimeIdent() override;

public:
	MimeNodeType Type() const override
	{
		return MimeNodeType::Ident;
	}
	bool GetSubPair(MimeNodeEx*& subPair);
};

class MimeNodeEx
{
private:
	std::unique_ptr<MimeCoreBase> m_xfer;

public:
	MimeNodeEx(std::unique_ptr<CToken> tok = {});

//private:
	MimeNodeEx(MimeCoreBase *core);

public:
	~MimeNodeEx();

private:
	template <class Fn>
	struct FunctionHelper;

	template <class RetTy, class Ty>
	struct FunctionHelperBase
	{
		using ret_type = RetTy;
		using object_type = Ty;
	};

	template <class Ret, class Ty, class... Args>
	struct FunctionHelper<Ret(Ty::*)(Args...)> : public FunctionHelperBase<Ret, Ty>
	{ };

	template <class Ret, class Ty, class... Args>
	struct FunctionHelper<Ret(Ty::*)(Args...)const> : public FunctionHelperBase<Ret, Ty>
	{ };

public:
	template <class MemberFn, class... Args>		//타입이 바뀌면, 오래된 객체는 지워지고, 새로운 객체가 생성됩니다
	auto Extra(MemberFn &&fn, Args&&... args)
		-> typename FunctionHelper<std::decay_t<decltype(fn)>>::ret_type		//modification on 20 June 2022-09:50
	{
		using CheckTy = typename FunctionHelper<std::remove_reference<decltype(fn)>::type>::object_type;
		using Ty = typename std::enable_if<std::is_base_of<MimeCoreBase, CheckTy>::value, CheckTy>::type;

		if (!dynamic_cast<Ty *>(m_xfer.get()))
			m_xfer = std::make_unique<Ty>();

		return (static_cast<Ty *>(m_xfer.get())->*(fn))(std::forward<Args>(args)...);
	}
	
	void Append(std::unique_ptr<CToken> tok);
	void Append(MimeNodeEx &other);

	template <class Ty, class... Args, class = typename std::enable_if<std::is_base_of<MimeCoreBase, Ty>::value, Ty>::type>
	static std::unique_ptr<MimeNodeEx> Create(Args&&... args)
	{
		return std::make_unique<MimeNodeEx>(new Ty(std::forward<Args>(args)...));
	}

	MimeNodeType Type() const
	{
		return m_xfer->Type();
	}

	std::string Error() const
	{
		return m_xfer->GetError();
	}

	int MatchWithList(const std::list<std::string> &cmp)
	{
		return m_xfer->MatchList(cmp);
	}

	int StartLine() const
	{
		return m_xfer->GetLineFromFirstToken();
	}

	std::string ToString();
};

#endif

