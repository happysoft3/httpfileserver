
#ifndef TOKEN_POSITION_H__
#define TOKEN_POSITION_H__

class TokenPosition
{
private:
	int m_line;
	int m_column;
	size_t m_offset;

public:
	TokenPosition();
	TokenPosition(int line, int column, size_t offset);
	~TokenPosition();

public:
	void SetParamsLater(int line, int column, size_t offset);
	int GetLine() const
	{
		return m_line;
	}
	int GetColumn() const
	{
		return m_column;
	}
	size_t GetOffset() const
	{
		return m_offset;
	}
};

#endif
