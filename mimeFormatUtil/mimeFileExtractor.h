
#ifndef MIME_FILE_EXTRACTOR_H__
#define MIME_FILE_EXTRACTOR_H__

#include "fileReader.h"
#include <map>

class AbstractMimeContent;
class MimeContentFile;
class AbstractMimeAttachPrinter;

class MimeFileExtractor : public FileReader
{
public:
	struct MimeFileInfo;

private:
	using content_file_list_ty = std::list<std::unique_ptr<MimeContentFile>>;
	using content_file_list_iter = content_file_list_ty::iterator;
	content_file_list_ty m_files;

	using content_file_map_ty = std::map<std::string, content_file_list_iter>;
	using content_file_map_iter = content_file_map_ty::const_iterator;
	content_file_map_ty m_fileContentMap;
	std::shared_ptr<SourceFile> m_srcStream;
	std::list<std::unique_ptr<MimeContentFile>> m_abandonded;

public:
	MimeFileExtractor();
	~MimeFileExtractor() override;

private:
	void accumulateTrash(std::unique_ptr<SourceFile> discard);
	void joinAllTrashes();
	int readc() override;
	void verbose(MimeContentFile *contentFile);
	void buildImpl();
	int buildApartImpl();

public:
	void PutSourceContext(FileReader &context);

public:
	void Build();
	void PickOne(const std::string &key, std::unique_ptr<AbstractMimeAttachPrinter> printer);
	int BuildOne();
	std::unique_ptr<MimeFileInfo> GetMimeFileInfo(const std::string &key);

private:
	std::unique_ptr<MimeContentFile> discardContentFileImpl(content_file_map_iter iter);
	std::unique_ptr<MimeContentFile> discardContentFileMapFront();
	std::unique_ptr<MimeContentFile> discardContentFileWithKey(const std::string &key);
	void putContentFile(std::unique_ptr<MimeContentFile> file);

public:
	void Include(AbstractMimeContent &content);	//여기에서, MimeContentFile을 생성합니다//
	//나중에, 생성된 MimeContentFile을, StreamPush 메서드를 사용하여, 처리합니다//
};

struct MimeFileExtractor::MimeFileInfo
{
	size_t fileSize;
	std::string contentType;
};

#endif

