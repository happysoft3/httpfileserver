
#include "mimeNodeEx.h"
#include "ctoken.h"

using token_list_const_iter = MimeCoreBase::token_list_ty::const_iterator;

MimeCoreBase::MimeCoreBase(std::unique_ptr<CToken> tok)
{
	if (tok)
		m_tokens.push_back(std::move(tok));
}

MimeCoreBase::~MimeCoreBase()
{ }

void MimeCoreBase::releaseString(std::string &dest, token_list_const_iter iter, size_t initSize)
{
	if (iter == m_tokens.cend())
	{
		if (initSize != 0)
			dest.resize(initSize);
		return;
	}
	auto &tok = **iter;
	std::string val = tok.Value();

	releaseString(dest, ++iter, initSize + val.length());
	std::copy(val.cbegin(), val.cend(), dest.begin() + initSize);
}

std::string MimeCoreBase::ToStr()
{
	std::string output;

	releaseString(output, m_tokens.cbegin());
	return output;
}

void MimeCoreBase::Append(std::unique_ptr<CToken> tok)
{
	m_tokens.push_back(std::move(tok));
}

void MimeCoreBase::Append(MimeCoreBase &other)
{
	for (auto &&tok : other.m_tokens)
		m_tokens.push_back(std::move(tok));

	other.m_tokens.clear();
}

int MimeCoreBase::GetLineFromFirstToken() const
{
	if (m_tokens.empty())
		return 0;

	auto &&tok = m_tokens.front();

	return tok->Line();
}

int MimeCoreBase::stringMatch(const std::list<std::string> &src, std::function<bool(const std::string &, const std::string &)> &&cond)
{
	std::string stream = ToStr();
	int index = 0;

	for (const auto &s : src)
	{
		if (std::forward<std::remove_reference<decltype(cond)>::type>(cond)(s, stream))
			return index;

		++index;
	}
	return -1;
}

std::string MimeCoreBase::GetError() const
{
	if (m_tokens.empty())
		return "(empty)";

	return m_tokens.front()->Error();
}

int MimeCoreBase::MatchList(const std::list<std::string> &srcList)
{
	return stringMatch(srcList, [](const std::string &first, const std::string &second) { return first == second; });
}

using MimePair = MimeCoreBase::MimePair;
MimePair::MimePair()
	: MimeCoreBase()
{ }

MimePair::MimePair(std::unique_ptr<MimeNodeEx> key, std::unique_ptr<MimeNodeEx> value)
	: MimeCoreBase()
{
	m_key.swap(key);
	m_value.swap(value);
}

MimePair::~MimePair()
{ }

std::string MimePair::Key()
{
	return m_key->Extra(&MimeCoreBase::ToStr);
}

std::string MimePair::Value()
{
	return m_value->Extra(&MimeCoreBase::ToStr);
}

int MimePair::GetLineFromFirstToken() const
{
	if (!m_key)
		return 0;

	return m_key->Extra(&MimeCoreBase::GetLineFromFirstToken);
}

using MimeIdent = MimeCoreBase::MimeIdent;
MimeIdent::MimeIdent()
	: MimeCoreBase::MimePair()
{ }

MimeIdent::MimeIdent(std::unique_ptr<MimeNodeEx> key, std::unique_ptr<MimeNodeEx> value, std::unique_ptr<MimeNodeEx> subexpr)
	: MimeCoreBase::MimePair(std::move(key), std::move(value))
{
	m_subexpr.swap(subexpr);
}

MimeIdent::~MimeIdent()
{ }

bool MimeIdent::GetSubPair(MimeNodeEx*& subPair)
{
	if (!m_subexpr)
		return false;

	subPair = m_subexpr.get();
	return true;
}

/////

MimeNodeEx::MimeNodeEx(std::unique_ptr<CToken> tok)
	: m_xfer(new MimeCoreBase)
{
	if (tok)
		m_xfer->Append(std::move(tok));
}

MimeNodeEx::MimeNodeEx(MimeCoreBase *core)
	: m_xfer(core)
{ }

MimeNodeEx::~MimeNodeEx()
{ }

void MimeNodeEx::Append(std::unique_ptr<CToken> tok)
{
	m_xfer->Append(std::move(tok));
}

void MimeNodeEx::Append(MimeNodeEx &other)
{
	m_xfer->Append(*other.m_xfer);
}

std::string MimeNodeEx::ToString()
{
	return m_xfer->ToStr();
}

