
#include "tokenPosition.h"

TokenPosition::TokenPosition()
{
	m_line = 0;
	m_column = 0;
	m_offset = 0;
}

TokenPosition::TokenPosition(int line, int column, size_t offset)
{
	SetParamsLater(line, column, offset);
}

TokenPosition::~TokenPosition()
{ }

void TokenPosition::SetParamsLater(int line, int column, size_t offset)
{
	m_line = line;
	m_column = column;
	m_offset = offset;
}

