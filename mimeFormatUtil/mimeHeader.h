
#ifndef MIME_HEADER_H__
#define MIME_HEADER_H__

#include <map>
#include <memory>
#include <string>
#include <functional>
#include <list>

class MimeNodeEx;
class CToken;

class MimeHeader
{
	struct HeaderElem;
	struct MimeBoundary;

public:
	enum class ValueType
	{
		MIME_VALUE,
		SUBKEY,
		SUBVALUE
	};

	using header_list_ty = std::list<std::unique_ptr<HeaderElem>>;
	using header_list_iterator_ty = header_list_ty::iterator;
	using header_map_ty = std::map<std::string, header_list_iterator_ty>;
	using header_map_const_iterator = header_map_ty::const_iterator;

private:
	header_map_ty m_headerMap;
	std::list<std::unique_ptr<HeaderElem>> m_headerList;
	std::string m_latestKey;

	using special_key_fn_list_ty = std::list<std::function<void(const std::string&)>>;
	using special_key_fn_list_iterator = special_key_fn_list_ty::iterator;
	special_key_fn_list_ty m_specialKeyFnList;
	std::map<std::string, special_key_fn_list_iterator> m_specialKeyFnIterMap;


	//////Boundary////
	std::shared_ptr<MimeBoundary> m_boundary;

	struct BoundaryPos
	{
		size_t offset;
		int line;
	};
	std::list<BoundaryPos> m_boundaryOffsetList;

public:
	MimeHeader();
	~MimeHeader();

private:
	void pushSpecialKeyFunction(const std::string &key, std::function<void(const std::string&)> &&fn);
	void popSpecialKeyFunctionAfterInvoke(const std::string &key);
	HeaderElem *makeHeaderCore(const std::string &value);
	bool retrieveMap(const std::string &key, std::function<void(HeaderElem &)> fn) const;

public:
	int Length() const
	{
		return m_headerMap.size();
	}
	int StartLine() const;
	int EndLine() const;
	size_t EntryOffset() const;
	size_t EndOffset() const;
	void PushBoundaryOffset(CToken &tok);
	bool MatchWithLatestKey(const std::string &key) const;
	bool HasKey(const std::string &key) const;
	void Put(std::unique_ptr<MimeNodeEx> node);
	std::string Value(const std::string &key, ValueType ty = ValueType::MIME_VALUE) const;
	void SetValue(const std::string &key, const std::string &value, ValueType ty = ValueType::MIME_VALUE);
	void ThrowNodeException(const std::string &key, const std::string &exceptName);

private:
	MimeBoundary *createBoundary(const std::string &uniqKey);
	void readContentType(const std::string &key);
	void readContentDisposition(const std::string &key);

public:
	std::string GetContentType() const;
	std::string GetContentMainType() const;
	std::string GetContentSubType() const;
	bool HasBoundaryKey() const;
	bool CheckBoundaryKey(const std::string &boundaryKey);
	bool EraseKey(const std::string &key);

	header_map_const_iterator cbegin() const
	{
		return m_headerMap.cbegin();
	}
	header_map_const_iterator cend() const
	{
		return m_headerMap.cend();
	}
	header_map_const_iterator begin() const
	{
		return m_headerMap.begin();
	}
	header_map_const_iterator end() const
	{
		return m_headerMap.end();
	}

public:
	static constexpr char mime_header_content_disposition[] = "Content-Disposition";
	static constexpr char mime_header_transfer_encoding[] = "Content-Transfer-Encoding";
	static constexpr char mime_header_content_type[] = "Content-Type";
};

#endif

