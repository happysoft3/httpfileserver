
#include "mimeFileExtractor.h"
#include "mimeContentFile.h"
#include "mimeAttachPrinter.h"
#include "mimeContent.h"

MimeFileExtractor::MimeFileExtractor()
	: FileReader()
{
	m_srcStream = nullptr;
}

MimeFileExtractor::~MimeFileExtractor()
{ }

//여기에서, 추출 작업이 완료된, 스트림을 버리지 않고, 다시 수집합니다//
void MimeFileExtractor::accumulateTrash(std::unique_ptr<SourceFile> discard)
{
	if (!discard)
		return;

	SourceFile *pDiscard = discard.release();
	std::unique_ptr<MimeContentFile> reuse(dynamic_cast<MimeContentFile *>(pDiscard));

	if (reuse)
		m_abandonded.push_back(std::move(reuse));
}

//여기에서, 수집된 폐기 스트림을, 처리 대기열에, 다시 합류되도록 합니다//
void MimeFileExtractor::joinAllTrashes()
{
	while (m_abandonded.size())
	{
		putContentFile(std::move(m_abandonded.front()));
		m_abandonded.pop_front();
	}
}

int MimeFileExtractor::readc()
{
	for (;;)
	{
		int c = get();

		if (c == EOF)		//파일의 내용이 끝났습니다, 다음 파일을 로드합니다//
		{
			if (Files() == 1)		//마지막 요소는, nullptr safety 목적으로, 지우지 않음//
				return c;

			accumulateTrash(streamDiscard());	//최상단 스트림이, 제거됩니다//
			if (!currentFile()->Open({}))	//!open 실패 시, 예외를 처리해야 합니다//
				return 0;

			continue;
		}
		return c;
	}
}

void MimeFileExtractor::verbose(MimeContentFile *contentFile)
{
	if (!contentFile)
		return;

	auto error = contentFile->GetErrorDetail();

	//std::cout << error->Explanation() << std::endl;
	//TODO. 여기에서, 오류정보를 얻어야 합니다//
}

void MimeFileExtractor::buildImpl()
{
	int state = 0;
	
	currentFile()->Open({});
	for (;;)
	{
		state = readc();

		if (state == EOF)
			break;			//정상: 처리완료입니다//
		if (!state)		//오류가 반환된 경우입니다//
		{
			auto file = streamDiscard();

			verbose(dynamic_cast<MimeContentFile *>(file.get()));
			break;
		}
	}
	joinAllTrashes();
}

int MimeFileExtractor::buildApartImpl()
{
	int state = readc();

	if (state == EOF)
	{
		streamDiscard();
	}
	else if (!state)
	{
		auto file = streamDiscard();

		verbose(dynamic_cast<MimeContentFile *>(file.get()));
	}
	return state;
}

void MimeFileExtractor::PutSourceContext(FileReader &context)
{
	if (currentFile())	//기존에 이미, 스트림이 차있으면 안됩니다!//
		return;

	if (fetchContext(context))
		m_srcStream = streamDiscard();		//이것은, 마인포맷//
}

void MimeFileExtractor::Build()
{
	if (m_files.empty() || (!m_srcStream))
		return;

	for (;;)
	{
		std::unique_ptr<MimeContentFile> pop = std::move(discardContentFileMapFront());

		if (!pop)
			break;
		streamPush(std::move(pop));
	}
	buildImpl();
}

int MimeFileExtractor::BuildOne()
{
	return buildApartImpl();
}

std::unique_ptr<MimeFileExtractor::MimeFileInfo> MimeFileExtractor::GetMimeFileInfo(const std::string &key)
{
	auto fileIterator = m_fileContentMap.find(key);

	if (fileIterator == m_fileContentMap.cend())
		return {};

	auto info = std::make_unique<MimeFileInfo>();
	MimeContentFile &file = **fileIterator->second;

	info->fileSize = file.GetFileSize();
	info->contentType = file.GetContentType();
	return info;
}

void MimeFileExtractor::PickOne(const std::string &key, std::unique_ptr<AbstractMimeAttachPrinter> printer)
{
	if (!m_srcStream)
		return;

	std::unique_ptr<MimeContentFile> contentFile = discardContentFileWithKey(key);

	if (!contentFile)	//찾기 실패//
		return;
	if (printer)
		contentFile->SetOutputPrinter(std::move(printer));

	streamPush(std::move(contentFile));
	currentFile()->Open({});
	return;
}

std::unique_ptr<MimeContentFile> MimeFileExtractor::discardContentFileImpl(MimeFileExtractor::content_file_map_iter iter)
{
	std::unique_ptr<MimeContentFile> discard;

	discard.swap(*iter->second);
	m_files.erase(iter->second);
	m_fileContentMap.erase(iter);
	return discard;
}

std::unique_ptr<MimeContentFile> MimeFileExtractor::discardContentFileMapFront()
{
	if (m_fileContentMap.empty())
		return {};

	return discardContentFileImpl(m_fileContentMap.begin());
}

std::unique_ptr<MimeContentFile> MimeFileExtractor::discardContentFileWithKey(const std::string &key)
{
	auto fileIterator = m_fileContentMap.find(key);

	if (fileIterator == m_fileContentMap.cend())
		return {};

	return discardContentFileImpl(fileIterator);
}

void MimeFileExtractor::putContentFile(std::unique_ptr<MimeContentFile> file)
{
	std::string fileKey = file->FileKey();

	if (fileKey.empty())
		return;				//ERROR

	m_files.push_back(std::move(file));
	m_fileContentMap.emplace(fileKey, std::prev(m_files.end()));
}

//mime content 을 받고, 그것을 content file 형식으로 저장합니다//
//그러나 그것은, 당장 처리되지 않습니다//
void MimeFileExtractor::Include(AbstractMimeContent &content)
{
	auto contentFile = std::make_unique<MimeContentFile>();

	contentFile->SetInputContext(m_srcStream);
	contentFile->Bind(content);
	putContentFile(std::move(contentFile));
}

