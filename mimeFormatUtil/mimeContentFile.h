
#ifndef MIME_CONTENT_FILE_H__
#define MIME_CONTENT_FILE_H__

#include "sourceFile.h"
#include <functional>

class AbstractMimeContent;
class Base64Helper;
class AbstractMimeAttachPrinter;

class MimeContentFile : public SourceFile
{
public:
	class Error;

private:
	size_t m_startOff;
	size_t m_endOff;
	size_t m_procOff;
	std::string m_extension;
	std::string m_contentType;
	std::string m_fullUrl;
	std::string m_fileKey;
	std::unique_ptr<AbstractMimeAttachPrinter> m_printer;
	std::vector<char> m_readBuffer;
	std::shared_ptr<SourceFile> m_inputContext;
	std::unique_ptr<Error> m_lastError;

	//Decode
	std::function<void()> m_decodeRule;
	std::unique_ptr<Base64Helper> m_decode;

public:
	MimeContentFile();
	~MimeContentFile() override;

private:
	void setContentFileName(AbstractMimeContent &content);

public:
	bool SetInputContext(std::shared_ptr<SourceFile> inputContext);
	void Bind(AbstractMimeContent &content);
	void SetOutputPrinter(std::unique_ptr<AbstractMimeAttachPrinter> printer);

private:
	size_t goToOffset(const size_t &off);

public:
	std::unique_ptr<Error> GetErrorDetail();
	size_t GetFileSize() const;
	std::string GetExtension() const;
	std::string GetContentType() const
	{
		return m_contentType;
	}

private:
	bool Open(const std::string &unused = {}) override;
	size_t writeImpl();
	size_t computeReadBytes();
	int readImpl();
	int putError(Error &&err);
	int Read() override;
	size_t SeekPointer() const override;
	void decodeRuleBase64();

	size_t ReadAsVector(std::vector<char> &dest, size_t readBytes) override
	{
		return 0;
	}

public:
	std::string FileKey() const
	{
		return m_fileKey;
	}
};

class MimeContentFile::Error
{
public:
	enum class Type
	{
		NONE,
		ENDLINE_UNMATCHED,
		WRITE_ERROR,
		NO_ENTRY,
	};

private:
	std::string m_errorMessage;
	Type m_errorId;

public:
	Error(Type errId, const std::string &error)
	{
		m_errorId = errId;
		m_errorMessage = error;
	}
	Error(Type errId = Type::NONE)
	{
		m_errorId = errId;
	}
	~Error()
	{ }

	Type ID() const
	{
		return m_errorId;
	}
	void SetExplanationLater(const std::string &error)
	{
		m_errorMessage = error;
	}
	std::string Explanation() const
	{
		return m_errorMessage;
	}
};

#endif

