
#ifndef MIME_ATTACH_PRINTER_H__
#define MIME_ATTACH_PRINTER_H__

#include <string>
#include <memory>

class AbstractMimeAttachPrinter
{
public:
	AbstractMimeAttachPrinter();
	virtual ~AbstractMimeAttachPrinter();

	virtual bool Create(const std::string &url) = 0;
	virtual size_t Seekpointer() const = 0;

private:
	virtual void writeImpl(const char *src, size_t length) = 0;

public:
	template <class Container>
	size_t Write(const Container &src)
	{
		size_t prev = Seekpointer();

		writeImpl(src.data(), src.size());
		return Seekpointer() - prev;
	}
};

class MimeAttachPrinter : public AbstractMimeAttachPrinter
{
	struct Impl;
private:
	std::unique_ptr<Impl> m_impl;

public:
	MimeAttachPrinter();
	~MimeAttachPrinter() override;

private:
	bool Create(const std::string &url) override;
	size_t Seekpointer() const override;
	void writeImpl(const char *src, size_t length) override;
};

#endif

