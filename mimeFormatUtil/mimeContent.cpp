
#include "mimeContent.h"
#include "mimeHeader.h"
#include "common/utils/stringhelper.h"
#include "common/include/incFilesystem.h"

using namespace _StringHelper;

MimeContent::MimeContent(AbstractMimeContent *parent)
	: AbstractMimeContent(parent)
{ }

MimeContent::~MimeContent()
{ }

int MimeContent::getParentNodeId() const
{
	AbstractMimeContent *parent = getParent();

	if (!parent)
		return -1;

	return parent->GetNodeId();
}

std::string MimeContent::GetNodeIdString() const
{
	int parentId = getParentNodeId();

	if (parentId == -1)
		return {};

	return stringFormat("%d-%d", parentId, GetNodeId());
}

AbstractMimeContent *MimeContent::findContent(int nodeId, int itemId)
{
	int parentId = getParentNodeId();

	if (parentId == -1)
		return nullptr;

	if (parentId == nodeId && itemId == GetNodeId())
		return this;

	return nullptr;
}

void MimeContent::onPutHeader(MimeHeader &header)
{
	std::string subKey = header.Value(MimeHeader::mime_header_content_disposition, MimeHeader::ValueType::SUBKEY);

	if (subKey == "filename")
	{
		std::string url = header.Value(MimeHeader::mime_header_content_disposition, MimeHeader::ValueType::SUBVALUE);

		if (url.empty())
			return;

		header.SetValue(MimeHeader::mime_header_content_disposition, NAMESPACE_FILESYSTEM::u8path(url).string(), MimeHeader::ValueType::SUBVALUE);
	}
}

