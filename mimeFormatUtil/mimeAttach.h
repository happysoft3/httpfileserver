
#ifndef MIME_ATTACH_H__
#define MIME_ATTACH_H__

#include <memory>
#include <string>
#include <vector>
#include <functional>

class AbstractMimeContent;
class SourceFile;
class Base64Helper;

class MimeAttach
{
private:
	size_t m_startOff;
	size_t m_endOff;
	size_t m_procOff;
	int m_lineCount;
	std::string m_contentTy;
	std::string m_encodeTy;
	std::string m_disposTy;
	std::string m_disposSub;	//�ӽ���//
	std::unique_ptr<SourceFile> m_inputCtx;
	std::vector<char> m_readBuffer;
	std::unique_ptr<Base64Helper> m_decode;
	
public:
	using write_buffer_fn_ty = std::function<void(const std::vector<char>&)>;
private:
	std::unique_ptr<write_buffer_fn_ty> m_outstreamFn;
	using outstream_dump_fn_ty = std::function<void()>;
	outstream_dump_fn_ty m_dumpFn;

public:
	MimeAttach();
	~MimeAttach();

private:
	size_t computeReadBytes();
	int readInput();
	size_t computeBase64Length();

public:
	std::string GetContentType() const
	{
		return m_contentTy;
	}
	std::string GetDispositionType() const
	{
		return m_disposTy;
	}
	std::string GetDispositionSubValue() const
	{
		return m_disposSub;
	}
	void RegistOutstreamMethod(write_buffer_fn_ty &&fn);
	size_t GetContentLength();
	void SetInputUrl(const std::string &url);
	void PutMimeContent(AbstractMimeContent &content);

private:
	void dumpOutstream();
	void dumpBase64();

public:
	int DoExtract();
};

#endif

