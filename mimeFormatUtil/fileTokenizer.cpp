
#include "fileTokenizer.h"
#include "ctoken.h"
#include "tokenPosition.h"
#include <iostream>

FileTokenizer::FileTokenizer()
	: FileReader()
{
	m_markPos = std::make_unique<TokenPosition>();
}

FileTokenizer::~FileTokenizer()
{ }

void FileTokenizer::marking()
{
	*m_markPos = *getPos();	//shallow copy
}

int FileTokenizer::peek()
{
	int c = readc();

	unreadc(c);
	return c;
}

bool FileTokenizer::next(int expect)
{
	int c = readc();

	if (c == expect)
		return true;
	unreadc(c);
	return false;
}

bool FileTokenizer::isWhitespace(int c) const
{
	switch (c)
	{
	case ' ': case '\t': case '\f': case '\v': return true;
	default: return false;
	}
}

bool FileTokenizer::doSkipSpace()
{
	int c = readc();

	if (c == EOF)
		return false;
	if (isWhitespace(c))
		return true;

	unreadc(c);
	return false;
}

bool FileTokenizer::skipSpace()
{
	if (!doSkipSpace())
		return false;

	while (doSkipSpace());
	return true;
}

std::string FileTokenizer::readHexaNumber(int zero, int symb)
{
	std::string buff(sizeof(char), zero);
	std::string nbuff;

	for (;;)
	{
		int c = readc();
		switch (c)
		{
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
		case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
			nbuff.push_back(c);
			break;

		default:
			unreadc(c);
			if (nbuff.empty())
				unreadc(symb);
			else
				buff.push_back(symb);
			return nbuff.empty() ? buff : (buff + nbuff);
		}
	}
}

std::string FileTokenizer::readBinaryNumber(int zero, int symb)
{
	std::string buff(sizeof(char), zero);
	std::string nbuff;

	for (;;)
	{
		int c = readc();

		switch (c)
		{
		case '0': case '1':
			nbuff.push_back(c);
			break;

		default:
			unreadc(c);		// push 'b' 
			if (nbuff.empty())
				unreadc(symb);
			else
				buff.push_back(symb);
			return nbuff.empty() ? buff : (buff + nbuff);
		}
	}
}

std::unique_ptr<CToken> FileTokenizer::readNumber(char c)
{
	switch (peek())
	{
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9': break;
	case 'x': case 'X': return makeToken<CTokenNumber>(readHexaNumber(c, readc()));
	case 'b': case 'B': return makeToken<CTokenNumber>(readBinaryNumber(c, readc()));
	}

	std::string buffer(sizeof(char), c);
	char last = c;

	for (;;)
	{
		int cs = readc();
		bool flonum = strchr("eEpP", last) && strchr("+-", cs);

		if (!isdigit(cs) && /*!isalpha(cs) &&*/ cs != '.' && !flonum)
		{
			unreadc(cs);
			if (buffer.back() == '.')
			{
				unreadc(buffer.back());
				buffer.pop_back();
			}
			return makeToken<CTokenNumber>(buffer);
		}
		buffer.push_back(cs);
		last = cs;
	}
}

std::unique_ptr<CToken> FileTokenizer::readIdent(char c)
{
	std::string buffer(sizeof(char), c);

	for (;;)
	{
		c = readc();
		if (isalnum(c) || (c & 0x80) || (c == '_') || (c == '$') || (c == '-'))
		{
			buffer.push_back(c);
			continue;
		}
		unreadc(c);
		return makeToken<CTokenIdent>(std::move(buffer));
	}
}

int FileTokenizer::readEscapedChar()
{
	int c = readc();

	switch (c)
	{
	case '\'': case '"': case '?': case '\\': return c;
	case 'a': return '\a';
	case 'b': return '\b';
	case 'f': return '\f';
	case 'n': return '\n';
	case 'r': return '\r';
	case 't': return '\t';
	case 'v': return '\v';
	}
	return c;
}

std::unique_ptr<CToken> FileTokenizer::readString()
{
	std::string buffer;

	for (;;)
	{
		int c = readc();
		if (c == EOF)
			return makeToken<CTokenInvalid>("unterminated string", c);

		if (c == '"')
			break;

		if (c != '\\')
		{
			buffer.push_back(c);
			continue;
		}

		c = readEscapedChar();
		buffer.push_back(c);
	}
	return makeToken<CTokenString>(buffer);
}

bool FileTokenizer::calcHexChar(int &res)
{
	static constexpr int decimal = 10;
	int c = readc();

	switch (c)
	{
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
		res = c - '0';
		return true;
	case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
		res = c - 'a' + decimal;
		return true;
	case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		res = c - 'A' + decimal;
		return true;
	default:
		unreadc(c);
		return false;
	}
}

bool FileTokenizer::calcHexWord(uint8_t &output)
{
	int low = 0;
	int c = peek();

	if (calcHexChar(low))
	{
		int high = 0;

		if (calcHexChar(high))
		{
			static constexpr int hexa = 16;

			output = high + (low * hexa);
			return true;
		}
		unreadc(c);
	}
	return false;
}

std::unique_ptr<CToken> FileTokenizer::readPercentString()
{
	std::string buffer;
	uint8_t hexa = 0;

	for (;;)
	{
		if (!calcHexWord(hexa))
		{
			if (buffer.length())
				return makeToken<CTokenString>(std::move(buffer));

			return makeToken<CTokenInvalid>("percent error", peek());		//일단, 오류로 처리합니다
		}

		buffer.push_back(hexa);

		int c = readc();

		if (c != '%')
		{
			unreadc(c);
			return makeToken<CTokenString>(std::move(buffer));
		}
	}
}

//std::unique_ptr<CToken> FileTokenizer::readAnyLine(int c)
//{
//	std::string buffer(sizeof(char), static_cast<char>(c));
//
//	for (;;)
//	{
//		c = readc();
//
//		switch (c)
//		{
//		case EOF: case '\n':
//			unreadc(c);
//			return makeToken<CTokenAnyLine>(buffer);
//
//		default:
//			buffer.push_back(c);
//		}
//	}
//}

std::unique_ptr<CToken> FileTokenizer::readUntilLine()
{
	int c;
	std::string buffer;

	for (;;)
	{
		c = readc();
		switch (c)
		{
		case EOF: case '\n':
			unreadc(c);
			return makeToken<CTokenAnyLine>(buffer);

		default:
			buffer.push_back(c);
		}
	}
}


//////토큰 생성 룰입니다////
std::unique_ptr<CToken> FileTokenizer::doReadToken()
{
	if (skipSpace())
		return makeToken<CTokenSpace>();

	marking();

	int c = readc();

	switch (c)
	{
	case EOF: return makeToken<CTokenEof>();
	case '\n': return makeToken<CTokenNewline>();
	case '_': return readIdent(c);
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9': return readNumber(c);
	case '"': return readString();
	case ':': case ';':
	case '-': if (next('-')) return makeToken<CTokenKeyword>(CTokenKeyword::ExtendType::TY_DOUBLE_MINUS);
	case '?': case '=': case '/': case ',': case '&': case '|':
		return makeToken<CTokenKeyword>(c);
	case '%':
		if (next('%')) return makeToken<CTokenKeyword>('%');
		else return readPercentString();
	case '.':
		if (isdigit(peek())) return readNumber(c);
		return makeToken<CTokenKeyword>(c);
	default:
		if ((c >= 'a' && c <= 'z') || (c >= 'A'&&c <= 'Z'))
			return readIdent(c);
		return makeToken<CTokenInvalid>("unknown char", c);
	}
}

std::unique_ptr<CToken> FileTokenizer::doReadLine()
{
	for (;;)
	{
		marking();
		int c = readc();

		switch (c)
		{
		case EOF: return makeToken<CTokenEof>();
		case '\n': return makeToken<CTokenNewline>();
		case '-': if (next('-')) return readUntilLine();
		default: break;
		}
	}
}

void FileTokenizer::createdToken(std::unique_ptr<CToken> &&tok)
{
	tok->SetPos(*m_markPos);
}

bool FileTokenizer::Open(const std::string &url)
{
	if (!AddFile(url))
		return false;

	return true;
}

void FileTokenizer::Process()
{
	for (;;)
	{
		auto tok = doReadToken();

		if (tok->IsType(CTokenType::Eof))
			break;

		std::cout << tok->Print();
	}
}


