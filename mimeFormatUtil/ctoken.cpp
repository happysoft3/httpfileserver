
#include "cToken.h"
#include "tokenPosition.h"
#include "common/utils/stringhelper.h"
#include <iostream>

using namespace _StringHelper;

CToken::CToken(CTokenType ty, int tokenChar)
{
	m_ty = ty;
	m_pos = std::make_unique<TokenPosition>();
	m_tokenChar = tokenChar;
}

CToken::CToken(const CToken &other)
{
	m_ty = other.m_ty;
	m_tokenChar = other.m_tokenChar;
	SetPos(*other.m_pos);
}

CToken::~CToken()
{ }

std::string CToken::tokenPosition(int l, int c) const
{
	return stringFormat("line: %d, column: %d", l, c);
}

std::string CToken::strValue() const
{
	return {};
}

std::string CToken::errorValue() const
{
	return strValue();
}

std::string CToken::typeToStr() const
{
	switch (m_ty)
	{
	case CTokenType::Eof: return "eof";
	case CTokenType::Ident: return "ident";
	case CTokenType::Invalid: return "invalid";
	case CTokenType::Keyword: return "keyword";
	case CTokenType::Newline: return "newline";
	case CTokenType::Number: return "number";
	case CTokenType::Space: return "space";
	case CTokenType::String: return "string";
	default: return "unknown";
	}
}

void CToken::SetPos(const TokenPosition &pos)
{
	*m_pos = pos;
}

std::string CToken::Error() const
{
	return stringFormat("[error- %s, (ty:%s, val:%s)]", tokenPosition(m_pos->GetLine(), m_pos->GetColumn()), typeToStr(), errorValue());
}

std::string CToken::Print() const
{
	if (errorTriggered())
		return stringFormat("[error- %s, (ty:%s, val:%s)]\n", tokenPosition(m_pos->GetLine(), m_pos->GetColumn()), typeToStr(), strValue());

	return stringFormat(" (ty:%s, val:%s) ", typeToStr(), strValue());
}

int CToken::Line() const
{
	return m_pos->GetLine();
}

size_t CToken::Offset() const
{
	return m_pos->GetOffset();
}

////Ident////

CTokenIdent::CTokenIdent(CTokenType ty, const std::string &ident)
	: CToken(ty)
{
	m_svalue = ident;
}

CTokenIdent::CTokenIdent(const std::string &ident)
	: CToken(CTokenType::Ident)
{
	m_svalue = ident;
}

CTokenIdent::~CTokenIdent()
{ }

std::string CTokenIdent::strValue() const
{
	return m_svalue;
}

CTokenIdent *CTokenIdent::Clone() const
{
	return new CTokenIdent(*this);
}

CTokenString::CTokenString(CTokenType ty, const std::string &s)
	: CTokenIdent(ty, s)
{ }

CTokenString::CTokenString(const std::string &s)
	: CTokenIdent(CTokenType::String, s)
{ }

CTokenString::~CTokenString()
{ }

std::string CTokenString::strValue() const
{
	//return stringFormat("\"%s\"", m_svalue);
	return m_svalue;
}

////Number////

CTokenNumber::CTokenNumber(const std::string &numberString)
	: CTokenString(CTokenType::Number, numberString)
{ }

CTokenNumber::~CTokenNumber()
{ }

CTokenKeyword::CTokenKeyword(int ch, CTokenType ty)
	: CToken(ty, ch)
{ }

CTokenKeyword::CTokenKeyword(CTokenType ty)
	: CToken(ty)
{ }

CTokenKeyword::~CTokenKeyword()
{ }

std::string CTokenKeyword::strValue() const
{
	int c = tokenChar();

	if (c == ExtendType::TY_DOUBLE_MINUS)
		return "--";

	return stringFormat("%c", static_cast<char>(tokenChar()));
}

CTokenKeyword *CTokenKeyword::Clone() const
{
	return new CTokenKeyword(*this);
}

CTokenSpace::CTokenSpace()
	: CTokenKeyword(CTokenType::Space)
{ }

CTokenSpace::~CTokenSpace()
{ }

std::string CTokenSpace::strValue() const
{
	return "(space)";
}

CTokenNewline::CTokenNewline()
	: CTokenKeyword(CTokenType::Newline)
{ }

CTokenNewline::~CTokenNewline()
{ }

std::string CTokenNewline::strValue() const
{
	return "(newline)";
}

CTokenEof::CTokenEof()
	: CTokenKeyword(CTokenType::Eof)
{ }

CTokenEof::~CTokenEof()
{ }

std::string CTokenEof::strValue() const
{
	return "(eof)";
}

CTokenInvalid::CTokenInvalid(const std::string &err, int c)
	: CTokenKeyword(c, CTokenType::Invalid)
{
	m_err = err;
}

CTokenInvalid::~CTokenInvalid()
{ }

std::string CTokenInvalid::strValue() const
{
	return std::string(sizeof(char), static_cast<char>(tokenChar()));
}

CTokenInvalid *CTokenInvalid::Clone() const
{
	return new CTokenInvalid(*this);
}

CTokenAnyLine::CTokenAnyLine(const std::string &anyStr)
	: CTokenIdent(CTokenType::Anyline, anyStr)
{ }

CTokenAnyLine::~CTokenAnyLine()
{ }



