
#include "sourceFile.h"
#include <fstream>
#include <algorithm>
#include <array>

SourceFile::SourceFile()
{
	m_line = 1;
	m_column = 1;
}

SourceFile::~SourceFile()
{ }

void SourceFile::IncreasePosition(int c)
{
	switch (c)
	{
	case '\n':
		++m_line;
		m_column = 1;
		break;

	case EOF:
		break;

	default:
		++m_column;
	}
}

void SourceFile::DecreasePosition(int c)
{
	if (c == '\n')
	{
		--m_line;
		m_column = 1;
	}
	else
		--m_column;
}


struct SourceFile::File::Impl
{
	std::fstream m_file;
	std::string m_name;
};

SourceFile::File::File()
	: SourceFile()
{
	m_last = 0;
}

SourceFile::File::~File()
{ }

void SourceFile::File::openImpl(SourceFile::File::Impl *impl)
{
	m_last = 0;
}

bool SourceFile::File::Open(const std::string &name)
{
	std::fstream file(name, std::ios::in | std::ios::binary);
	bool result = file ? true : false;

	if (!result)
	{
		if (m_core)
			m_core.reset();
	}
	else
	{
		file << std::noskipws;
		m_core = std::make_unique<Impl>();
		m_core->m_name = name;
		m_core->m_file = std::move(file);
	}
	openImpl(m_core.get());
	return result;
}

int SourceFile::File::Read()
{
	int c = m_core->m_file.get();

	if (c == EOF)
		c = (m_last == '\n' || m_last == EOF) ? EOF : '\n';
	else if (c == '\r')
	{
		int c2 = m_core->m_file.get();
		if (c2 != '\n')
			m_core->m_file.unget();
		c = '\n';
	}
	m_last = c;
	return c;
}

size_t SourceFile::File::SeekPointer() const
{
	return static_cast<int>(m_core->m_file.tellg());
}

void SourceFile::File::MoveSeekPointer(size_t seekpointer)
{
	if (m_core)
	{
		if (m_core->m_file.eof())
			m_core->m_file.clear();
		m_core->m_file.seekg(static_cast<size_t>(seekpointer), std::ios::beg);
	}
}

std::string SourceFile::File::FileName() const
{
	if (!m_core)
		return {};

	return m_core->m_name;
}

size_t SourceFile::File::ReadAsVector(std::vector<char> &dest, size_t readBytes)
{
	m_core->m_file.read(dest.data(), (readBytes == 0) ? dest.size() : readBytes);

	return static_cast<size_t>(m_core->m_file.gcount());
}

SourceFile::Stream::Stream()
	: SourceFile()
{ }

SourceFile::Stream::~Stream()
{ }

bool SourceFile::Stream::Open(const std::string &stream)
{
	m_stream = stream;
	m_readPos = 0;
	m_last = 0;
	return true;
}

int SourceFile::Stream::peek() const
{
	return (m_readPos < m_stream.size()) ? m_stream[m_readPos] : EOF;
}

int SourceFile::Stream::Read()
{
	int c;

	if (peek() == EOF)
		c = (m_last == '\n' || m_last == EOF) ? EOF : '\n';
	else if (peek() == '\r')
	{
		++m_readPos;
		if (peek() == '\n')
			++m_readPos;
		c = '\n';
	}
	else
	{
		c = peek();
		++m_readPos;
	}
	m_last = c;
	return c;
}

size_t SourceFile::Stream::SeekPointer() const
{
	return m_readPos;
}

void SourceFile::Stream::MoveSeekPointer(size_t seekpointer)
{
	if (seekpointer < m_stream.length())
	{
		m_readPos = static_cast<size_t>(seekpointer);
	}
}

size_t SourceFile::Stream::ReadAsVector(std::vector<char> &dest, size_t readBytes)
{
	if (m_readPos > m_stream.size())
		return 0;

	size_t readableBytes = (m_readPos + readBytes > m_stream.size()) ? (m_stream.size() - m_readPos) : readBytes;
	size_t realBytes = 0;
	std::string::const_iterator iter = m_stream.cbegin() + m_readPos;

	std::transform(iter, iter + readableBytes, dest.begin(), [&realBytes](char c) { return c + static_cast<char>(0 * (++realBytes)); });	//The worst design, i swear.
	return realBytes;
}

SourceFile::AdvancedFile::AdvancedFile(size_t cacheSize)
	: File()
{
	static constexpr size_t implicit_cache_size = 16384;

	m_impl = nullptr;
	m_cache.resize((cacheSize == 0) ? implicit_cache_size : cacheSize);
	calcFilesize();
}

SourceFile::AdvancedFile::~AdvancedFile()
{ }

void SourceFile::AdvancedFile::calcFilesize()
{
	if (!m_impl)
	{
		m_fileSize = 0;
		return;
	}

	m_impl->m_file.seekg(0, std::ios::end);
	m_fileSize = static_cast<size_t>(m_impl->m_file.tellg());
	m_cachePos = 0;
	size_t cacheSize = m_cache.size();
	m_cacheReadPos = (cacheSize > m_fileSize) ? m_fileSize : cacheSize;
	m_impl->m_file.seekg(0, std::ios::beg);
}

void SourceFile::AdvancedFile::openImpl(SourceFile::File::Impl *impl)
{
	m_impl = impl;

	calcFilesize();
}

bool SourceFile::AdvancedFile::cacheStreamBuffer()
{
	if (m_fileSize == m_cachePos)
		return false;

	size_t cacheSize = m_cache.size();

	if (m_fileSize - m_cachePos < cacheSize)
	{
		//마지막 회차,
		cacheSize = m_fileSize - m_cachePos;
		m_cache.resize(cacheSize);
	}

	m_impl->m_file.read(m_cache.data(), cacheSize);
	m_cachePos += static_cast<size_t>(m_impl->m_file.gcount()); //cacheSize;
	m_cacheReadPos = 0;
	return true;
}

int SourceFile::AdvancedFile::readImpl()
{
	if (m_ungetBuffer.size())
	{
		int ungetc = m_ungetBuffer.top();

		m_ungetBuffer.pop();
		return ungetc;
	}

	if (m_cache.size() == m_cacheReadPos)
	{
		if (!cacheStreamBuffer())
			return EOF;
	}
	auto p = m_cache.data();

	return p[m_cacheReadPos++];
	//return m_cache[m_cacheReadPos++];
}

int SourceFile::AdvancedFile::Read()
{
	int c = readImpl();

	if (c == EOF)
		c = (m_lastc == '\n' || m_lastc == EOF) ? EOF : '\n';
	else if (c == '\r')
	{
		int c2 = readImpl();
		if (c2 != '\n')
			m_ungetBuffer.push(c2);
		c = '\n';
	}
	m_lastc = c;
	return c;
}

void SourceFile::AdvancedFile::MoveSeekPointer(size_t seekpointer)
{
	File::MoveSeekPointer(seekpointer);
	m_cachePos = seekpointer;
}
