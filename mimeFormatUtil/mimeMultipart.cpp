
#include "mimeMultipart.h"

MimeMultipart::MimeMultipart(AbstractMimeContent *parent)
	: AbstractMimeContent(parent)
{
	m_childIndex = 1;
}

MimeMultipart::~MimeMultipart()
{ }

void MimeMultipart::onSetSequence(AbstractMimeContent::Numbering &seq)
{
	AbstractMimeContent *parent = getParent();

	SetNodeId(parent ? seq.Get() : 0);
}

void MimeMultipart::onAddedChild(std::unique_ptr<AbstractMimeContent> &&child)
{
	if (!dynamic_cast<MimeMultipart *>(child.get()))
		child->SetNodeId(m_childIndex++);
	m_childVec.push_back(std::forward<std::unique_ptr<AbstractMimeContent>>(child));
}

void MimeMultipart::traversalPreorder(MimeMultipart::mime_traversal_fn_ty &&fn)
{
	//std::forward<mime_traversal_fn_ty>(fn)(this);	//자기자신 포함안함// //일단은, 파일 추출을 먼저 해봅니다/.///

	for (const auto &node : m_childVec)
		traversalPreorderInvoke(std::forward<mime_traversal_fn_ty>(fn), node.get());
}

AbstractMimeContent *MimeMultipart::findItem(int itemId)
{
	for (const auto &node : m_childVec)
	{
		if (node->GetNodeId() == itemId)
			return node.get();
	}
	return nullptr;
}

AbstractMimeContent *MimeMultipart::findContent(int nodeId, int itemId)
{
	AbstractMimeContent *res = nullptr;

	for (const auto &node : m_childVec)
	{
		res = findContentInvoke(nodeId, itemId, *node);

		if (res)
			return res;
	}
	return nullptr;
}
