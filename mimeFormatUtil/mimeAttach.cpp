
#include "mimeAttach.h"
#include "abstractMimeContent.h"
#include "mimeHeader.h"
#include "sourceFile.h"
#include "common/utils/base64helper.h"

static constexpr size_t s_readBufferOneshot = 256;

MimeAttach::MimeAttach()
{
	m_startOff = 0;
	m_endOff = 0;
	m_procOff = 0;
	m_lineCount = 0;
	m_dumpFn = [this]() { this->dumpOutstream(); };
}

MimeAttach::~MimeAttach()
{ }

size_t MimeAttach::computeReadBytes()
{
	size_t elevatation = (m_endOff - m_procOff);

	return (elevatation > s_readBufferOneshot) ? s_readBufferOneshot : elevatation;
}

int MimeAttach::readInput()
{
	size_t reqBytes = computeReadBytes();

	if (reqBytes == 0)
		return EOF;

	if (m_readBuffer.size() != reqBytes)
		m_readBuffer.resize(reqBytes);

	size_t realReadbytes = m_inputCtx->ReadAsVector(m_readBuffer, m_readBuffer.size());

	if (realReadbytes == 0)
		return 0;
	m_procOff += realReadbytes;
	if (realReadbytes != m_readBuffer.size())
		m_readBuffer.resize(realReadbytes);

	return static_cast<int>(realReadbytes);
}

size_t MimeAttach::computeBase64Length()
{
	static constexpr int max_base64_col = 72;
	size_t rewind = m_endOff;
	int c, paddings=0;
	size_t prevSeek = m_inputCtx->SeekPointer();

//#define COMPUTE_TEST_C
#ifdef COMPUTE_TEST_C
	m_inputCtx->MoveSeekPointer(m_startOff);
	int ctxFirst = m_inputCtx->Read();
	m_inputCtx->MoveSeekPointer(m_endOff);
	int ctxLast = m_inputCtx->Read();
#endif

	while (rewind > m_startOff)
	{
		m_inputCtx->MoveSeekPointer(--rewind);
		c = m_inputCtx->Read();

		if (c == '=')
		{
			++paddings;
			continue;
		}
		if (Base64Helper::IsBase64Symbol(c))
			break;
	}
	m_inputCtx->MoveSeekPointer(prevSeek);
	int skipCarriageReturn = m_lineCount * 2;
	int gapOffset = ((m_endOff - m_startOff - skipCarriageReturn) % max_base64_col) + 1;
	int encodedBase64Length = ((m_lineCount-1) * max_base64_col) + gapOffset;

	return ((encodedBase64Length / 4) * 3) - paddings;
}

void MimeAttach::RegistOutstreamMethod(MimeAttach::write_buffer_fn_ty &&fn)
{
	m_outstreamFn = std::make_unique<write_buffer_fn_ty>(std::forward<std::decay_t<decltype(fn)>>(fn));
}

size_t MimeAttach::GetContentLength()
{
	if (m_encodeTy == Base64Helper::s_base64_symbol)
		return computeBase64Length();

	return m_endOff - m_startOff;
}

void MimeAttach::SetInputUrl(const std::string &url)
{
	if (url.empty())
		return;

	std::unique_ptr<SourceFile> input = std::make_unique<SourceFile::File>();

	if (input->Open(url))
	{
		input->MoveSeekPointer(m_startOff);
		m_inputCtx = std::move(input);
		if (m_encodeTy == Base64Helper::s_base64_symbol)
		{
			m_decode = std::make_unique<Base64Helper>();
			m_dumpFn = [this]() { this->dumpBase64(); };
		}
	}
}

void MimeAttach::PutMimeContent(AbstractMimeContent &content)
{
	m_startOff = content->EntryOffset();
	m_endOff = content->EndOffset() - 2; /// 10 10 10 10 �̷��� ��(�� 4��)//  3->2
	m_procOff = m_startOff;
	m_lineCount = (content->EndLine() - 2) - (content->StartLine() + 1) + 1;
	m_contentTy = content->Value("Content-Type");
	m_encodeTy = content->Value("Content-Transfer-Encoding");
	m_disposTy = content->Value("Content-Disposition");
	m_disposSub = content->Value("Content-Disposition", MimeHeader::ValueType::SUBVALUE);
}

void MimeAttach::dumpOutstream()
{
	if (m_outstreamFn)
		(*m_outstreamFn)(m_readBuffer);
}

void MimeAttach::dumpBase64()
{
	m_decode->StreamPush(m_readBuffer);

	std::vector<char> decodedStream;
	m_decode->Decode(decodedStream);
	(*m_outstreamFn)(decodedStream);
}

int MimeAttach::DoExtract()
{
	int readBytes = readInput();

	if (readBytes == EOF || readBytes == 0)
		return readBytes;

	m_dumpFn();
	return readBytes;
}
