
#include "mimeContentFile.h"
#include "abstractMimeContent.h"
#include "mimeHeader.h"
#include "mimeAttachPrinter.h"
#include "common/utils/base64helper.h"
#include "common/utils/stringhelper.h"
#include <fstream>

using namespace _StringHelper;

static constexpr size_t s_read_buffer_init_size = 256;

static std::map<std::string, std::string> s_contentTypeToExtensionMap =
{
#define MIMETYPE(val, key) {key, val},
#include "common/identifier/mimetypes.inc"
#undef MIMETYPE
};

MimeContentFile::MimeContentFile()
	: SourceFile()
{
	m_startOff = 0;
	m_endOff = 0;
	m_readBuffer.resize(s_read_buffer_init_size);
	m_printer = std::make_unique<MimeAttachPrinter>();
}

MimeContentFile::~MimeContentFile()
{ }

void MimeContentFile::setContentFileName(AbstractMimeContent &content)
{
	std::string url = content->Value(MimeHeader::mime_header_content_disposition, MimeHeader::ValueType::SUBVALUE);

	if (url.length())
	{
		m_fullUrl = url;
		return;
	}

	auto extensionIterator = s_contentTypeToExtensionMap.find(content->GetContentType());
	m_extension = extensionIterator != s_contentTypeToExtensionMap.cend() ? extensionIterator->second : "txt";
	static int tempIndex = 0;

	m_fullUrl = stringFormat("test%.3d.%s", ++tempIndex, m_extension);
}

bool MimeContentFile::SetInputContext(std::shared_ptr<SourceFile> inputContext)
{
	m_inputContext = inputContext;
	return Open({});
}

void MimeContentFile::Bind(AbstractMimeContent &content)
{
	//오프셋 가져옴//
	m_startOff = content->EntryOffset();
	m_endOff = content->EndOffset();
	m_procOff = m_startOff;
	///
	if (content->Value(MimeHeader::mime_header_transfer_encoding) == "base64")
	{
		m_decode = std::make_unique<Base64Helper>();
		m_decodeRule = [this]() { return this->decodeRuleBase64(); };
	}

	if ((!m_startOff) || (!m_endOff))
		return;

	m_fileKey = content.GetNodeIdString();
	setContentFileName(content);
	m_contentType = content->Value("Content-Type");
}

void MimeContentFile::SetOutputPrinter(std::unique_ptr<AbstractMimeAttachPrinter> printer)
{
	m_printer = std::move(printer);
}

size_t MimeContentFile::goToOffset(const size_t &off)
{
	m_inputContext->MoveSeekPointer(off);
	return off;
}

std::unique_ptr<MimeContentFile::Error> MimeContentFile::GetErrorDetail()
{
	if (!m_lastError)
		return {};

	return std::move(m_lastError);
}

size_t MimeContentFile::GetFileSize() const
{
	return m_endOff - m_startOff;
}

std::string MimeContentFile::GetExtension() const
{
	return m_extension;
}

bool MimeContentFile::Open(const std::string &/*mimeFormatUrl*/)
{
	if (m_printer->Create(m_fullUrl))
	{
		size_t startPos = goToOffset(m_startOff);

		if (!startPos)				///목표 위치로, 미리 옮겨둡니다///
		{
			putError(Error::Type::NO_ENTRY);

			return false;
		}
		return true;
	}
	return false;
}

size_t MimeContentFile::writeImpl()
{
	if (m_decodeRule)
		m_decodeRule();

	return m_printer->Write(m_readBuffer);
}

size_t MimeContentFile::computeReadBytes()
{
	size_t elevatation = (m_endOff - m_procOff);

	return (elevatation > s_read_buffer_init_size) ? s_read_buffer_init_size : elevatation;
}

int MimeContentFile::readImpl()
{
	size_t reqReadBytes = computeReadBytes();

	if (reqReadBytes == 0)
		return EOF;

	if (m_readBuffer.size() != reqReadBytes)
		m_readBuffer.resize(reqReadBytes);

	size_t realReadBytes = m_inputContext->ReadAsVector(m_readBuffer, m_readBuffer.size());

	if (realReadBytes == 0)
		return 0;	//Error

	m_procOff += realReadBytes;

	if (realReadBytes != m_readBuffer.size())
		m_readBuffer.resize(realReadBytes);

	return static_cast<int>(realReadBytes);
}

int MimeContentFile::putError(Error &&err)
{
	m_lastError = std::unique_ptr<Error>(new Error(std::forward<Error>(err)));
	return 0;
}

int MimeContentFile::Read()		//읽기 및 쓰기를, 동시에//
{
	int c = readImpl();

	if (c == 0) return putError(Error::Type::ENDLINE_UNMATCHED);		//사실은 이러한 경우, 오류입니다//
	if (c == EOF) return c;
	if (!writeImpl()) return putError(Error::Type::WRITE_ERROR);		//이러한 경우 역시, 오류입니다//

	return 1;	//pass//
}

size_t MimeContentFile::SeekPointer() const
{
	if (m_printer)
		return m_printer->Seekpointer();

	return static_cast<size_t>(-1);
}

void MimeContentFile::decodeRuleBase64()
{
	//if (!m_readableLineCount)
	//	m_readBuffer.push_back('\n');	//처리 목적으로, 개행을 추가합니다//

	size_t lastWs = 0;
	auto streamIterator = m_readBuffer.cbegin();

	for (const auto &c : m_readBuffer)
	{
		if (c == '\n')
			lastWs = std::distance(m_readBuffer.cbegin(), streamIterator);
		++streamIterator;
	}

	m_decode->StreamPush(m_readBuffer, lastWs);

	std::vector<char> next(m_readBuffer.cbegin() + lastWs + 1, m_readBuffer.cend());
	m_decode->Decode(m_readBuffer);
	m_decode->StreamSet(next);
}

