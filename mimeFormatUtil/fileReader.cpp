
#include "fileReader.h"
#include "sourceFile.h"
#include "tokenPosition.h"
#include <fstream>

FileReader::FileReader()
{
	m_files = std::make_unique<file_vector_ty>();
}

FileReader::~FileReader()
{ }

std::unique_ptr<SourceFile> FileReader::makeFile(const std::string &name)
{
	std::unique_ptr<SourceFile> file(new SourceFile::File);
	//std::unique_ptr<SourceFile> file(new SourceFile::AdvancedFile);

	if (file->Open(name))
		return file;

	return {};
}

std::unique_ptr<SourceFile> FileReader::makeStream(const std::string &source)
{
	std::unique_ptr<SourceFile> stream(new SourceFile::Stream);

	stream->Open(source);
	return stream;
}

int FileReader::get()
{
	auto &&file = m_files->back();
	int c;

	if (file->HasBuffer())
		c = file->BufferPop();
	else
		c = file->Read();

	file->IncreasePosition(c);
	return c;
}

int FileReader::readc()
{
	for (;;)
	{
		int c = get();

		if (c == EOF)
		{
			if (m_files->size() == 1)
				return c;

			m_files->pop_back();
			continue;
		}

		if (c != '\\')
			return c;

		int c2 = get();
		if (c2 == '\n')
			continue;
		unreadc(c2);
		return c;
	}
}

void FileReader::unreadc(int c)
{
	if (c == EOF)
		return;

	auto &&file = m_files->back();

	file->BufferPush(c);
	file->DecreasePosition(c);
}

SourceFile *FileReader::currentFile() const
{
	if (m_files->empty())
		return nullptr;

	return m_files->back().get();
}

void FileReader::streamPush(std::unique_ptr<SourceFile> file)
{
	m_files->push_back(std::move(file));
}

std::unique_ptr<SourceFile> FileReader::streamDiscard()
{
	if (m_files->size())
	{
		std::unique_ptr<SourceFile> discard = std::move(m_files->back());

		m_files->pop_back();
		return discard;
	}
	return {};
}

size_t FileReader::streamDepth() const
{
	return m_files->size();
}

void FileReader::streamStash(std::unique_ptr<SourceFile> file)
{
	m_stashed.push_back(std::move(m_files));
	m_files = std::make_unique<file_vector_ty>();

	m_files->push_back(std::move(file));
}

void FileReader::streamUnstash()
{
	file_vector_pointer backStashed = std::move(m_stashed.back());

	m_stashed.pop_back();
	m_files = std::move(backStashed);
}

//public Method
bool FileReader::AddFile(const std::string &name)
{
	auto file = makeFile(name);

	if (!file)
		return false;

	streamPush(std::move(file));
	return true;
}

std::unique_ptr<TokenPosition> FileReader::getPos(int delta) const
{
	SourceFile *file = currentFile();
	auto pos = std::make_unique<TokenPosition>();

	pos->SetParamsLater(file->GetLine(), file->GetColumn() + delta, file->SeekPointer());
	return pos;
}

bool FileReader::fetchContext(FileReader &otherReader)
{
	std::unique_ptr<SourceFile> stream = otherReader.streamDiscard();

	if (!stream)
		return false;

	streamPush(std::move(stream));
	return true;
}

