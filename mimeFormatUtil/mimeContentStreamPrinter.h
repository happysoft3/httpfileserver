
#ifndef MIME_CONTENT_STREAM_PRINTER_H__
#define MIME_CONTENT_STREAM_PRINTER_H__

#include <string>
#include <memory>

class MimeContentStreamPrinter
{
	struct Impl;

private:
	std::unique_ptr<Impl> m_file;

public:
	MimeContentStreamPrinter();
	virtual ~MimeContentStreamPrinter();

public:
	virtual bool Create(const std::string &url);
	virtual size_t Seekpointer() const;
	virtual size_t Write(const char *src, size_t writeBytes);
};

class MimePrinterFactory
{
public:
	MimePrinterFactory();
	~MimePrinterFactory();

	virtual std::unique_ptr<MimeContentStreamPrinter> Create() const;
};

#endif

