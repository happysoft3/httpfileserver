
#include "mimeAttachPrinter.h"
#include <fstream>

AbstractMimeAttachPrinter::AbstractMimeAttachPrinter()
{ }

AbstractMimeAttachPrinter::~AbstractMimeAttachPrinter()
{ }

struct MimeAttachPrinter::Impl
{
	std::fstream m_file;
};

MimeAttachPrinter::MimeAttachPrinter()
	: AbstractMimeAttachPrinter()
{ }

MimeAttachPrinter::~MimeAttachPrinter()
{ }

bool MimeAttachPrinter::Create(const std::string &url)
{
	std::fstream file(url, std::ios::in | std::ios::binary);

	if (file)
	{
		m_impl = std::make_unique<Impl>();
		m_impl->m_file = std::move(file);
		return true;
	}
	return false;
}

size_t MimeAttachPrinter::Seekpointer() const
{
	if (m_impl)
		return static_cast<size_t>(m_impl->m_file.tellg());

	return 0;
}

void MimeAttachPrinter::writeImpl(const char *src, size_t length)
{
	if (m_impl)
		m_impl->m_file.write(src, length);
}
