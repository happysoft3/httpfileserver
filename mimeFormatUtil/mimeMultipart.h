
#ifndef MIME_MULTIPART_H__
#define MIME_MULTIPART_H__

#include "abstractMimeContent.h"
#include <vector>

class Numbering;

class MimeMultipart : public AbstractMimeContent
{
private:
	std::vector<std::unique_ptr<AbstractMimeContent>> m_childVec;
	int m_childIndex;

public:
	MimeMultipart(AbstractMimeContent *parent = nullptr);
	~MimeMultipart() override;

private:
	void onSetSequence(Numbering &seq) override;
	void onAddedChild(std::unique_ptr<AbstractMimeContent> &&child) override;

protected:
	void traversalPreorder(mime_traversal_fn_ty &&fn) override;

private:
	AbstractMimeContent *findItem(int itemId);

protected:
	AbstractMimeContent *findContent(int nodeId, int itemId) override;
};

#endif

