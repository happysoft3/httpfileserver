
#ifndef SOURCE_FILE_H__
#define SOURCE_FILE_H__

#include <string>
#include <memory>
#include <stack>
#include <tuple>
#include <vector>

class SourceFile		//이름 바꾸기를 원하지만,
{
public:
	class File;
	class Stream;
	class AdvancedFile;

private:
	std::stack<int> m_buffer;

protected:
	int m_line;
	int m_column;

public:
	SourceFile();
	virtual ~SourceFile();

public:
	void IncreasePosition(int c);
	void DecreasePosition(int c);

public:
	virtual bool Open(const std::string &name) = 0;
	virtual int Read() = 0;
	virtual size_t ReadAsVector(std::vector<char> &dest, size_t readBytes) = 0;
	virtual size_t SeekPointer() const = 0;
	virtual void MoveSeekPointer(size_t seekpointer) {};
	virtual std::string FileName() const
	{
		return {};
	}

	bool HasBuffer() const
	{
		return !m_buffer.empty();
	}
	void BufferPush(int c)
	{
		m_buffer.push(c);
	}
	int BufferPop()
	{
		int pop = m_buffer.top();

		m_buffer.pop();
		return pop;
	}

	int GetLine() const
	{
		return m_line;
	}

	int GetColumn() const
	{
		return m_column;
	}
};

class SourceFile::File : public SourceFile
{
protected:
	struct Impl;

private:
	std::unique_ptr<Impl> m_core;
	int m_last;

public:
	File();
	~File() override;

private:
	virtual void openImpl(Impl *core);
	bool Open(const std::string &name) override;

private:
	int Read() override;
	size_t SeekPointer() const override;

protected:
	void MoveSeekPointer(size_t seekpointer) override;

private:
	std::string FileName() const override;
	size_t ReadAsVector(std::vector<char> &dest, size_t readBytes) override;
};

class SourceFile::Stream : public SourceFile
{
private:
	std::string m_stream;
	size_t m_readPos;
	int m_last;

public:
	Stream();
	~Stream() override;

private:
	bool Open(const std::string &stream) override;
	int peek() const;
	int Read() override;
	size_t SeekPointer() const override;
	void MoveSeekPointer(size_t seekpointer) override;
	size_t ReadAsVector(std::vector<char> &dest, size_t readBytes) override;
};

class SourceFile::AdvancedFile : public SourceFile::File
{
private:
	std::vector<char> m_cache;
	SourceFile::File::Impl *m_impl;
	size_t m_fileSize;
	size_t m_cachePos;
	size_t m_cacheReadPos;
	int m_lastc;
	std::stack<char> m_ungetBuffer;

public:
	AdvancedFile(size_t cacheSize = 0);
	~AdvancedFile() override;

private:
	void calcFilesize();
	void openImpl(SourceFile::File::Impl *core) override;
	bool cacheStreamBuffer();
	int readImpl();
	int Read() override;
	void MoveSeekPointer(size_t seekpointer) override;
};

#endif

