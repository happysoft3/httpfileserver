
#include "mimeReader.h"
#include "sourceFile.h"
#include "mimePackage.h"
#include "mimeContent.h"
#include "mimeHeader.h"
#include "mimeError.h"
#include "mimeNodeex.h"
#include "ctoken.h"
#include "common/utils/stringhelper.h"

#include <vector>

using namespace _StringHelper;

class MimeReader::ContentNumbering : public AbstractMimeContent::Numbering
{
public:
	ContentNumbering()
	{ }
	~ContentNumbering()
	{ }
};

MimeReader::MimeReader()
	: FileTokenizer()
{
	m_topLevels = nullptr;
	m_readerFnStack.push([this]() { return this->doReadToken(); });
}

MimeReader::~MimeReader()
{ }

///Unused
void MimeReader::readerSwitching(MimeReader::reader_fn_ty &&fn)
{
	m_readerFnStack.push(std::forward<reader_fn_ty>(fn));
}

//Mime 중첩깊이가 1 증가합니다//
void MimeReader::pushMimeScope(AbstractMimeContent &dict)
{
	m_topLevels = &dict;
	m_root.push(m_topLevels);
}

//Mime 중첩깊이가 1 감소합니다//
AbstractMimeContent *MimeReader::popMimeScope()
{
	if (m_root.empty())
		return nullptr;

	AbstractMimeContent *pop = m_root.top();

	m_root.pop();
	m_topLevels = m_root.empty() ? nullptr : m_root.top();
	return pop;
}

//다음 토큰읽기//
std::unique_ptr<CToken> MimeReader::getToken()
{
	if (m_tokens.empty())
		return m_readerFnStack.top()();

	std::unique_ptr<CToken> tok = std::move(m_tokens.back());

	m_tokens.pop_back();
	return tok;
}

//현재 토큰 되돌리기//
void MimeReader::ungetToken(std::unique_ptr<CToken> tok)
{
	m_tokens.push_back(std::move(tok));
}

//다음 토큰 확인하기//
CToken *MimeReader::peekToken()
{	
	auto &&tok = getToken();
	CToken *pTok = tok.get();

	ungetToken(std::move(tok));
	return pTok;
}

bool MimeReader::expectKeyword(int expect)
{
	auto tok = getToken();

	if (tok->IsChar(expect))
		return true;

	ungetToken(std::move(tok));
	return false;
}

///? something ? 형식을 읽어들입니다///
void MimeReader::readInscope(MimeNodeEx &node)
{
	for (;;)
	{
		node.Append(*readIdent());
		
		if (expectKeyword('?'))
			return;

		auto tok = getToken();

		switch (tok->Type())
		{
		case CTokenType::Keyword:
			node.Append(std::move(tok));
			break;

		case CTokenType::Eof:
		case CTokenType::Newline:
			throw std::make_unique<MimeError>(std::move(tok), "unterminate inner scope");
			break;
		}
	}
}

////=?express?c?express?= 형식을 읽어들입니다///
void MimeReader::readOutscope(MimeNodeEx &node)
{
	readInscope(node);

	for (;;)
	{
		if (expectKeyword('='))
			return;

		node.Append(*readIdent());

		if (expectKeyword('?'))
		{
			readInscope(node);
			continue;
		}
		auto peek = peekToken();

		if (peek->IsType(CTokenType::Eof) || peek->IsType(CTokenType::Newline))
		{
			throw std::make_unique<MimeError>(getToken(), "unterminated outer scope");
			break;
		}
	}
}

//특문까지 다 읽음////
std::unique_ptr<MimeNodeEx> MimeReader::readExpr()
{
	std::unique_ptr<MimeNodeEx> node(new MimeNodeEx);

	for (;;)
	{
		auto tok = getToken();

		switch (tok->Type())
		{
		case CTokenType::Ident:
		case CTokenType::Number:
		case CTokenType::String:
			node->Append(std::move(tok));
			node->Append(*readIdent()); //TODO. 리턴으로 노드 연결
			break;

		case CTokenType::Keyword:
			if (tok->IsChar('='))
			{
				if (expectKeyword('?'))
				{
					readOutscope(*node);
					break;
				}
			}
			else if (tok->IsChar(';'))
			{
				ungetToken(std::move(tok));
				return node;
			}
			
			node->Append(std::move(tok));
			break;

		case CTokenType::Newline:
		case CTokenType::Eof:
			ungetToken(std::move(tok));
			return node;
		}
	}
}

//문자, 넘버, 스트링만 읽음//
std::unique_ptr<MimeNodeEx> MimeReader::readIdent()
{
	std::unique_ptr<MimeNodeEx> node(new MimeNodeEx);

	for (;;)
	{
		auto tok = getToken();

		switch (tok->Type())
		{		
		case CTokenType::Ident:
		case CTokenType::Number:
		case CTokenType::String:
			node->Append(std::move(tok));
			break;

		case CTokenType::Space:
			break;		//공백은, 일단 건너뜁니다

		case CTokenType::Newline:
		case CTokenType::Eof:
		default:
			ungetToken(std::move(tok));
			return node;
		}
	}
}

//// key=value 형식을 읽어들입니다////
std::unique_ptr<MimeNodeEx> MimeReader::readProperty()
{
	skipSpaceNewline();

	auto ident = getToken();
	std::string errvalue;

	if (!ident->IsType(CTokenType::Ident))
	{
		errvalue = ident->Value();

		throw std::make_unique<MimeError>(std::move(ident), stringFormat("expect ident but got '%s'", errvalue));
		//return {};
	}
	if (!expectKeyword('='))
	{
		errvalue = peekToken()->Value();
		
		throw std::make_unique<MimeError>(getToken(), stringFormat("expect = but got '%s'", errvalue));
		//return {};
	}

	return MimeNodeEx::Create<MimeCoreBase::MimePair>(std::make_unique<MimeNodeEx>(std::move(ident)) , readExpr());
}

////이러한 형식을 읽어들입니다, key:value //////
////key:value; skey=svalue 형식을 포함합니다/////
std::unique_ptr<MimeNodeEx> MimeReader::readLine()
{
	skipSpaceNewline();
	auto key = readIdent();

	if (!expectKeyword(':'))
	{
		auto errTok = std::move(getToken());
		std::string svalue = errTok->Value();

		throw std::make_unique<MimeError>(std::move(errTok), stringFormat("expect ':' but got '%s'", svalue));
	}

	auto value = readExpr();
	std::unique_ptr<MimeNodeEx> properties;

	if (expectKeyword(';'))
		properties = readProperty();

	return MimeNodeEx::Create<MimeCoreBase::MimeIdent>(std::move(key), std::move(value), std::move(properties));
}

/////mime 맨 앞에, EF BB BF 가 있는데 이것을 무시합니다
void MimeReader::ignoreHeaderValue(size_t ignoreByteCount)
{
	std::vector<std::unique_ptr<CToken>> ignoreVec(ignoreByteCount);

	for (auto &&assign : ignoreVec)
	{
		assign = getToken();
		if (!assign->IsType(CTokenType::Invalid))
		{
			for (auto &&tok : ignoreVec)
			{
				if (tok)
					ungetToken(std::move(tok));
			}
			break;
		}
	}
}

////RFC-822헤더를 읽음////
////MIME Version: 1.0, 헤더까지만 여기에 들어옵니다///
void MimeReader::readMailHeader(AbstractMimeContent &content)
{
	std::string mimeKey;
	auto header = std::make_unique<MimeHeader>();

	for (;;)
	{
		std::unique_ptr<MimeNodeEx> node;
		try
		{
			node = readLine();
		}
		catch (std::unique_ptr<MimeError> &err)
		{
			err->What();
		}
		
		if (peekToken()->IsType(CTokenType::Eof))
			throw std::make_unique<MimeError>(getToken(), "not found mime header");
		
		header->Put(std::move(node));
		if (header->MatchWithLatestKey("MIME-Version"))		////여기에서 MIME 헤더 찾고, 찾았으면 나갑니다
			break;
	}
	content.PutHeader(std::move(header));
}

///////공백 그리고, 개행이 아닐 때 까지 읽어들입니다////
void MimeReader::skipSpaceNewline()
{
	for (;;)
	{
		auto tok = getToken();

		if (tok->IsType(CTokenType::Newline) || tok->IsType(CTokenType::Space))
			continue;

		ungetToken(std::move(tok));
		break;
	}
}

bool MimeReader::readMimeBoundaryDirect(CToken &boundary)
{
	std::string boundaryKeySrc = boundary.Value();
	bool boundaryTerminated = boundaryKeySrc.back() == '-';
	std::string boundaryKey = boundaryKeySrc;

	if (boundaryTerminated)
		boundaryKey.resize(boundaryKey.size() - 2);

	bool matched = m_topLevels->MatchBoundaryKey(boundaryKey);

	if (matched)
	{
		(*m_topLevels)->PushBoundaryOffset(boundary);
		if (!dynamic_cast<MimeMultipart *>(m_topLevels))
			popMimeScope();
		if (boundaryTerminated)
		{
			popMimeScope();
			tryReadMimeBoundary();
		}
	}
	return matched;
}

//바운더리 읽지 못한 것을, 오류로 처리하지 않고 계속 읽어들입니다//
void MimeReader::tryReadMimeBoundary()
{
	for (;;)
	{
		auto tok = getToken();

		switch (tok->Type())
		{
		case CTokenType::Newline:
			break;

		case CTokenType::Anyline:
		{
			if (readMimeBoundaryDirect(*tok))
				return;
			break;
		}

		case CTokenType::Eof:
			//throw std::make_unique<MimeError>(std::move(tok), "there is no boundary");
			ungetToken(std::move(tok));

		default:
			return;
		}
	}
}

/****
* key: value
* key: value; subkey= subvalue
* ...
* (newline) then end
***/
std::unique_ptr<MimeHeader> MimeReader::readMimePairField()
{
	std::unique_ptr<MimeNodeEx> node;
	auto header = std::make_unique<MimeHeader>();
	//!before space!//

	for (;;)
	{
		node = readLine();

		if (peekToken()->IsType(CTokenType::Eof))
			throw std::make_unique<MimeError>(getToken(), "mime header is terminated incorrectly");

		header->Put(std::move(node));
		if (getToken()->IsType(CTokenType::Newline))
		{
			if (peekToken()->IsType(CTokenType::Newline))
				break;
		}
	}
	return header;
}

void MimeReader::doReadMimeHeader()
{
	if (peekToken()->IsType(CTokenType::Eof))
		return;

	auto header = readMimePairField();

	if (!header->Length())
		throw std::make_unique<MimeError>(getToken(), "mime header is missing");

	auto lineTok = getToken();

	if (!lineTok->IsType(CTokenType::Newline))
	{
		std::string err = lineTok->Value();
		throw std::make_unique<MimeError>(std::move(lineTok), stringFormat("expect (newline) but got '%s'", err));
	}

	AbstractMimeContent *prevTop = m_topLevels;
	std::unique_ptr<AbstractMimeContent> child;

	header->PushBoundaryOffset(*lineTok);
	if (header->HasBoundaryKey())
		child = std::make_unique<MimeMultipart>(m_topLevels);
	else
		child = std::make_unique<MimeContent>(m_topLevels);

	child->PutSequence(*m_seq);
	child->PutHeader(std::move(header));
	pushMimeScope(*child);

	if (prevTop)
		prevTop->PushChild(std::move(child));
}

void MimeReader::assertIfUnterminatedLevel()
{
	if (m_root.size() > 1)
		throw std::make_unique<MimeError>(m_topLevels, "unterminated content");
}

////사실 상, Mime Content 헤더와 Content Offset(line number)? 을 읽어요////
void MimeReader::readMime()
{
	for (;;)
	{
		skipSpaceNewline();
		doReadMimeHeader();

		m_readerFnStack.push([this]() { return this->doReadLine(); });
		if (peekToken()->IsType(CTokenType::Eof))
			break;

		tryReadMimeBoundary();
		m_readerFnStack.pop();
	}
	assertIfUnterminatedLevel();
}

////파싱에 성공한 경우, true 입니다////
bool MimeReader::LastError(std::string &err) const
{
	if (m_lastError.empty())
		return true;

	err = m_lastError;
	return false;
}


/*
multipart/mixed
  +-- multipart/alternative
  | +-- text/plain
  | +-- multipart/related
  | +-- text/html
  | +-- image/png
  | +-- image/png
  +-- application/octet-stream;
*/
////파싱 시작입니다-- 파싱 오류 시, 자세한 정보는 LastError을 호출하여 받으세요////
std::unique_ptr<MimePackage> MimeReader::Parse()
{
	static constexpr size_t ignore_unknown_byte_count = 3;		//UTF-8 BOM 형식으로 저장하면, 생성됩니다//
	std::unique_ptr<MimePackage> package(new MimePackage);

	m_seq = std::make_unique<ContentNumbering>();
	package->PutSequence(*m_seq);

	pushMimeScope(*package);

	package->SetUrl(currentFile()->FileName());
	m_lastError = {};
	ignoreHeaderValue(ignore_unknown_byte_count);
	try
	{
		readMailHeader(*m_topLevels);
		readMime();
	}
	catch (std::unique_ptr<MimeError> &err)
	{
		m_lastError = err->What();
		return {};
	}

	return package;
}

