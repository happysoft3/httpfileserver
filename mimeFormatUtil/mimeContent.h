
#ifndef MIME_CONTENT_H__
#define MIME_CONTENT_H__

#include "abstractMimeContent.h"

class MimeContent : public AbstractMimeContent
{
public:
	MimeContent(AbstractMimeContent *parent = nullptr);
	~MimeContent() override;

private:
	int getParentNodeId() const;
	std::string GetNodeIdString() const override;
	AbstractMimeContent *findContent(int nodeId, int itemId) override;
	void onPutHeader(MimeHeader &header) override;
};

#endif

