
#ifndef FILE_READER_H__
#define FILE_READER_H__

#include <list>
#include <memory>
#include <vector>
#include <string>

class SourceFile;
class TokenPosition;

class FileReader
{
private:
	using file_vector_ty = std::vector<std::unique_ptr<SourceFile>>;
	using file_vector_pointer = std::unique_ptr<file_vector_ty>;
	file_vector_pointer m_files;

	using stashed_vector_ty = std::vector<file_vector_pointer>;
	stashed_vector_ty m_stashed;

public:
	FileReader();
	virtual ~FileReader();

private:
	virtual std::unique_ptr<SourceFile> makeFile(const std::string &name);
	std::unique_ptr<SourceFile> makeStream(const std::string &source);

protected:
	int get();
	virtual int readc();
	void unreadc(int c);
	SourceFile *currentFile() const;
	void streamPush(std::unique_ptr<SourceFile> file);
	std::unique_ptr<SourceFile> streamDiscard();
	size_t streamDepth() const;
	void streamStash(std::unique_ptr<SourceFile> file);
	void streamUnstash();

public:
	int Files() const
	{
		return m_files->size();
	}
	bool AddFile(const std::string &name);

protected:
	std::unique_ptr<TokenPosition> getPos(int delta = 0) const;
	bool fetchContext(FileReader &otherReader);
};

#endif

