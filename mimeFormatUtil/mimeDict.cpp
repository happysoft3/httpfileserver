
#include "mimeDict.h"
#include "mimeError.h"
#include "mimeNodeEx.h"

#include "common/utils/stringhelper.h"

using namespace _StringHelper;

class MimeDict::Core
{
private:
	MimeDict *m_owner;

public:
	Core(MimeDict *owner)
		: m_owner(owner)
	{ }

	~Core()
	{ }

	MimeDict *Owner() const
	{
		return m_owner;
	}
	MimeDict *operator->() const
	{
		return m_owner;
	}
};

struct MimeDict::MimeDictElem
{
	std::string m_value;
	std::string m_subKey;
	std::string m_subValue;
	std::unique_ptr<MimeNodeEx> m_nodebase;
};

struct MimeDict::MimeBoundary
{
	std::string m_uniqueKey;
	std::list<int> m_lines;
};

MimeDict::MimeDict(MimeDict *parent)
	: m_core(new Core(this))
{
	if (parent)
	{
		m_parentCore = parent->m_core;
		m_createdId = parent->m_createdId + 1;
	}
	else
	{
		m_createdId = 1;
	}
}

MimeDict::~MimeDict()
{
	m_core.reset();		//실제 소유자가 소멸하기 전에, 먼저 소멸해야 합니다//
}

MimeDict::MimeDictElem *MimeDict::makeElem(const std::string &value)
{
	MimeDictElem *elem(new MimeDictElem);

	elem->m_value = value;
	return elem;
}

MimeDict::MimeBoundary *MimeDict::makeBoundary(const std::string &uniqKey)
{
	MimeBoundary *boundary(new MimeBoundary);

	boundary->m_uniqueKey = uniqKey;
	return boundary;
}

bool MimeDict::retrieveMap(const std::string &key, std::function<void(MimeDict::MimeDictElem &)> fn) const
{
	auto keyIterator = m_headMap.find(key);

	if (keyIterator != m_headMap.cend())
	{
		if (fn)
			fn(*keyIterator->second);
		return true;
	}
	return false;
}

std::string MimeDict::BoundaryKey() const
{
	if (!m_boundary)
		return {};

	return m_boundary->m_uniqueKey;
}

bool MimeDict::HasKey(const std::string &key) const
{
	return retrieveMap(key, {});
}

bool MimeDict::MatchWithLatestKey(const std::string &key) const
{
	if (m_latestKey.empty())
		return {};

	return m_latestKey == key;
}

void MimeDict::Put(std::unique_ptr<MimeNodeEx> node)
{
	m_latestKey = {};

	if (node->Type() == MimeNodeType::Ident)
	{
		m_latestKey = node->Extra(&MimeCoreBase::MimeIdent::Key);
		if (m_latestKey.empty())
			return;

		std::unique_ptr<MimeDictElem> elem(makeElem(node->Extra(&MimeCoreBase::MimeIdent::Value)));
		MimeNodeEx *subPair = nullptr;

		if (node->Extra(&MimeCoreBase::MimeIdent::GetSubPair, subPair))
		{
			elem->m_subKey = subPair->Extra(&MimeCoreBase::MimePair::Key);
			elem->m_subValue = subPair->Extra(&MimeCoreBase::MimePair::Value);
		}
		elem->m_nodebase.swap(node);
		m_headMap[m_latestKey] = std::move(elem);
	}
}

std::string MimeDict::Value(const std::string &key, MimeDict::ValueType ty)
{
	std::shared_ptr<std::string> value = std::make_shared<std::string>();

	switch (ty)
	{
	case ValueType::MIME_VALUE:
		retrieveMap(key, [value](MimeDictElem &elem) { *value = elem.m_value; });
		break;

	case ValueType::SUBKEY:
		retrieveMap(key, [value](MimeDictElem &elem) { *value = elem.m_subKey; });
		break;

	case ValueType::SUBVALUE:
		retrieveMap(key, [value](MimeDictElem &elem) { *value = elem.m_subValue; });
		break;
	}
	return *value;
}

void MimeDict::readContentType(MimeDict::MimeDictElem &dictElem)
{
	size_t seperator = dictElem.m_value.find_first_of('/');
	std::string mainTy = dictElem.m_value.substr(0, seperator);

	///멀티파트 이면, 바운더리, 아니면 오류///
	if (mainTy == "multipart")
	{
		if (dictElem.m_subKey != "boundary")
			throw std::make_unique<MimeError>(std::move(dictElem.m_nodebase), stringFormat("expect 'boundary' but got %s", dictElem.m_subKey));

		if (dictElem.m_subValue.empty())
			throw std::make_unique<MimeError>(std::move(dictElem.m_nodebase), stringFormat("boundary key is empty"));

		m_boundary = std::shared_ptr<MimeBoundary>(makeBoundary(dictElem.m_subValue));
	}
}

bool MimeDict::HasBoundaryKey() const
{
	if (!m_boundary)
		return false;

	return true;
}

void MimeDict::MimePreorder()
{
	for (const auto &elem : m_headMap)
	{
		if (elem.first == "Content-Type")
			readContentType(*elem.second);
	}
}

// 현재 필드가 멀티파트가 아닌 경우, 부모 필드로 부터, boundary 을 가져옵니다//
std::shared_ptr<MimeDict::MimeBoundary> MimeDict::loadBoundary()
{
	if (m_boundary)
		return m_boundary;
	else if (m_parentCore.expired())
		return {};

	auto parentCore = m_parentCore.lock();
	
	return (*parentCore)->m_boundary;		//사실, 부모의 boundary 가 nullptr 이면, 오류로 처리해야 합니다//
}

MimeDict *MimeDict::getBoundaryOwner()
{
	if (m_boundary)
		return this;

	auto parentCore = m_parentCore.expired() ? nullptr : m_parentCore.lock();

	if (parentCore)
		return parentCore->Owner();

	return nullptr;
}

MimeDict *MimeDict::getParent() const
{
	if (m_parentCore.expired())
		return nullptr;

	return m_parentCore.lock()->Owner();
}

size_t MimeDict::getChildCount() const
{
	return m_childVec.size();
}

MimeDict *MimeDict::getChild(size_t index)
{
	if (index >= m_childVec.size())
		return {};

	return m_childVec[index].get();
}

//	slience 이면, throw 안합니다///
bool MimeDict::MatchBoundaryKey(std::unique_ptr<MimeNodeEx> &&boundary, bool silence)
{
	MimeDict *owner = getBoundaryOwner();

	if (!owner)
		return false;

	std::string bKey = owner->m_boundary->m_uniqueKey;
	int column = boundary->MatchWithList({ bKey });

	if (column == -1)
	{
		if (silence)
			return false;

		throw std::make_unique<MimeError>(std::move(boundary), stringFormat("unmatched boundary key"));
	}

	owner->m_boundary->m_lines.push_back(boundary->StartLine());
	return true;
}

void MimeDict::onAddedChild(const MimeDict &child)
{
	MimeDict *parent = getParent();

	if (parent)
		parent->onAddedChild(child);
}

void MimeDict::onExpiredBoundary()
{
	MimeDict *parent = getParent();

	if (parent)
		parent->onExpiredBoundary();
}

void MimeDict::PushChild(std::unique_ptr<MimeDict> child)
{
	onAddedChild(*child);
	m_childVec.push_back(std::move(child));
	///TODO. 여기에서 child 추가 작업을, 합니다
}

void MimeDict::ExitBoundary()
{
	onExpiredBoundary();
}

void MimeDict::traversalPreorder(MimeDict::node_travel_fn_ty &&fn)
{
	std::forward<node_travel_fn_ty>(fn)(this);

	for (const auto &node : m_childVec)
		node->traversalPreorder(std::forward<std::remove_reference<decltype(fn)>::type>(fn));
}

void MimeDict::traversalCurrentChild(MimeDict::node_travel_fn_ty &&fn)
{
	for (auto &node : m_childVec)
		std::forward<std::remove_reference<decltype(fn)>::type>(fn)(node.get());
}

void MimeDict::childTraversal(MimeDict::node_travel_fn_ty &&fn, MimeDict::traversal_ty traversal, MimeDict &from)
{
	(from.*traversal)(std::forward<std::decay_t<decltype(fn)>>(fn));
}

