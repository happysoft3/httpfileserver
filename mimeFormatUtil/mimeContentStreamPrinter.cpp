
#include "mimeContentStreamPrinter.h"
#include <fstream>

struct MimeContentStreamPrinter::Impl
{
	std::fstream m_writter;
};

MimeContentStreamPrinter::MimeContentStreamPrinter()
{ }

MimeContentStreamPrinter::~MimeContentStreamPrinter()
{ }

bool MimeContentStreamPrinter::Create(const std::string &url)
{
	auto file = std::make_unique<Impl>();

	file->m_writter = std::fstream(url, std::ios::out | std::ios::binary);
	if (!file)
		return false;

	m_file = std::move(file);
	return true;
}

size_t MimeContentStreamPrinter::Seekpointer() const
{
	if (m_file)
		return static_cast<size_t>(m_file->m_writter.tellg());

	return -1;
}

size_t MimeContentStreamPrinter::Write(const char *src, size_t writeBytes)
{
	size_t prev = Seekpointer();

	if (m_file)
	{
		m_file->m_writter.write(src, writeBytes);

		return Seekpointer() - prev;
	}
	return prev;
}

////

MimePrinterFactory::MimePrinterFactory()
{ }

MimePrinterFactory::~MimePrinterFactory()
{ }

std::unique_ptr<MimeContentStreamPrinter> MimePrinterFactory::Create() const
{
	return std::make_unique<MimeContentStreamPrinter>();
}


